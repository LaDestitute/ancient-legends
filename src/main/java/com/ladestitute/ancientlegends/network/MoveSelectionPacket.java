package com.ladestitute.ancientlegends.network;

import com.ladestitute.ancientlegends.battle.core.BattleManager;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.protocol.common.custom.CustomPacketPayload;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.neoforged.neoforge.common.util.FakePlayer;
import net.neoforged.neoforge.network.handling.PlayPayloadContext;

import javax.annotation.Nonnull;
import java.util.Optional;

public class MoveSelectionPacket implements CustomPacketPayload {
    public static final ResourceLocation ID = new ResourceLocation("ancientlegends", "move_selection");
    private final int moveSlot;

    public MoveSelectionPacket(int moveSlot) {
        this.moveSlot = moveSlot;
    }

    @Override
    @Nonnull
    public ResourceLocation id() {
        return ID;
    }

    @Override
    public void write(FriendlyByteBuf buffer) {
        buffer.writeInt(moveSlot);
    }

    public MoveSelectionPacket(FriendlyByteBuf buffer) {
        this.moveSlot = buffer.readInt();
    }

    public static void handle(MoveSelectionPacket message, PlayPayloadContext context) {
        System.out.println("MoveSelectionPacket received with slot: " + message.moveSlot);
        Optional<Player> optionalPlayer = context.player();
        if (optionalPlayer.isPresent() && optionalPlayer.get() instanceof ServerPlayer serverPlayer) {
            if (!(serverPlayer instanceof FakePlayer)) {
                System.out.println("Processing move selection for player: " + serverPlayer.getName().getString());
                if (BattleManager.instance != null) {
                    System.out.println("BattleManager instance found, handling move...");
                    BattleManager.instance.handlePlayerMoveInput(serverPlayer, message.moveSlot);
                    BattleManager.instance.isPlayerTurn=false;
                } else {
                    System.out.println("BattleManager instance is null");
                }
            }
        }
    }
}
