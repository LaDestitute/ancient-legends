package com.ladestitute.ancientlegends.network;

import com.ladestitute.ancientlegends.AncientLegendsMain;
import com.ladestitute.ancientlegends.capability.playertracking.PlayerTrackingCapability;
import com.ladestitute.ancientlegends.registry.AttachmentInit;
import net.minecraft.client.Minecraft;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.protocol.common.custom.CustomPacketPayload;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.neoforged.neoforge.network.handling.PlayPayloadContext;

public class PlayerDataPacket implements CustomPacketPayload {
    public static final ResourceLocation ID = AncientLegendsMain.prefix("al_player_data");

    private final int entityId;

    private final boolean haskit;

    public PlayerDataPacket(LivingEntity entity, boolean haskit) {
        this.entityId = entity.getId();
        this.haskit = haskit;
    }

    public PlayerDataPacket(int id, boolean haskit) {
        this.entityId = id;
        this.haskit = haskit;
    }

    @Override
    public ResourceLocation id() {
        return ID;
    }

    public void write(FriendlyByteBuf buffer) {
        buffer.writeInt(this.entityId);
        buffer.writeBoolean(this.haskit);
    }

    public PlayerDataPacket(FriendlyByteBuf buffer) {
        this(buffer.readInt(), buffer.readBoolean());
    }

    public static void handle(PlayerDataPacket message, PlayPayloadContext context) {
        context.workHandler().execute(() -> {
            Entity entity = (Minecraft.getInstance()).player.level().getEntity(message.entityId);
            if (entity != null && entity instanceof LivingEntity living) {
                PlayerTrackingCapability cap = living.getData(AttachmentInit.PLAYER_DATA);
                cap.setHasNewWorldKit((LivingEntity) entity, message.haskit);
            }
        });

    }
}
