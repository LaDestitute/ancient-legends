package com.ladestitute.ancientlegends.network;

import com.ladestitute.ancientlegends.AncientLegendsMain;
import com.ladestitute.ancientlegends.capability.playertracking.PlayerTrackingCapability;
import com.ladestitute.ancientlegends.capability.pokedata.PokeDataCapability;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.PokemonEntity;
import com.ladestitute.ancientlegends.registry.AttachmentInit;
import net.minecraft.client.Minecraft;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.protocol.common.custom.CustomPacketPayload;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.Level;
import net.neoforged.neoforge.network.handling.PlayPayloadContext;

public class SyncPokeDataPacket implements CustomPacketPayload {
    public static final ResourceLocation ID = AncientLegendsMain.prefix("al_poke_data");
    private final int entityId;
    private final int level;
    private final int gender;

    public SyncPokeDataPacket(LivingEntity entity, int level, int gender) {
        this.entityId = entity.getId();
        this.level = level;
        this.gender = gender;
    }

    public SyncPokeDataPacket(int id, int level, int gender) {
        this.entityId = id;
        this.level = level;
        this.gender = gender;
    }

    @Override
    public ResourceLocation id() {
        return ID;
    }

    public void write(FriendlyByteBuf buffer) {
        buffer.writeInt(this.entityId);
        buffer.writeInt(this.level);
        buffer.writeInt(this.gender);
    }

    public SyncPokeDataPacket(FriendlyByteBuf buffer) {
        this(buffer.readInt(), buffer.readInt(), buffer.readInt());
    }

    public static void handle(SyncPokeDataPacket msg, PlayPayloadContext context) {
        context.workHandler().execute(() -> {
            Level world = Minecraft.getInstance().level;
            if (world != null) {
                Entity entity = world.getEntity(msg.entityId);
                if (entity instanceof PokemonEntity) {
                    PokeDataCapability data = entity.getData(AttachmentInit.POKE_DATA);
                    data.SetPokemonLevel(msg.level);
                    data.SetPokemonGender(msg.gender);
                }
            }
        });
    }
}
