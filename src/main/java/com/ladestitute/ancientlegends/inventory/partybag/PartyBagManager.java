package com.ladestitute.ancientlegends.inventory.partybag;

import com.ladestitute.ancientlegends.AncientLegendsMain;
import com.ladestitute.ancientlegends.items.utilities.PartyBagItem;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.saveddata.SavedData;
import net.neoforged.fml.util.thread.SidedThreadGroups;
import net.neoforged.neoforge.server.ServerLifecycleHooks;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.UUID;

public class PartyBagManager extends SavedData {
    private static final String NAME = AncientLegendsMain.MOD_NAME + "_party_bag_data";

    private static final HashMap<UUID, PartyBagData> data = new HashMap<>();

    public static final PartyBagManager blankClient = new PartyBagManager();

    public static PartyBagManager get() {
        if (Thread.currentThread().getThreadGroup() == SidedThreadGroups.SERVER)
            return ServerLifecycleHooks.getCurrentServer().getLevel(Level.OVERWORLD).getDataStorage().computeIfAbsent(new Factory<>(PartyBagManager::new, PartyBagManager::load), NAME);
        else
            return blankClient;
    }

    public PartyBagData getOrCreatePartyBag(UUID uuid, PartyBagItem item) {
        return data.computeIfAbsent(uuid, id -> {
            setDirty();
            return new PartyBagData(id);
        });
    }

    public static PartyBagManager load(CompoundTag nbt) {
        if (nbt.contains("Party Bags")) {
            ListTag list = nbt.getList("Party Bags", Tag.TAG_COMPOUND);
            list.forEach((partybagNBT) -> PartyBagData.fromNBT((CompoundTag) partybagNBT).ifPresent((partybag) -> data.put(partybag.getUuid(), partybag)));
        }
        return new PartyBagManager();
    }

    @Override
    @Nonnull
    public CompoundTag save(CompoundTag compound) {
        ListTag partybags = new ListTag();
        data.forEach(((uuid, partybagData) -> partybags.add(partybagData.toNBT())));
        compound.put("Party Bags", partybags);
        return compound;
    }
}

