package com.ladestitute.ancientlegends.inventory.partybag;

import com.ladestitute.ancientlegends.registry.ItemInit;
import net.minecraft.world.item.ItemStack;
import net.neoforged.neoforge.items.IItemHandler;
import net.neoforged.neoforge.items.IItemHandlerModifiable;
import net.neoforged.neoforge.items.SlotItemHandler;

import javax.annotation.Nonnull;

public class PartyBagContainerSlot extends SlotItemHandler {
    private int index;
    public PartyBagContainerSlot(IItemHandler itemHandler, int index, int xPosition, int yPosition) {
        super(itemHandler, index, xPosition, yPosition);
        this.index = index;
    }

    @Override
    public int getMaxStackSize(@Nonnull ItemStack stack) {
        return super.getMaxStackSize();
    }

    @Override
    public boolean mayPlace(@Nonnull ItemStack stack) {
        return stack.is(ItemInit.POKE_BALL.get()) && stack.hasTag() && (stack.hasTag() && stack.getTag().contains("Captured"));
    }

    //bandage till forge PR fixes this
    @Override
    public void initialize(ItemStack itemStack) {
        ((IItemHandlerModifiable) this.getItemHandler()).setStackInSlot(index, itemStack);
    }
}

