package com.ladestitute.ancientlegends.inventory.partybag;

import com.ladestitute.ancientlegends.registry.ItemInit;
import net.minecraft.world.item.ItemStack;
import net.neoforged.neoforge.items.ItemStackHandler;

import javax.annotation.Nonnull;

public class PartyBagInvHandler extends ItemStackHandler {
    public PartyBagInvHandler(int size) {
        super(size);
    }

    @Override
    protected void onContentsChanged(int slot) {
        PartyBagManager.get().setDirty();
    }

    @Override
    public boolean isItemValid(int slot, @Nonnull ItemStack stack) {
        return stack.is(ItemInit.POKE_BALL.get()) && stack.hasTag() && (stack.hasTag() && stack.getTag().contains("Captured"));
    }
}
