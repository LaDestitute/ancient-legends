package com.ladestitute.ancientlegends.inventory.satchel;

import com.ladestitute.ancientlegends.registry.ItemInit;
import net.minecraft.world.item.ItemStack;
import net.neoforged.neoforge.items.ItemStackHandler;

import javax.annotation.Nonnull;

public class SatchelInvHandler extends ItemStackHandler {
    public SatchelInvHandler(int size) {
        super(size);
    }

    @Override
    protected void onContentsChanged(int slot) {
        SatchelManager.get().setDirty();
    }

    @Override
    public boolean isItemValid(int slot, @Nonnull ItemStack stack) {
        if (stack.is(ItemInit.APRICORN.get()) || stack.is(ItemInit.TUMBLESTONE.get())) {
            return true;
        }
        return stack.is(ItemInit.POKE_BALL.get()) && !stack.hasTag() || (stack.hasTag() && !stack.getTag().contains("Captured"));
    }
}