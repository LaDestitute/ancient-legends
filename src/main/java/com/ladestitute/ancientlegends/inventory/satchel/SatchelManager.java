package com.ladestitute.ancientlegends.inventory.satchel;

import com.ladestitute.ancientlegends.AncientLegendsMain;
import com.ladestitute.ancientlegends.items.utilities.SatchelItem;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.saveddata.SavedData;
import net.neoforged.fml.util.thread.SidedThreadGroups;
import net.neoforged.neoforge.server.ServerLifecycleHooks;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.UUID;

public class SatchelManager extends SavedData {
    private static final String NAME = AncientLegendsMain.MOD_NAME + "_satchel_data";

    private static final HashMap<UUID, SatchelData> data = new HashMap<>();

    public static final SatchelManager blankClient = new SatchelManager();

    public static SatchelManager get() {
        if (Thread.currentThread().getThreadGroup() == SidedThreadGroups.SERVER)
            return ServerLifecycleHooks.getCurrentServer().getLevel(Level.OVERWORLD).getDataStorage().computeIfAbsent(new Factory<>(SatchelManager::new, SatchelManager::load), NAME);
        else
            return blankClient;
    }

    public SatchelData getOrCreateSatchel(UUID uuid, SatchelItem item) {
        return data.computeIfAbsent(uuid, id -> {
            setDirty();
            return new SatchelData(id);
        });
    }

    public static SatchelManager load(CompoundTag nbt) {
        if (nbt.contains("Satchels")) {
            ListTag list = nbt.getList("Satchels", Tag.TAG_COMPOUND);
            list.forEach((satchelNBT) -> SatchelData.fromNBT((CompoundTag) satchelNBT).ifPresent((satchel) -> data.put(satchel.getUuid(), satchel)));
        }
        return new SatchelManager();
    }

    @Override
    @Nonnull
    public CompoundTag save(CompoundTag compound) {
        ListTag satchels = new ListTag();
        data.forEach(((uuid, satchelData) -> satchels.add(satchelData.toNBT())));
        compound.put("Satchels", satchels);
        return compound;
    }
}
