package com.ladestitute.ancientlegends.container;

import com.ladestitute.ancientlegends.inventory.satchel.SatchelContainerSlot;
import com.ladestitute.ancientlegends.registry.MenuInit;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ClickType;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;
import net.neoforged.neoforge.items.IItemHandler;
import net.neoforged.neoforge.items.ItemStackHandler;

import javax.annotation.Nonnull;
import java.util.UUID;

public class SatchelMenu extends AbstractContainerMenu {
    public final IItemHandler handler;

    public static SatchelMenu fromNetwork(int windowId, final Inventory playerInventory, FriendlyByteBuf data) {
        UUID uuidIn = data.readUUID();
        return new SatchelMenu(windowId, playerInventory, uuidIn, new ItemStackHandler(6));
    }

    public SatchelMenu(final int windowId, Inventory playerInventoryIn, UUID uuidIn, IItemHandler handler) {
        super(MenuInit.SATCHEL.get(), windowId);

        this.handler = handler;
        addMySlots();

        // Add player inventory slots
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 9; j++) {
                addSlot(new Slot(playerInventoryIn, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
            }
        }

        // Add hotbar slots
        for (int k = 0; k < 9; k++) {
            addSlot(new Slot(playerInventoryIn, k, 8 + k * 18, 142));
        }
    }

    @Override
    public boolean stillValid(@Nonnull Player playerIn) {
        return true;
    }

    private void addMySlots() {
        if (this.handler == null) return;

        int slot_index = 0;
        int cols = 3;
        int rows = 2;

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                int x = 44 + col * 18;  // Adjusted x position to align with the texture
                int y = 20 + row * 18;  // Adjusted y position to align with the texture

                this.addSlot(new SatchelContainerSlot(this.handler, slot_index, x, y));
                slot_index++;
                if (slot_index >= 6)
                    break;
            }
        }
    }

    @Override
    @Nonnull
    public void clicked(int slot, int dragType, @Nonnull ClickType clickTypeIn, @Nonnull Player player) {
        if (clickTypeIn == ClickType.SWAP)
            return;
        if (slot >= 0) getSlot(slot).container.setChanged();
        super.clicked(slot, dragType, clickTypeIn, player);
    }

    @Override
    @Nonnull
    public ItemStack quickMoveStack(@Nonnull Player playerIn, int index) {
        ItemStack itemstack = ItemStack.EMPTY;
        Slot slot = this.slots.get(index);

        if (slot != null && slot.hasItem()) {
            int bagslotcount = this.slots.size();
            ItemStack itemstack1 = slot.getItem();
            itemstack = itemstack1.copy();
            if (index < playerIn.getInventory().items.size()) {
                if (!this.moveItemStackTo(itemstack1, playerIn.getInventory().items.size(), bagslotcount, false))
                    return ItemStack.EMPTY;
            } else if (!this.moveItemStackTo(itemstack1, 0, playerIn.getInventory().items.size(), false)) {
                return ItemStack.EMPTY;
            }
            if (itemstack1.isEmpty()) slot.set(ItemStack.EMPTY); else slot.setChanged();
        }
        return itemstack;
    }
}