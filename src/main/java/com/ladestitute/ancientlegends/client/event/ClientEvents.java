package com.ladestitute.ancientlegends.client.event;

import com.ladestitute.ancientlegends.battle.core.BattleManager;
import com.ladestitute.ancientlegends.battle.gui.turn_order.TurnOrderRenderer;
import com.ladestitute.ancientlegends.client.AncientLegendsClient;
import com.ladestitute.ancientlegends.network.MoveSelectionPacket;
import net.minecraft.client.Minecraft;
import net.minecraft.client.player.LocalPlayer;
import net.neoforged.neoforge.network.PacketDistributor;

public class ClientEvents {

    //private static WildGuiRenderer battleGuiRenderer;

    public static void clientTick(Minecraft minecraft) {
        LocalPlayer player = minecraft.player;
        if (player == null) return;

        if (BattleManager.instance != null) {
            if (AncientLegendsClient.KEY_MOVE_1.consumeClick()) {
                System.out.println("KEY 1 PRESSED!");
                sendMovePacket(0);
            } else if (AncientLegendsClient.KEY_MOVE_2.consumeClick()) {
                sendMovePacket(1);
            } else if (AncientLegendsClient.KEY_MOVE_3.consumeClick()) {
               sendMovePacket(2);
            } else if (AncientLegendsClient.KEY_MOVE_4.consumeClick()) {
                sendMovePacket(3);
            }
            if(AncientLegendsClient.KEY_TOGGLE_TURN_ORDER.consumeClick()) {
                TurnOrderRenderer.toggleTurnOrderDisplay();
            }
        }
    }

    private static void sendMovePacket(int moveSlot) {
        MoveSelectionPacket message = new MoveSelectionPacket(moveSlot);
        PacketDistributor.SERVER.noArg().send(message);
        System.out.println("PACKET SENT");
    }


}
