package com.ladestitute.ancientlegends.client.event;

import com.ladestitute.ancientlegends.AncientLegendsMain;
import com.ladestitute.ancientlegends.api.IGuiGraphicsGetter;
import com.ladestitute.ancientlegends.battle.gui.entity_display.BarStates;
import com.ladestitute.ancientlegends.battle.gui.entity_display.EntityHealthBarRenderer;
import com.ladestitute.ancientlegends.battle.gui.entity_display.RayTrace;
import com.ladestitute.ancientlegends.battle.gui.turn_order.PlayerMoveSlotsRenderer;
import com.ladestitute.ancientlegends.battle.gui.turn_order.TurnOrderRenderer;
import com.ladestitute.ancientlegends.client.AncientLegendsClient;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.PokemonEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Camera;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.model.EntityModel;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.LivingEntity;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.neoforge.client.event.RegisterGuiOverlaysEvent;
import net.neoforged.neoforge.client.event.RenderLevelStageEvent;
import net.neoforged.neoforge.client.event.RenderLivingEvent;
import net.neoforged.neoforge.client.gui.overlay.VanillaGuiOverlay;
import net.neoforged.neoforge.common.NeoForge;
import net.neoforged.neoforge.event.TickEvent;

public class ClientEventHandler {
    public static void init(IEventBus bus) {
        NeoForge.EVENT_BUS.addListener(ClientEventHandler::entityRender);
        NeoForge.EVENT_BUS.addListener(ClientEventHandler::playerTick);
        NeoForge.EVENT_BUS.addListener(ClientEventHandler::renderParticles);
        bus.addListener(ClientEventHandler::registerOverlays);
    }

    private static void registerOverlays(final RegisterGuiOverlaysEvent event) {
        event.registerAbove(VanillaGuiOverlay.POTION_ICONS.id(), new ResourceLocation(AncientLegendsMain.MOD_ID, "battle_gui"), AncientLegendsClient.HUD::draw);
        event.registerAbove(VanillaGuiOverlay.POTION_ICONS.id(), new ResourceLocation(AncientLegendsMain.MOD_ID, "turn_order"), TurnOrderRenderer.getInstance());
        event.registerAbove(VanillaGuiOverlay.POTION_ICONS.id(), new ResourceLocation(AncientLegendsMain.MOD_ID, "player_moves"), PlayerMoveSlotsRenderer.getInstance());
    }

    private static void renderParticles(RenderLevelStageEvent event) {
        if (event.getStage() == RenderLevelStageEvent.Stage.AFTER_PARTICLES) {
            Camera camera = Minecraft.getInstance().gameRenderer.getMainCamera();
            final GuiGraphics graphics = ((IGuiGraphicsGetter) Minecraft.getInstance()).getGuiGraphics(event.getPoseStack());
            EntityHealthBarRenderer.renderInWorld(event.getPartialTick(), graphics, camera);
            graphics.flush();
        }
    }

    private static void entityRender(RenderLivingEvent.Post<? extends LivingEntity, ? extends EntityModel<?>> event) {
        if (!(event.getEntity() instanceof PokemonEntity)) {
            return;
        }
        EntityHealthBarRenderer.prepareRenderInWorld((PokemonEntity) event.getEntity());
    }

    private static void playerTick(TickEvent.PlayerTickEvent event) {
        if (!event.player.level().isClientSide) return;
        AncientLegendsClient.HUD.setEntity(RayTrace.getEntityInCrosshair(0, 60f));
        BarStates.tick();
        AncientLegendsClient.HUD.tick();
    }
}
