package com.ladestitute.ancientlegends.client;

import com.ladestitute.ancientlegends.client.model.BidoofModel;
import com.ladestitute.ancientlegends.client.model.StarlyModel;
import com.ladestitute.ancientlegends.client.render.BidoofRenderer;
import com.ladestitute.ancientlegends.client.render.StarlyRenderer;
import com.ladestitute.ancientlegends.client.screen.PartyScreen;
import com.ladestitute.ancientlegends.client.screen.SatchelScreen;
import com.ladestitute.ancientlegends.client.event.ClientEvents;
import com.ladestitute.ancientlegends.registry.EntityTypeInit;
import com.ladestitute.ancientlegends.registry.MenuInit;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.ThrownItemRenderer;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.Mod;
import net.neoforged.neoforge.client.event.EntityRenderersEvent;
import net.neoforged.neoforge.client.event.RegisterKeyMappingsEvent;
import net.neoforged.neoforge.client.event.RegisterMenuScreensEvent;
import net.neoforged.neoforge.common.NeoForge;
import net.neoforged.neoforge.event.TickEvent;

@Mod.EventBusSubscriber(value = Dist.CLIENT, bus = Mod.EventBusSubscriber.Bus.MOD)
public class ClientEventBusSubscriber {

    public static void init(IEventBus bus) {
        NeoForge.EVENT_BUS.addListener(ClientEventBusSubscriber::onClientTick);
    }

    @SubscribeEvent
    public static void onRegisterRenderers(EntityRenderersEvent.RegisterRenderers event) {
        event.registerEntityRenderer(EntityTypeInit.POKE_BALL.get(), ThrownItemRenderer::new);
        event.registerEntityRenderer(EntityTypeInit.BIDOOF.get(), BidoofRenderer::new);
        event.registerEntityRenderer(EntityTypeInit.STARLY.get(), StarlyRenderer::new);
    }

    @SubscribeEvent
    public static void registerLayerDefinitions(EntityRenderersEvent.RegisterLayerDefinitions event) {
        event.registerLayerDefinition(BidoofModel.LAYER_LOCATION, BidoofModel::createBodyLayer);
        event.registerLayerDefinition(StarlyModel.LAYER_LOCATION, StarlyModel::createBodyLayer);
    }

    @SubscribeEvent
    public static void screenEvent(RegisterMenuScreensEvent event) {
        event.register(MenuInit.SATCHEL.get(), SatchelScreen::new);
        event.register(MenuInit.PARTY_BAG.get(), PartyScreen::new);
    }

    @SubscribeEvent
    public static void onRegisterKeyBindings(RegisterKeyMappingsEvent event) {
        event.register(AncientLegendsClient.KEY_MOVE_1);
        event.register(AncientLegendsClient.KEY_MOVE_2);
        event.register(AncientLegendsClient.KEY_MOVE_3);
        event.register(AncientLegendsClient.KEY_MOVE_4);
        event.register(AncientLegendsClient.KEY_TOGGLE_TURN_ORDER);
    }

    private static void onClientTick(TickEvent.ClientTickEvent event) {
           ClientEvents.clientTick(Minecraft.getInstance());
    }

}
