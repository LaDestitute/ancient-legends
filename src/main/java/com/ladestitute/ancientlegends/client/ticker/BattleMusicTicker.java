package com.ladestitute.ancientlegends.client.ticker;

import com.ladestitute.ancientlegends.registry.SoundInit;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.sounds.SimpleSoundInstance;
import net.minecraft.client.resources.sounds.SoundInstance;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.level.Level;

public class BattleMusicTicker {
    private static SoundEvent BATTLE_MUSIC = SoundInit.MUSIC_WILD_BATTLE.get();

    public static void startBattleMusic(Level level) {
        if (level.isClientSide) {
            stopBackgroundMusic();
            Minecraft.getInstance().getSoundManager().play(SimpleSoundInstance.forMusic(BATTLE_MUSIC));
        }
    }

    public static void stopBattleMusic(Level level) {
        if (level.isClientSide) {
            stopBackgroundMusic();
        }
    }

    private static void stopBackgroundMusic() {
        Minecraft.getInstance().getMusicManager().stopPlaying();
    }
}
