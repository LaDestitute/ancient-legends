package com.ladestitute.ancientlegends.client.render;

import com.ladestitute.ancientlegends.AncientLegendsMain;
import com.ladestitute.ancientlegends.client.model.StarlyModel;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.skittish.StarlyEntity;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.resources.ResourceLocation;

import javax.annotation.Nullable;

public class StarlyRenderer extends MobRenderer<StarlyEntity, StarlyModel> {

    private static final ResourceLocation TEXTURE = new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/entity/starly.png");

    //In 1.18, we now pass a LAYER_LOCATION (which holds a string-ref to the model's body) and baked in
    //using EntityRendererProvider.Context in the entity's renderer and passed through to the constructor
    public StarlyRenderer(EntityRendererProvider.Context context) {
        super(context, new StarlyModel(context.getModelSet().bakeLayer(StarlyModel.LAYER_LOCATION)), 0.5f);
    }

    @Nullable
    @Override
    public ResourceLocation getTextureLocation(StarlyEntity entity) {
        return TEXTURE;
    }
}
