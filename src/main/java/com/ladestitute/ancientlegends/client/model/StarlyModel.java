package com.ladestitute.ancientlegends.client.model;

import com.ladestitute.ancientlegends.AncientLegendsMain;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.skittish.StarlyEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;

public class StarlyModel extends EntityModel<StarlyEntity> {
    public static final String BODY = "body";
    // This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
    public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation(AncientLegendsMain.MOD_ID, "starly"), BODY);
    private final ModelPart body;
    private final ModelPart wing0;
    private final ModelPart wing1;
    private final ModelPart leg1;
    private final ModelPart leg0;
    private final ModelPart tail;
    private final ModelPart head;

    public StarlyModel(ModelPart root) {
        this.body = root.getChild("body");
        this.wing0 = root.getChild("wing0");
        this.wing1 = root.getChild("wing1");
        this.leg1 = root.getChild("leg1");
        this.leg0 = root.getChild("leg0");
        this.tail = root.getChild("tail");
        this.head = root.getChild("head");
    }

    public static LayerDefinition createBodyLayer() {
        MeshDefinition meshdefinition = new MeshDefinition();
        PartDefinition partdefinition = meshdefinition.getRoot();

        PartDefinition body = partdefinition.addOrReplaceChild("body", CubeListBuilder.create().texOffs(0, 7).addBox(-2.5F, 1.0F, -1.5F, 5.0F, 6.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 16.5F, -3.0F, 0.4363F, 0.0F, 0.0F));

        PartDefinition wing0 = partdefinition.addOrReplaceChild("wing0", CubeListBuilder.create().texOffs(20, 8).addBox(0.5F, 2.0F, -1.5F, 1.0F, 5.0F, 3.0F, new CubeDeformation(0.0F))
                .texOffs(0, 19).addBox(0.5F, 4.0F, 1.5F, 1.0F, 3.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.5F, 15.9F, -0.8F, 0.1745F, 0.0F, 0.0F));

        PartDefinition wing1 = partdefinition.addOrReplaceChild("wing1", CubeListBuilder.create().texOffs(20, 8).addBox(-1.5F, 2.0F, -1.5F, 1.0F, 5.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-1.5F, 15.9F, -0.8F, 0.1745F, 0.0F, 0.0F));

        PartDefinition wing2_r1 = wing1.addOrReplaceChild("wing2_r1", CubeListBuilder.create().texOffs(0, 19).addBox(2.0F, -3.0F, -0.7F, 1.0F, 3.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.5F, 7.1F, 2.8F, -3.1416F, 0.0F, 3.1416F));

        PartDefinition leg1 = partdefinition.addOrReplaceChild("leg1", CubeListBuilder.create().texOffs(14, 18).addBox(-1.0F, 0.5F, -1.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(-0.5F, 22.5F, -0.5F));

        PartDefinition leg0 = partdefinition.addOrReplaceChild("leg0", CubeListBuilder.create().texOffs(14, 18).addBox(-1.0F, 0.5F, -1.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(1.5F, 22.5F, -0.5F));

        PartDefinition tail = partdefinition.addOrReplaceChild("tail", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 21.1F, 1.2F, 0.8727F, 0.0F, 0.0F));

        PartDefinition tail_r1 = tail.addOrReplaceChild("tail_r1", CubeListBuilder.create().texOffs(22, 1).addBox(-0.5F, 2.1F, 2.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(20, 19).addBox(-0.5F, 3.1F, 2.2F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(23, 2).addBox(-1.5F, 3.1F, 2.2F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(23, 2).addBox(0.5F, 3.1F, 2.2F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 2.9F, -1.2F, 1.3963F, 0.0F, 0.0F));

        PartDefinition head = partdefinition.addOrReplaceChild("head", CubeListBuilder.create().texOffs(16, 27).addBox(-1.0F, -0.5F, -1.0F, 2.0F, 3.0F, 2.0F, new CubeDeformation(0.0F))
                .texOffs(6, 7).addBox(-1.0F, -0.5F, 0.0F, 2.0F, 3.0F, 3.0F, new CubeDeformation(0.0F))
                .texOffs(24, 17).addBox(-2.0F, -0.5F, -1.0F, 1.0F, 3.0F, 3.0F, new CubeDeformation(0.0F))
                .texOffs(9, 21).addBox(1.0F, -0.5F, -1.0F, 1.0F, 3.0F, 3.0F, new CubeDeformation(0.0F))
                .texOffs(24, 29).addBox(-1.0F, -1.5F, -0.8F, 2.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
                .texOffs(11, 7).addBox(-0.5F, 0.5F, -1.9F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(0, 23).addBox(0.0F, -4.8F, -1.1F, 0.0F, 5.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 15.7F, -2.8F));

        return LayerDefinition.create(meshdefinition, 32, 32);
    }

    @Override
    public void setupAnim(StarlyEntity p_225597_1_, float limbSwing, float limbSwingAmount, float ageInTicks,
                          float netHeadYaw, float headPitch) {
        this.head.xRot = headPitch * ((float)Math.PI / 180F);
        this.head.yRot = netHeadYaw * ((float)Math.PI / 180F);
        this.wing0.yRot = Mth.cos(limbSwing * 2.6662F + (float)Math.PI) * 2.4F * limbSwingAmount;
        this.wing1.yRot = Mth.cos(limbSwing * 2.6662F) * 2.4F * limbSwingAmount;
    }

    @Override
    public void renderToBuffer(PoseStack poseStack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
        body.render(poseStack, buffer, packedLight, packedOverlay);
        wing0.render(poseStack, buffer, packedLight, packedOverlay);
        wing1.render(poseStack, buffer, packedLight, packedOverlay);
        leg1.render(poseStack, buffer, packedLight, packedOverlay);
        leg0.render(poseStack, buffer, packedLight, packedOverlay);
        tail.render(poseStack, buffer, packedLight, packedOverlay);
        head.render(poseStack, buffer, packedLight, packedOverlay);
    }
}