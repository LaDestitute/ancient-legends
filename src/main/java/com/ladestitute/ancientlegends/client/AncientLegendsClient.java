package com.ladestitute.ancientlegends.client;

import com.ladestitute.ancientlegends.battle.gui.entity_display.Hud;
import com.ladestitute.ancientlegends.client.event.ClientEventHandler;
import com.mojang.blaze3d.platform.InputConstants;
import net.minecraft.client.KeyMapping;
import net.minecraft.network.chat.Component;
import net.neoforged.bus.api.IEventBus;

public class AncientLegendsClient {
    public static final Hud HUD = new Hud();

    public static void init(IEventBus bus) {
        ClientEventHandler.init(bus);
    }

    public static final Component AL_CATEGORY = Component.translatable("key.categories.ancient_legends");
    public static final Component BATTLE_MOVE_1 = Component.translatable("key.ancient_legends.battle_move_1");
    public static final Component BATTLE_MOVE_2 = Component.translatable("key.ancient_legends.battle_move_2");
    public static final Component BATTLE_MOVE_3 = Component.translatable("key.ancient_legends.battle_move_3");
    public static final Component BATTLE_MOVE_4 = Component.translatable("key.ancient_legends.battle_move_4");
    public static final Component TOGGLE_TURN_ORDER = Component.translatable("key.ancient_legends.toggle_turn_order");

    public static final KeyMapping KEY_MOVE_1 = new KeyMapping(
            BATTLE_MOVE_1.getString(),
            InputConstants.KEY_NUMPAD1,
            AL_CATEGORY.getString());

    public static final KeyMapping KEY_MOVE_2 = new KeyMapping(
            BATTLE_MOVE_2.getString(),
            InputConstants.KEY_NUMPAD2,
            AL_CATEGORY.getString());

    public static final KeyMapping KEY_MOVE_3 = new KeyMapping(
            BATTLE_MOVE_3.getString(),
            InputConstants.KEY_NUMPAD3,
            AL_CATEGORY.getString());

    public static final KeyMapping KEY_MOVE_4 = new KeyMapping(
            BATTLE_MOVE_4.getString(),
            InputConstants.KEY_NUMPAD4,
            AL_CATEGORY.getString());

    public static final KeyMapping KEY_TOGGLE_TURN_ORDER = new KeyMapping(
            TOGGLE_TURN_ORDER.getString(),
            InputConstants.KEY_Y,
            AL_CATEGORY.getString());
}
