package com.ladestitute.ancientlegends.client.screen;

import com.ladestitute.ancientlegends.container.SatchelMenu;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;

import javax.annotation.Nonnull;

public class SatchelScreen extends AbstractContainerScreen<SatchelMenu> {
    private static final ResourceLocation GUI = new ResourceLocation("ancientlegends", "textures/gui/satchel.png");

    public SatchelScreen(SatchelMenu container, Inventory playerInventory, Component name) {
        super(container, playerInventory, name);
    }

    @Override
    protected void init() {
        super.init();
        this.titleLabelX = (this.imageWidth - this.font.width(this.title)) / 2;
    }

    @Override
    protected void renderBg(@Nonnull GuiGraphics gg, float partialTicks, int x, int y) {
        gg.blit(GUI, this.leftPos, this.topPos, 0, 0, this.imageWidth, this.imageHeight);
    }

    @Override
    protected void renderLabels(@Nonnull GuiGraphics gg, int x, int y) {
        gg.drawString(font, this.title.getString(), this.titleLabelX, 6, 0x404040, false);
    }

    @Override
    public void render(@Nonnull GuiGraphics gg, int pMouseX, int pMouseY, float pPartialTicks) {
        this.renderBackground(gg, pMouseX, pMouseY, pPartialTicks);
        super.render(gg, pMouseX, pMouseY, pPartialTicks);
        this.renderTooltip(gg, pMouseX, pMouseY);
    }
}