package com.ladestitute.ancientlegends.registry;

import com.ladestitute.ancientlegends.AncientLegendsMain;
import com.ladestitute.ancientlegends.blocks.ApricornTreeBlock;
import com.ladestitute.ancientlegends.blocks.DepletedTumblestoneOreBlock;
import com.ladestitute.ancientlegends.blocks.EmptyApricornTreeBlock;
import com.ladestitute.ancientlegends.blocks.TumblestoneOreBlock;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.world.level.block.Block;
import net.neoforged.neoforge.registries.DeferredHolder;
import net.neoforged.neoforge.registries.DeferredRegister;

public class BlockInit {
    public static DeferredRegister<Block> BLOCKS = DeferredRegister.create(BuiltInRegistries.BLOCK, AncientLegendsMain.MOD_ID);

    public static DeferredHolder<Block, Block> TUMBLESTONE_ORE = BLOCKS.register("tumblestone_ore", () ->
            new TumblestoneOreBlock(PropertiesInit.TUMBLESTONE_ORE));
    public static DeferredHolder<Block, Block> DEPLETED_TUMBLESTONE_ORE = BLOCKS.register("depleted_tumblestone_ore", () ->
            new DepletedTumblestoneOreBlock(PropertiesInit.TUMBLESTONE_ORE));
    public static DeferredHolder<Block, Block> APRICORN_TREE = BLOCKS.register("apricorn_tree", () ->
            new ApricornTreeBlock(PropertiesInit.APRICORN_TREE));
    public static DeferredHolder<Block, Block> EMPTY_APRICORN_TREE = BLOCKS.register("empty_apricorn_tree", () ->
            new EmptyApricornTreeBlock(PropertiesInit.APRICORN_TREE));

}
