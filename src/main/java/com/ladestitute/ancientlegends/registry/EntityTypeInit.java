package com.ladestitute.ancientlegends.registry;

import com.ladestitute.ancientlegends.AncientLegendsMain;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.passive.BidoofEntity;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.skittish.StarlyEntity;
import com.ladestitute.ancientlegends.entities.pokeballs.PokeBallEntity;
import com.ladestitute.ancientlegends.entities.pokeballs.PokeBallItemEntity;
import net.minecraft.core.registries.Registries;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import net.neoforged.neoforge.registries.DeferredHolder;
import net.neoforged.neoforge.registries.DeferredRegister;

public class EntityTypeInit {

    public static final DeferredRegister<EntityType<?>> ENTITY_TYPES =
            DeferredRegister.create(Registries.ENTITY_TYPE, AncientLegendsMain.MOD_ID);

    //Poke Balls
    public static DeferredHolder<EntityType<?>, EntityType<PokeBallItemEntity>> POKE_BALL_ITEM = ENTITY_TYPES.register(
            "poke_ball_item",
            () -> EntityType.Builder.<PokeBallItemEntity>of((entity, level) ->
                            new PokeBallItemEntity(level), MobCategory.MISC)
                    .sized(1F, 1F)
                    .build("poke_ball_item"));
    public static DeferredHolder<EntityType<?>, EntityType<PokeBallEntity>> POKE_BALL = ENTITY_TYPES.register(
            "poke_ball",
            () -> EntityType.Builder.<PokeBallEntity>of((entity, level) ->
                            new PokeBallEntity(level), MobCategory.MISC)
                    .sized(1F, 1F)
                    .build("poke_ball"));

    //Pokemon
    public static final DeferredHolder<EntityType<?>, EntityType<BidoofEntity>> BIDOOF = ENTITY_TYPES.register("bidoof", () ->
            EntityType.Builder.of(BidoofEntity::new, MobCategory.CREATURE)
                    .sized(1F, 1F)
                    .build(AncientLegendsMain.MOD_ID + "bidoof"));
    public static final DeferredHolder<EntityType<?>, EntityType<StarlyEntity>> STARLY = ENTITY_TYPES.register("starly", () ->
            EntityType.Builder.of(StarlyEntity::new, MobCategory.CREATURE)
                    .sized(1F, 1F)
                    .build(AncientLegendsMain.MOD_ID + "starly"));

}

