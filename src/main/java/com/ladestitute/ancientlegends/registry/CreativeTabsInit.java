package com.ladestitute.ancientlegends.registry;

import com.ladestitute.ancientlegends.AncientLegendsMain;
import net.minecraft.core.registries.Registries;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.ItemStack;
import net.neoforged.neoforge.registries.DeferredHolder;
import net.neoforged.neoforge.registries.DeferredRegister;

public class CreativeTabsInit {
    public static final DeferredRegister<CreativeModeTab> CREATIVE_MODE_TABS = DeferredRegister.create(Registries.CREATIVE_MODE_TAB, AncientLegendsMain.MOD_ID);

    public static final DeferredHolder<CreativeModeTab, CreativeModeTab> BLOCKS_TAB = CREATIVE_MODE_TABS.register("ancientlegends_blocks", () -> CreativeModeTab.builder()
            .title(Component.translatable("ancientlegends_blocks")) //The language key for the title of your CreativeModeTab
            .icon(() -> new ItemStack(ItemInit.APRICORN_TREE_BLOCK.get()))
            .displayItems((parameters, output) -> {
                output.accept(ItemInit.TUMBLESTONE_ORE_BLOCK.get());
                output.accept(ItemInit.DEPLETED_TUMBLESTONE_ORE_BLOCK.get());
                output.accept(ItemInit.APRICORN_TREE_BLOCK.get());// Add the example item to the tab. For your own tabs, this method is preferred over the event
                output.accept(ItemInit.EMPTY_APRICORN_TREE_BLOCK.get());
            }).build());

    public static final DeferredHolder<CreativeModeTab, CreativeModeTab> UTIL_TAB = CREATIVE_MODE_TABS.register("ancientlegends_utility", () -> CreativeModeTab.builder()
            .title(Component.translatable("ancientlegends_utility")) //The language key for the title of your CreativeModeTab
            .icon(() -> new ItemStack(ItemInit.CRAFTING_KIT.get()))
            .displayItems((parameters, output) -> {
                output.accept(ItemInit.SATCHEL.get());
                output.accept(ItemInit.PARTY_BAG.get());
                output.accept(ItemInit.CRAFTING_KIT.get());
            }).build());

    public static final DeferredHolder<CreativeModeTab, CreativeModeTab> MATERIALS_TAB = CREATIVE_MODE_TABS.register("ancientlegends_materials", () -> CreativeModeTab.builder()
            .title(Component.translatable("ancientlegends_materials")) //The language key for the title of your CreativeModeTab
            .icon(() -> new ItemStack(ItemInit.APRICORN.get()))
            .displayItems((parameters, output) -> {
                output.accept(ItemInit.APRICORN.get());
                output.accept(ItemInit.TUMBLESTONE.get());
            }).build());

    public static final DeferredHolder<CreativeModeTab, CreativeModeTab> POKE_BALLS_TAB = CREATIVE_MODE_TABS.register("ancientlegends_pokeballs", () -> CreativeModeTab.builder()
            .title(Component.translatable("ancientlegends_pokeballs")) //The language key for the title of your CreativeModeTab
            .icon(() -> new ItemStack(ItemInit.POKE_BALL.get()))
            .displayItems((parameters, output) -> {
                output.accept(ItemInit.POKE_BALL.get());
            }).build());

    public static final DeferredHolder<CreativeModeTab, CreativeModeTab> POKEMON_TAB = CREATIVE_MODE_TABS.register("ancientlegends_spawneggs", () -> CreativeModeTab.builder()
            .title(Component.translatable("ancientlegends_spawneggs")) //The language key for the title of your CreativeModeTab
            .icon(() -> new ItemStack(ItemInit.BIDOOF_SPAWN_EGG.get()))
            .displayItems((parameters, output) -> {
                output.accept(ItemInit.BIDOOF_SPAWN_EGG.get());
                output.accept(ItemInit.STARLY_SPAWN_EGG.get());
            }).build());
}
