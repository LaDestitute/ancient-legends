package com.ladestitute.ancientlegends.registry;

import com.ladestitute.ancientlegends.AncientLegendsMain;
import com.ladestitute.ancientlegends.items.materials.ApricornItem;
import com.ladestitute.ancientlegends.items.materials.TumblestoneItem;
import com.ladestitute.ancientlegends.items.pokeballs.PokeBallItem;
import com.ladestitute.ancientlegends.items.utilities.CraftingKitItem;
import com.ladestitute.ancientlegends.items.utilities.PartyBagItem;
import com.ladestitute.ancientlegends.items.utilities.PokeScannerItem;
import com.ladestitute.ancientlegends.items.utilities.SatchelItem;
import net.minecraft.core.registries.Registries;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.neoforged.neoforge.common.DeferredSpawnEggItem;
import net.neoforged.neoforge.registries.DeferredHolder;
import net.neoforged.neoforge.registries.DeferredRegister;

public class ItemInit {
    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(Registries.ITEM, AncientLegendsMain.MOD_ID);

    //Helper/spawneggs
    public static final DeferredHolder<Item, DeferredSpawnEggItem> BIDOOF_SPAWN_EGG = ITEMS.register("bidoof_spawn_egg",
            () -> new DeferredSpawnEggItem(EntityTypeInit.BIDOOF, 16645629, 16645629, new Item.Properties()));
    public static final DeferredHolder<Item, DeferredSpawnEggItem> STARLY_SPAWN_EGG = ITEMS.register("starly_spawn_egg",
            () -> new DeferredSpawnEggItem(EntityTypeInit.STARLY, 16645629, 16645629, new Item.Properties()));

    //Materials
    public static DeferredHolder<Item, Item> APRICORN = ITEMS.register("apricorn",
            () -> new ApricornItem(new Item.Properties()));
    public static DeferredHolder<Item, Item> TUMBLESTONE = ITEMS.register("tumblestone",
            () -> new TumblestoneItem(new Item.Properties()));

    //Utility
    public static DeferredHolder<Item, Item> SATCHEL = ITEMS.register("satchel",
            () -> new SatchelItem(new Item.Properties()));
    public static DeferredHolder<Item, Item> PARTY_BAG = ITEMS.register("party_bag",
            () -> new PartyBagItem(new Item.Properties()));
    public static DeferredHolder<Item, Item> CRAFTING_KIT = ITEMS.register("crafting_kit",
            () -> new CraftingKitItem(new Item.Properties()));
    public static DeferredHolder<Item, Item> POKE_SCANNER = ITEMS.register("poke_scanner",
            () -> new PokeScannerItem(new Item.Properties()));

    //Poke Balls
    public static DeferredHolder<Item, Item> POKE_BALL = ITEMS.register("poke_ball",
            () -> new PokeBallItem(new Item.Properties().fireResistant()));

    //Blockitems
    public static final DeferredHolder<Item, BlockItem> TUMBLESTONE_ORE_BLOCK =
            ITEMS.register("tumblestone_ore", () -> new BlockItem(BlockInit.TUMBLESTONE_ORE.get(), new Item.Properties()));
    public static final DeferredHolder<Item, BlockItem> DEPLETED_TUMBLESTONE_ORE_BLOCK =
            ITEMS.register("depleted_tumblestone_ore", () -> new BlockItem(BlockInit.DEPLETED_TUMBLESTONE_ORE.get(), new Item.Properties()));
    public static final DeferredHolder<Item, BlockItem> APRICORN_TREE_BLOCK =
            ITEMS.register("apricorn_tree", () -> new BlockItem(BlockInit.APRICORN_TREE.get(), new Item.Properties()));
    public static final DeferredHolder<Item, BlockItem> EMPTY_APRICORN_TREE_BLOCK =
            ITEMS.register("empty_apricorn_tree", () -> new BlockItem(BlockInit.EMPTY_APRICORN_TREE.get(), new Item.Properties()));
}
