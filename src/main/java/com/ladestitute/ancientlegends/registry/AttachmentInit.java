package com.ladestitute.ancientlegends.registry;

import com.ladestitute.ancientlegends.AncientLegendsMain;
import com.ladestitute.ancientlegends.capability.playertracking.PlayerTrackingCapability;
import com.ladestitute.ancientlegends.capability.pokedata.PokeDataCapability;
import net.neoforged.neoforge.attachment.AttachmentType;
import net.neoforged.neoforge.registries.DeferredRegister;
import net.neoforged.neoforge.registries.NeoForgeRegistries;

import java.util.function.Supplier;

public class AttachmentInit {
    public static final DeferredRegister<AttachmentType<?>> ATTACHMENT_TYPES = DeferredRegister.create(NeoForgeRegistries.Keys.ATTACHMENT_TYPES, AncientLegendsMain.MOD_ID);

    public static final Supplier<AttachmentType<PlayerTrackingCapability>> PLAYER_DATA = ATTACHMENT_TYPES.register(
            "al_playerdata", () -> AttachmentType.serializable(PlayerTrackingCapability::new).build());

    public static final Supplier<AttachmentType<PokeDataCapability>> POKE_DATA = ATTACHMENT_TYPES.register(
            "al_pokedata", () -> AttachmentType.serializable(PokeDataCapability::new).build());
}
