package com.ladestitute.ancientlegends.registry;

import com.ladestitute.ancientlegends.AncientLegendsMain;
import com.ladestitute.ancientlegends.container.PartyMenu;
import com.ladestitute.ancientlegends.container.SatchelMenu;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.world.inventory.MenuType;
import net.neoforged.neoforge.common.extensions.IMenuTypeExtension;
import net.neoforged.neoforge.registries.DeferredHolder;
import net.neoforged.neoforge.registries.DeferredRegister;

public class MenuInit {
    public static DeferredRegister<MenuType<?>> MENUS = DeferredRegister.create(BuiltInRegistries.MENU, AncientLegendsMain.MOD_ID);

    public static final DeferredHolder<MenuType<?>, MenuType<SatchelMenu>> SATCHEL =
            MENUS.register("satchel", () -> IMenuTypeExtension.create(SatchelMenu::fromNetwork));

    public static final DeferredHolder<MenuType<?>, MenuType<PartyMenu>> PARTY_BAG =
            MENUS.register("party_bag", () -> IMenuTypeExtension.create(PartyMenu::fromNetwork));
}
