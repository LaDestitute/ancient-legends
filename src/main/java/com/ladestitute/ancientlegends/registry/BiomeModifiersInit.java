package com.ladestitute.ancientlegends.registry;

import com.ladestitute.ancientlegends.AncientLegendsMain;
import com.ladestitute.ancientlegends.world.biomemod.AncientLegendsBiomeModifier;
import com.mojang.serialization.Codec;
import net.neoforged.neoforge.common.world.BiomeModifier;
import net.neoforged.neoforge.registries.DeferredRegister;
import net.neoforged.neoforge.registries.NeoForgeRegistries;

import java.util.function.Supplier;

public class BiomeModifiersInit {
    public static final DeferredRegister<Codec<? extends BiomeModifier>> BIOME_MODIFIER_SERIALIZERS = DeferredRegister.create(NeoForgeRegistries.Keys.BIOME_MODIFIER_SERIALIZERS, AncientLegendsMain.MOD_ID);

    public static final Supplier<Codec<AncientLegendsBiomeModifier>> POKEMON_ENTITY_MODIFIER_TYPE = BIOME_MODIFIER_SERIALIZERS.register("pokemon_entity_modifier", () -> Codec.unit(AncientLegendsBiomeModifier.INSTANCE));
}
