package com.ladestitute.ancientlegends;

import com.ladestitute.ancientlegends.battle.api.LearnListRegistryHelper;
import com.ladestitute.ancientlegends.battle.registry.PokemonMoveInit;
import com.ladestitute.ancientlegends.battle.api.MoveRegistryHelper;
import com.ladestitute.ancientlegends.battle.api.commands.LoadMovesCommand;
import com.ladestitute.ancientlegends.battle.api.commands.SaveMovesCommand;
import com.ladestitute.ancientlegends.client.AncientLegendsClient;
import com.ladestitute.ancientlegends.client.ClientEventBusSubscriber;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.passive.BidoofEntity;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.skittish.StarlyEntity;
import com.ladestitute.ancientlegends.events.AddPokeDataHandler;
import com.ladestitute.ancientlegends.events.BattleEvents;
import com.ladestitute.ancientlegends.events.MainEventHandler;
import com.ladestitute.ancientlegends.events.PlayerHandler;
import com.ladestitute.ancientlegends.network.MoveSelectionPacket;
import com.ladestitute.ancientlegends.network.PlayerDataPacket;
import com.ladestitute.ancientlegends.network.SyncPokeDataPacket;
import com.ladestitute.ancientlegends.registry.*;
import net.minecraft.core.Direction;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.Mod;
import net.neoforged.fml.event.lifecycle.FMLCommonSetupEvent;
import net.neoforged.fml.loading.FMLEnvironment;
import net.neoforged.neoforge.common.NeoForge;
import net.neoforged.neoforge.event.RegisterCommandsEvent;
import net.neoforged.neoforge.event.entity.EntityAttributeCreationEvent;
import net.neoforged.neoforge.network.event.RegisterPayloadHandlerEvent;
import net.neoforged.neoforge.network.registration.IPayloadRegistrar;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Locale;

@Mod(AncientLegendsMain.MOD_ID)
public class AncientLegendsMain {
    public static final String MOD_NAME = "Ancient Legends";
    public static final String MOD_ID = "ancientlegends";
    public static final Logger LOGGER = LogManager.getLogger(AncientLegendsMain.class);

    public AncientLegendsMain(IEventBus modEventBus) {
        // Register moves first
        if (FMLEnvironment.dist.isClient()) {
            ClientEventBusSubscriber.init(modEventBus);
            AncientLegendsClient.init(modEventBus);
        }

        PokemonMoveInit.POKEMON_MOVES.register(modEventBus);
        modEventBus.addListener(this::commonSetup);
        modEventBus.addListener(this::postSetup);

        // Register learn lists after moves are registered

        modEventBus.addListener(this::setupPackets);
        NeoForge.EVENT_BUS.addListener(this::registerCommands);
        NeoForge.EVENT_BUS.register(new PlayerHandler());
        NeoForge.EVENT_BUS.register(new AddPokeDataHandler());
        NeoForge.EVENT_BUS.register(new BattleEvents());
        NeoForge.EVENT_BUS.register(new MainEventHandler());

        // Register custom registries
        CreativeTabsInit.CREATIVE_MODE_TABS.register(modEventBus);
        AttachmentInit.ATTACHMENT_TYPES.register(modEventBus);
        SoundInit.SOUNDS.register(modEventBus);
        ItemInit.ITEMS.register(modEventBus);
        BlockInit.BLOCKS.register(modEventBus);
        MenuInit.MENUS.register(modEventBus);
        EntityTypeInit.ENTITY_TYPES.register(modEventBus);
        BiomeModifiersInit.BIOME_MODIFIER_SERIALIZERS.register(modEventBus);

        // Register attribute listener after entity types
        modEventBus.addListener(this::addAttributes);

    }

    private void commonSetup(final FMLCommonSetupEvent event) {
        event.enqueueWork(() -> {
            LOGGER.info("Initializing Move API...");
            PokemonMoveInit.initMoveAPI();
            MoveRegistryHelper.initializeRegistry();
            MoveRegistryHelper moveregistryhelper = new MoveRegistryHelper();
          //  moveregistryhelper.saveMovesToJson();
           // LearnListRegistryHelper.saveLearnLists();
           moveregistryhelper.loadMovesFromJson();
            LearnListRegistryHelper.loadLearnLists();
        });
        //moveregistryhelper.loadMovesFromJson();


    }

    public static VoxelShape calculateShapes(Direction to, VoxelShape shape) {
        final VoxelShape[] buffer = { shape, Shapes.empty() };

        final int times = (to.get2DDataValue() - Direction.NORTH.get2DDataValue() + 4) % 4;
        for (int i = 0; i < times; i++) {
            buffer[0].forAllBoxes((minX, minY, minZ, maxX, maxY, maxZ) -> buffer[1] = Shapes.or(buffer[1],
                    Shapes.create(1 - maxZ, minY, minX, 1 - minZ, maxY, maxX)));
            buffer[0] = buffer[1];
            buffer[1] = Shapes.empty();
        }

        return buffer[0];
    }

    @SubscribeEvent
    private void addAttributes(final EntityAttributeCreationEvent event) {
        event.put(EntityTypeInit.BIDOOF.get(), BidoofEntity.customAttributes().build());
        event.put(EntityTypeInit.STARLY.get(), StarlyEntity.customAttributes().build());
    }

    public void setupPackets(RegisterPayloadHandlerEvent event) {
        IPayloadRegistrar registrar = event.registrar(MOD_ID).versioned("1.0.0").optional();
        registrar.play(PlayerDataPacket.ID, PlayerDataPacket::new, payload -> payload.client(PlayerDataPacket::handle));
        registrar.play(MoveSelectionPacket.ID, MoveSelectionPacket::new, payload -> payload.server(MoveSelectionPacket::handle));
        registrar.play(SyncPokeDataPacket.ID, SyncPokeDataPacket::new, payload -> payload.server(SyncPokeDataPacket::handle));
    }

    public static ResourceLocation prefix(String name) {
        return new ResourceLocation(AncientLegendsMain.MOD_ID, name.toLowerCase(Locale.ROOT));
    }

    public void registerCommands(RegisterCommandsEvent event) {
        SaveMovesCommand.register(event.getDispatcher());
        LoadMovesCommand.register(event.getDispatcher());
    }

    private void postSetup(final FMLCommonSetupEvent event) {
        event.enqueueWork(() -> {
            LOGGER.info("Initializing Learn List API...");
        });
    }
}
