package com.ladestitute.ancientlegends.mixin;

import com.ladestitute.ancientlegends.api.IGuiGraphicsGetter;
import com.ladestitute.ancientlegends.api.IGuiGraphicsSetter;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.renderer.RenderBuffers;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(Minecraft.class)
public abstract class MixinMinecraft implements IGuiGraphicsGetter {

    @Shadow
    @Final
    private RenderBuffers renderBuffers;

    private GuiGraphics getGuiGraphicsRaw() {
        return new GuiGraphics((Minecraft) (Object) this, renderBuffers.bufferSource());
    }

    @Override
    public GuiGraphics getGuiGraphics(PoseStack pose) {
        final GuiGraphics graphics = getGuiGraphicsRaw();
        ((IGuiGraphicsSetter) graphics).setPose(pose);
        return graphics;
    }
}