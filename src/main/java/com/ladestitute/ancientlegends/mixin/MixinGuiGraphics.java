package com.ladestitute.ancientlegends.mixin;

import com.ladestitute.ancientlegends.api.IGuiGraphicsSetter;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.gui.GuiGraphics;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(GuiGraphics.class)
public abstract class MixinGuiGraphics implements IGuiGraphicsSetter {

    @Mutable
    @Shadow
    @Final
    private PoseStack pose;

    @Override
    public void setPose(PoseStack pose) {
        this.pose = pose;
    }
}
