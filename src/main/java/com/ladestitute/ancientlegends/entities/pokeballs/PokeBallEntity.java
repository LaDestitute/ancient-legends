package com.ladestitute.ancientlegends.entities.pokeballs;

import com.ladestitute.ancientlegends.battle.constructors.PokemonMove;
import com.ladestitute.ancientlegends.battle.core.BattleManager;
import com.ladestitute.ancientlegends.capability.pokedata.PokeDataCapability;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.PokemonEntity;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.base.PassivePokemonEntity;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.base.SkittishPokemonEntity;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.passive.BidoofEntity;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.skittish.StarlyEntity;
import com.ladestitute.ancientlegends.items.pokeballs.PokeBallItem;
import com.ladestitute.ancientlegends.network.SyncPokeDataPacket;
import com.ladestitute.ancientlegends.registry.AttachmentInit;
import com.ladestitute.ancientlegends.registry.EntityTypeInit;
import com.ladestitute.ancientlegends.registry.ItemInit;
import net.minecraft.core.BlockPos;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.TamableAnimal;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.ThrowableItemProjectile;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.EntityHitResult;
import net.minecraft.world.phys.HitResult;
import net.neoforged.neoforge.network.PacketDistributor;

import java.util.*;

public class PokeBallEntity extends ThrowableItemProjectile {
    public ItemStack stack = ItemStack.EMPTY;

    public PokeBallEntity(Level world) {
        super(EntityTypeInit.POKE_BALL.get(), world);
    }

    public PokeBallEntity(double x, double y, double z, Level world) {
        super(EntityTypeInit.POKE_BALL.get(), x, y, z, world);
    }

    public PokeBallEntity(LivingEntity shooter, Level world) {
        super(EntityTypeInit.POKE_BALL.get(), shooter, world);
    }


    @Override
    protected Item getDefaultItem() {
        return ItemInit.POKE_BALL.get().asItem();
    }

    @Override
    public Packet<ClientGamePacketListener> getAddEntityPacket() {
        return super.getAddEntityPacket();
    }

    @Override
    protected void onHit(HitResult result) {
        if (result.getType() == HitResult.Type.ENTITY) {
            EntityHitResult entityRayTrace = (EntityHitResult) result;
            Entity target = entityRayTrace.getEntity();
            if (target instanceof PokemonEntity) {
                PokemonEntity pokemon = (PokemonEntity) target;
                ItemStack itemStack = this.getItem();
                if (itemStack.hasTag() && itemStack.getTag().contains("CapturedPokemon")) {
                    // Handle caught Pokémon ball logic
                    if (pokemon.isTame()) {
                        handleCaughtPokemonHit(pokemon);
                    } else {
                        Player player = (Player) this.getOwner();
                        if (player != null) {
                            //
                            CompoundTag itemTag = itemStack.getTag();
                            CompoundTag capturedData = itemTag.getCompound("CapturedPokemon");
                            String registryName = capturedData.getString("RegistryName");
                            EntityType<?> entityType = BuiltInRegistries.ENTITY_TYPE.get(new ResourceLocation(registryName));
                            if (entityType != null && entityType.create(level()) instanceof PokemonEntity) {
                                PokemonEntity userPokemon = (PokemonEntity) entityType.create(level());
                                Level level = player.level();
                                if (userPokemon != null && !level.isClientSide()) {
                                    userPokemon.deserializeNBT(capturedData);
                                    boolean isBackstrike = isBackstrike(player, pokemon);
                                    level.addFreshEntity(userPokemon);
                                    BattleManager.initiateBattle(level, player, userPokemon, pokemon, isBackstrike);
                                }
                            }
                            //
                        }
                    }
                } else {
                    // Proceed with the normal capturing calculation
                    handlePokemonCatch(pokemon);
                }
            } else if (target instanceof Player) {
                handlePlayerHit((Player) target);
            }
        } else if (result.getType() == HitResult.Type.BLOCK) {
            handleBlockHit((BlockHitResult) result);
        }

        if (!level().isClientSide) {
            this.discard();
        }
    }

    private static void syncStatsToTrackingPlayers(PokemonEntity pokemon) {
        if(!pokemon.level().isClientSide()) {
            PokeDataCapability data = pokemon.getData(AttachmentInit.POKE_DATA);
            SyncPokeDataPacket message = new SyncPokeDataPacket(pokemon, data.GetPokemonLevel(), data.GetPokemonGender());
            PacketDistributor.TRACKING_ENTITY.with(pokemon).send(message);
        }
    }

    private void handleCaughtPokemonHit(PokemonEntity pokemon) {
        ItemStack itemStack = this.getItem();
        CompoundTag itemTag = itemStack.getOrCreateTag();
        if (itemTag.contains("CapturedPokemon")) {
            CompoundTag capturedData = itemTag.getCompound("CapturedPokemon");
            int ballID = capturedData.getInt("BallID");
            if (pokemon.getBallID() == ballID) {
                pokemon.discard();
                itemTag.putBoolean("IsOutOfBall", false);
            } else {
                Player player = (Player) this.getOwner();
                if (player != null) {
                    player.sendSystemMessage(Component.literal("This isn't this Pokémon's ball!"));
                }
            }
        }
    }

    private void handlePokemonCatch(PokemonEntity pokemon) {
        Player player = (Player) this.getOwner();
        if (player == null) return;

        boolean isBackstrike = isBackstrike(player, pokemon);
        double catchRateModifier = isBackstrike ? 1.75 : 0.75;
        double finalCatchRate = pokemon.getCatchrate() * catchRateModifier;

        if (new Random().nextDouble() * 255 <= finalCatchRate) {
            System.out.println("Pokemon caught");
            pokemon.setTame(true);
            pokemon.setOwnerUUID(player.getUUID());
            pokemon.setPersistenceRequired();
            this.level().broadcastEntityEvent(this, (byte) 7);

            // Create a new instance of the correct subclass
            pokemon.setMoveSlots(new ArrayList<>(pokemon.getMoveSlots())); // Ensure moves are copied
            pokemon.setTame(true);
            pokemon.setOwnerUUID(player.getUUID());
            pokemon.setPersistenceRequired();

            // Call the modified spawnCapturedPokeBall method with the caughtPokemon
            spawnCapturedPokeBall(pokemon, player);

            // Check if a battle is running and set battleOver to true
            if (BattleManager.instance != null && !BattleManager.instance.battleOver) {
                BattleManager.instance.battleOver = true;
                BattleManager.instance.endBattle();
            }
        } else {
            player.sendSystemMessage(Component.literal("Oh no! The Pokémon broke free!"));
        }
    }

    private boolean isBackstrike(Player player, PokemonEntity pokemon) {
        double angle = Math.toDegrees(Math.atan2(pokemon.getZ() - player.getZ(), pokemon.getX() - player.getX())) - player.getYRot();
        angle = (angle + 360) % 360;
        return angle > 135 && angle < 225;
    }

    private ListTag serializeMoveSlots(List<PokemonMove> moveSlots) {
        ListTag moveList = new ListTag();
        for (PokemonMove move : moveSlots) {
            CompoundTag moveTag = new CompoundTag();
            moveTag.putString("MoveName", move.getName());
            moveTag.putInt("CurrentPP", move.getPP());
            moveTag.putInt("MaxPP", move.getMaxPP());
            moveList.add(moveTag);
        }
        return moveList;
    }

    //Merged the old helper method of setting capture data back into the spawning ball to prevent crashes
    private void spawnCapturedPokeBall(PokemonEntity pokemon, Player player) {
        PokeDataCapability data = pokemon.getData(AttachmentInit.POKE_DATA);
        ItemStack pokeBall = new ItemStack(ItemInit.POKE_BALL.get());
        CompoundTag capturedData = new CompoundTag();
        capturedData.putString("Name", pokemon.getName().getString());
        capturedData.putString("Gender", getGenderString(data.GetPokemonGender()));
        capturedData.putInt("Level", data.GetPokemonLevel());
        capturedData.putInt("HP", pokemon.getPokeHP());
        capturedData.putInt("Max-HP", pokemon.getPokeMaxHP());
        capturedData.putInt("ATK", pokemon.getPokeAttack());
        capturedData.putInt("DEF", pokemon.getPokeDefense());
        capturedData.putString("RegistryName", BuiltInRegistries.ENTITY_TYPE.getKey(pokemon.getType()).toString());
        capturedData.put("MoveSlots", serializeMoveSlots(pokemon.getMoveSlots()));
        int ballID = new Random().nextInt(1000000);
        capturedData.putInt("BallID", ballID);
        capturedData.putBoolean("Tamed", pokemon.isTame());
        capturedData.putBoolean("PersistenceRequired", pokemon.isPersistenceRequired());
        if (pokemon.getOwnerUUID() != null) {
            capturedData.putString("OwnerUUID", pokemon.getOwnerUUID().toString());
        }
        pokemon.setBallID(ballID);
        pokeBall.getOrCreateTag().put("CapturedPokemon", capturedData);
        pokeBall.getOrCreateTag().putInt("Level", data.GetPokemonLevel());
        pokeBall.getOrCreateTag().putInt("Gender", data.GetPokemonGender());
        pokeBall.getOrCreateTag().putBoolean("Captured", true);
        pokeBall.getOrCreateTag().putBoolean("IsOutOfBall", false);
        player.addItem(pokeBall);
        player.sendSystemMessage(Component.literal("Gotcha! The " + pokemon.getName().getString() + " was caught!"));
        if(!player.level().isClientSide()) {
            syncStatsToTrackingPlayers(pokemon);
        }
        pokemon.discard();
    }

    private String getGenderString(int gender) {
        switch (gender) {
            case 0:
                return "♂";
            case 1:
                return "♀";
            case 2:
                return "?";
            default:
                return "?";
        }
    }

    private void handleBlockHit(BlockHitResult result) {
        BlockPos pos = result.getBlockPos();
        BlockState blockState = level().getBlockState(pos);
        ItemStack itemStack = this.getItem();

        if (blockState.isSolid() && level().isEmptyBlock(pos.above())) {
            CompoundTag itemTag = itemStack.getOrCreateTag();
            if (itemTag.contains("CapturedPokemon")) {
                CompoundTag capturedData = itemTag.getCompound("CapturedPokemon");
                String registryName = capturedData.getString("RegistryName");
                int ballID = capturedData.getInt("BallID");
                boolean isOutOfBall = itemTag.getBoolean("IsOutOfBall");
                EntityType<?> entityType = BuiltInRegistries.ENTITY_TYPE.get(new ResourceLocation(registryName));

                if (entityType != null && entityType.create(level()) instanceof PokemonEntity) {
                    PokemonEntity capturedPokemon = (PokemonEntity) entityType.create(level());
                    if (isOutOfBall)
                    {
                        return;
                    }
                    else if (capturedPokemon != null) {

                        capturedPokemon.deserializeNBT(capturedData);
                        capturedPokemon.setBallID(ballID);
                        if (capturedData.contains("OwnerUUID")) {
                            String uuidString = capturedData.getString("OwnerUUID");
                            if (!uuidString.isEmpty()) {
                                capturedPokemon.setOwnerUUID(UUID.fromString(uuidString));
                            }
                        }
                        capturedPokemon.moveTo(pos.getX(), pos.getY() + 1, pos.getZ(), this.getYRot(), this.getXRot());
                        capturedPokemon.getNavigation().stop();
                        capturedPokemon.setTarget(null);
                        capturedPokemon.setOrderedToSit(true);
                        capturedPokemon.setJumping(false);
                        level().addFreshEntity(capturedPokemon);
                        //Set data from itemstack tags and sync AFTER entity spawns
                        PokeDataCapability data = capturedPokemon.getData(AttachmentInit.POKE_DATA);
                        data.SetPokemonLevel(itemTag.getInt("Level"));
                        data.SetPokemonGender(itemTag.getInt("Gender"));
                        syncStatsToTrackingPlayers(capturedPokemon);
                        capturedPokemon.setData(AttachmentInit.POKE_DATA, data);
                        itemStack.getTag().putBoolean("IsOutOfBall", true);
                        System.out.println("User Pokémon Moves:");
                        capturedPokemon.getMoveSlots().forEach(move -> System.out.println(move.getName()));
                    }
                }
            }
        }
    }

    private void handlePlayerHit(Player player) {
        ItemStack itemStack = this.getItem();
        CompoundTag itemTag = itemStack.getOrCreateTag();
        if (itemTag.contains("CapturedPokemon")) {
            CompoundTag capturedData = itemTag.getCompound("CapturedPokemon");
            int ballID = capturedData.getInt("BallID");
            if (player.getInventory().contains(new ItemStack(ItemInit.POKE_BALL.get(), 1))) {
                for (ItemStack stack : player.getInventory().items) {
                    if (stack.hasTag() && stack.getTag().contains("CapturedPokemon")) {
                        CompoundTag data = stack.getTag().getCompound("CapturedPokemon");
                        int id = data.getInt("BallID");
                        if (id == ballID) {
                            player.sendSystemMessage(Component.literal("This isn't this Pokémon's ball!"));
                            return;
                        }
                    }
                }
            }
        }
    }
}