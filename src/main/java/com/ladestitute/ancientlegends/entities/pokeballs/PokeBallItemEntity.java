package com.ladestitute.ancientlegends.entities.pokeballs;

import com.ladestitute.ancientlegends.registry.EntityTypeInit;
import com.ladestitute.ancientlegends.registry.ItemInit;
import net.minecraft.tags.DamageTypeTags;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.damagesource.DamageType;
import net.minecraft.world.damagesource.DamageTypes;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.monster.Zombie;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Explosion;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.gameevent.GameEvent;
import net.minecraft.world.level.levelgen.Heightmap;

public class PokeBallItemEntity extends ItemEntity {

    public PokeBallItemEntity(Level world) {
        super(EntityTypeInit.POKE_BALL_ITEM.get(), world);
    }

    public PokeBallItemEntity(Level level, double x, double y, double z, ItemStack copy) {
        super(level, x, y, z, copy);
    }

    @Override
    public boolean fireImmune() {
        ItemStack stack = this.getItem();
        return stack.hasTag() && stack.getTag().contains("CapturedPokemon");
    }

    @Override
    public boolean ignoreExplosion(Explosion pExplosion) {
        ItemStack stack = this.getItem();
        return stack.hasTag() && stack.getTag().contains("CapturedPokemon");
    }

    @Override
    public boolean hurt(DamageSource pSource, float pAmount) {
        if (!this.getItem().isEmpty() && this.getItem().is(ItemInit.POKE_BALL) && pSource.is(DamageTypes.FALLING_ANVIL)) {
            return true;
        } else if (!this.getItem().isEmpty() && this.getItem().is(ItemInit.POKE_BALL) && pSource.is(DamageTypes.CACTUS)) {
            return false;
        }
        else return true;
    }

    @Override
    public void tick() {
        // Prevent the item from despawning
        this.setNoDespawn();
        // Call the superclass method to ensure normal ticking behavior
        super.tick();

        // Prevent void damage
        if (this.getY() < -64.0D) {
            this.setDeltaMovement(0, 0, 0);
            this.teleportTo(this.getX(), this.level().getHeight(Heightmap.Types.MOTION_BLOCKING, (int) this.getX(), (int) this.getZ()), this.getZ());
        }

        if (!this.hasPickUpDelay() && this.isNearZombie()) {
            this.setPickUpDelay(20); // 20 ticks = 1 second delay
        }
    }

    private boolean isNearZombie() {
        // Check if there's a zombie within a certain radius
        return this.level().getEntitiesOfClass(Zombie.class, this.getBoundingBox().inflate(1)).size() > 0;
    }

    @Override
    public boolean isInvulnerableTo(DamageSource source) {
        if (source == this.level().damageSources().fellOutOfWorld()) {
            // Prevent void damage by making it immune to the OUT_OF_WORLD damage source
            return true;
        }
        return super.isInvulnerableTo(source);
    }

    private void setNoDespawn() {
        // Method to ensure the item never despawns
        this.lifespan = Integer.MAX_VALUE;
        this.setUnlimitedLifetime();
    }
}
