package com.ladestitute.ancientlegends.entities.mobs.pokemon;

import com.ladestitute.ancientlegends.battle.constructors.PokemonMove;
import com.ladestitute.ancientlegends.battle.constructors.PokemonMoveLevel;
import com.ladestitute.ancientlegends.battle.registry.PokemonLevelLearnListInit;
import com.ladestitute.ancientlegends.battle.registry.PokemonMoveInit;
import com.ladestitute.ancientlegends.capability.pokedata.PokeDataCapability;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.passive.BidoofEntity;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.skittish.StarlyEntity;
import com.ladestitute.ancientlegends.registry.AttachmentInit;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.AgeableMob;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.TamableAnimal;
import net.minecraft.world.entity.ai.goal.*;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import org.jetbrains.annotations.Nullable;

import java.util.*;

@SuppressWarnings("EntityConstructor")
public class PokemonEntity extends TamableAnimal {
    private static final EntityDataAccessor<String> OWNER_UUID = SynchedEntityData.defineId(PokemonEntity.class, EntityDataSerializers.STRING);
    private static final EntityDataAccessor<Integer> LEVEL =
            SynchedEntityData.defineId(PokemonEntity.class, EntityDataSerializers.INT);
    private static final EntityDataAccessor<Integer> CATCHRATE =
            SynchedEntityData.defineId(PokemonEntity.class, EntityDataSerializers.INT);
    private static final EntityDataAccessor<Integer> TYPE_1 =
            SynchedEntityData.defineId(PokemonEntity.class, EntityDataSerializers.INT);
    private static final EntityDataAccessor<Integer> TYPE_2 =
            SynchedEntityData.defineId(PokemonEntity.class, EntityDataSerializers.INT);
    private static final EntityDataAccessor<Integer> BALL_ID = SynchedEntityData.defineId(PokemonEntity.class, EntityDataSerializers.INT);
    private static final EntityDataAccessor<Integer> HP =
            SynchedEntityData.defineId(PokemonEntity.class, EntityDataSerializers.INT);
    private static final EntityDataAccessor<Integer> MAX_HP =
            SynchedEntityData.defineId(PokemonEntity.class, EntityDataSerializers.INT);
    private static final EntityDataAccessor<Integer> ATTACK =
            SynchedEntityData.defineId(PokemonEntity.class, EntityDataSerializers.INT);
    private static final EntityDataAccessor<Integer> DEFENSE =
            SynchedEntityData.defineId(PokemonEntity.class, EntityDataSerializers.INT);
    private static final EntityDataAccessor<Integer> SPECIAL_ATTACK =
            SynchedEntityData.defineId(PokemonEntity.class, EntityDataSerializers.INT);
    private static final EntityDataAccessor<Integer> SPECIAL_DEFENSE =
            SynchedEntityData.defineId(PokemonEntity.class, EntityDataSerializers.INT);
    private static final EntityDataAccessor<Integer> SPEED =
            SynchedEntityData.defineId(PokemonEntity.class, EntityDataSerializers.INT);
    private static final EntityDataAccessor<Integer> ACCURACY =
            SynchedEntityData.defineId(PokemonEntity.class, EntityDataSerializers.INT);
    private static final EntityDataAccessor<Integer> EVASION =
            SynchedEntityData.defineId(PokemonEntity.class, EntityDataSerializers.INT);
    private static final EntityDataAccessor<Integer> ACTION_SPEED =
            SynchedEntityData.defineId(PokemonEntity.class, EntityDataSerializers.INT);
    private static final EntityDataAccessor<Boolean> DRAW_BATTLE_GUI =
            SynchedEntityData.defineId(PokemonEntity.class, EntityDataSerializers.BOOLEAN);
    private static final EntityDataAccessor<Integer> ACTION_SPEED_TIMER =
            SynchedEntityData.defineId(PokemonEntity.class, EntityDataSerializers.INT);

    // Move Slots
    private List<PokemonMove> moveSlots = new ArrayList<>();
    private Map<PokemonMove, Integer> movePP = new HashMap<>();
    private static final int MAX_MOVES = 4;

    public PokemonEntity(EntityType<? extends TamableAnimal> p_21803_, Level p_21804_) {
        super(p_21803_, p_21804_);
        this.setTame(false);
    }

    public void learnMovesBasedOnLevel() {
        Map<Integer, PokemonMoveLevel> learnList = PokemonLevelLearnListInit.getLearnList(getSpeciesName());
        PokeDataCapability data = this.getData(AttachmentInit.POKE_DATA);

        if (learnList == null || learnList.isEmpty()) {
            return;
        }

        learnList.entrySet().stream()
                .filter(entry -> data.GetPokemonLevel() >= entry.getKey())
                .forEach(entry -> {
                    PokemonMove move = entry.getValue().getMove();
                    if (move != null) {
                        addMoveToSlot(move);
                    }
                });

        // Fill remaining slots with "Empty" moves if less than 4 moves are learned
        while (moveSlots.size() < MAX_MOVES) {
            moveSlots.add(PokemonMoveInit.getMove("empty"));
            movePP.put(PokemonMoveInit.getMove("empty"), 0);
        }
    }

    private void addMoveToSlot(PokemonMove move) {
        if (moveSlots.size() < MAX_MOVES) {
            moveSlots.add(move);
            movePP.put(move, move.getMaxPP());
        } else {
            if (shouldRemoveFirstMove()) {
                movePP.remove(moveSlots.remove(0));
                for (int i = 0; i < moveSlots.size() - 1; i++) {
                    moveSlots.set(i, moveSlots.get(i + 1));
                }
                moveSlots.set(moveSlots.size() - 1, move);
                movePP.put(move, move.getMaxPP());
            } else {
                movePP.remove(moveSlots.remove(moveSlots.size() - 1));
                for (int i = moveSlots.size() - 1; i > 0; i--) {
                    moveSlots.set(i, moveSlots.get(i - 1));
                }
                moveSlots.set(0, move);
                movePP.put(move, move.getMaxPP());
            }
        }
    }

    private boolean shouldRemoveFirstMove() {
        return true;
    }

    public String getSpeciesName() {
        if (this instanceof BidoofEntity) {
            return "bidoof";
        } else if (this instanceof StarlyEntity) {
            return "starly";
        } else return "";
    }

    public List<PokemonMove> getMoveSlots() {
        return moveSlots;
    }

    public void setMoveSlots(List<PokemonMove> moveSlots) {
        this.moveSlots = moveSlots;
        this.movePP.clear();
        for (PokemonMove move : moveSlots) {
            this.movePP.put(move, move.getMaxPP());
        }
    }

    public int getPP(PokemonMove move) {
        return movePP.getOrDefault(move, 0);
    }

    public void setPP(PokemonMove move, int pp) {
        if (movePP.containsKey(move)) {
            movePP.put(move, pp);
        }
    }

    @Override
    protected void registerGoals() {
        this.goalSelector.addGoal(1, new FloatGoal(this));
        this.goalSelector.addGoal(2, new SitWhenOrderedToGoal(this));
        this.goalSelector.addGoal(6, new FollowOwnerGoal(this, 1.0, 10.0F, 2.0F, false));
        this.goalSelector.addGoal(8, new RandomStrollGoal(this, 1.0));
        this.goalSelector.addGoal(10, new LookAtPlayerGoal(this, Player.class, 8.0F));
        this.goalSelector.addGoal(10, new RandomLookAroundGoal(this));
    }

    @Nullable
    @Override
    public AgeableMob getBreedOffspring(ServerLevel p_146743_, AgeableMob p_146744_) {
        return null;
    }

    @Override
    protected void defineSynchedData() {
        super.defineSynchedData();
        this.entityData.define(OWNER_UUID, "");
        this.entityData.define(LEVEL, 0);
        this.entityData.define(CATCHRATE, 0);
        this.entityData.define(TYPE_1, 0);
        this.entityData.define(TYPE_2, 0);
        this.entityData.define(BALL_ID, 0);
        this.entityData.define(HP, 1);
        this.entityData.define(MAX_HP, 1);
        this.entityData.define(ATTACK, 1);
        this.entityData.define(DEFENSE, 1);
        this.entityData.define(SPECIAL_ATTACK, 1);
        this.entityData.define(SPECIAL_DEFENSE, 1);
        this.entityData.define(SPEED, 1);
        this.entityData.define(ACCURACY, 0);
        this.entityData.define(EVASION, 0);
        this.entityData.define(ACTION_SPEED, 14);
        this.entityData.define(DRAW_BATTLE_GUI, false);
        this.entityData.define(ACTION_SPEED_TIMER, 14);
    }

    public boolean getDrawBattleGui() {
        return this.entityData.get(DRAW_BATTLE_GUI);
    }

    public void setDrawBattleGui(boolean amount) {
        this.entityData.set(DRAW_BATTLE_GUI, amount);
    }

    public int getPokeHP() {
        return this.entityData.get(HP);
    }

    public void setPokeHP(int amount) {
        this.entityData.set(HP, amount);
    }

    public int getPokeMaxHP() {
        return this.entityData.get(MAX_HP);
    }

    public void setPokeMaxHP(int amount) {
        this.entityData.set(MAX_HP, amount);
    }

    public int getPokeAttack() {
        return this.entityData.get(ATTACK);
    }

    public void setPokeAttack(int amount) {
        this.entityData.set(ATTACK, amount);
    }

    public int getPokeDefense() {
        return this.entityData.get(DEFENSE);
    }

    public void setPokeDefense(int amount) {
        this.entityData.set(DEFENSE, amount);
    }

    public int getPokeSpecialAttack() {
        return this.entityData.get(SPECIAL_ATTACK);
    }

    public void setPokeSpecialAttack(int amount) {
        this.entityData.set(SPECIAL_ATTACK, amount);
    }

    public int getPokeSpecialDefense() {
        return this.entityData.get(SPECIAL_DEFENSE);
    }

    public void setPokeSpecialDefense(int amount) {
        this.entityData.set(SPECIAL_DEFENSE, amount);
    }

    public int getPokeSpeed() {
        return this.entityData.get(SPEED);
    }

    public void setPokeSpeed(int amount) {
        this.entityData.set(SPEED, amount);
    }

    public int getPokeAccuracy() {
        return this.entityData.get(ACCURACY);
    }

    public void setPokeAccuracy(int amount) {
        this.entityData.set(ACCURACY, amount);
    }

    public int getPokeEvasion() {
        return this.entityData.get(EVASION);
    }

    public void setPokeEvasion(int amount) {
        this.entityData.set(EVASION, amount);
    }

    public int getPokeActionSpeed() {
        return this.entityData.get(ACTION_SPEED);
    }

    public void setPokeActionSpeed(int amount) {
        this.entityData.set(ACTION_SPEED, amount);
    }

    public int getPokeActionSpeedTimer() {
        return this.entityData.get(ACTION_SPEED_TIMER);
    }

    public void setPokeActionSpeedTimer(int amount) {
        this.entityData.set(ACTION_SPEED_TIMER, amount);
    }

    @Override
    public boolean causeFallDamage(float pFallDistance, float pMultiplier, DamageSource pSource) {
        return super.causeFallDamage(pFallDistance, pMultiplier, pSource);
    }

    public UUID getOwnerUUID() {
        String uuidString = this.entityData.get(OWNER_UUID);
        return uuidString.isEmpty() ? null : UUID.fromString(uuidString);
    }

    public void setOwnerUUID(UUID ownerUUID) {
        this.entityData.set(OWNER_UUID, ownerUUID == null ? "" : ownerUUID.toString());
    }

    public int getBallID() {
        return this.entityData.get(BALL_ID);
    }

    public void setBallID(int ballID) {
        this.entityData.set(BALL_ID, ballID);
    }

    @Override
    public InteractionResult mobInteract(Player pPlayer, InteractionHand pHand) {
        return super.mobInteract(pPlayer, pHand);
    }

    @Override
    public void addAdditionalSaveData(CompoundTag compound) {
        super.addAdditionalSaveData(compound);
        compound.putInt("BallID", this.getBallID());
        compound.putBoolean("PersistenceRequired", this.isPersistenceRequired());
        compound.putBoolean("Tamed", this.isTame());
        compound.putInt("PokemonLevel", this.getPokemonLevel());
        compound.putInt("Catchrate", this.getCatchrate());
        compound.putInt("Type1", this.getType1());
        compound.putInt("Type2", this.getType2());
        String ownerUUID = this.entityData.get(OWNER_UUID);
        if (!ownerUUID.isEmpty()) {
            compound.putString("OwnerUUID", ownerUUID);
        }
        compound.putInt("PokeHP", this.getPokeHP());
        compound.putInt("PokeMaxHP", this.getPokeMaxHP());
        compound.putInt("PokeAttack", this.getPokeAttack());
        compound.putInt("PokeDefense", this.getPokeDefense());
        compound.putInt("PokeSpecialAttack", this.getPokeSpecialAttack());
        compound.putInt("PokeSpecialDefense", this.getPokeSpecialDefense());
        compound.putInt("PokeSpeed", this.getPokeSpeed());
        compound.putInt("PokeAccuracy", this.getPokeAccuracy());
        compound.putInt("PokeEvasion", this.getPokeEvasion());
        compound.putInt("PokeActionSpeed", this.getPokeActionSpeed());
        compound.putInt("PokeActionSpeedTimer", this.getPokeActionSpeedTimer());
        compound.putBoolean("DrawBattleGui", this.getDrawBattleGui());
        ListTag moveList = new ListTag();
        for (PokemonMove move : this.moveSlots) {
            CompoundTag moveTag = new CompoundTag();
            moveTag.putString("Move", move.getName());
            moveTag.putInt("PP", getPP(move));
            moveList.add(moveTag);
        }
        compound.put("Moves", moveList);
    }

    @Override
    public void readAdditionalSaveData(CompoundTag compound) {
        super.readAdditionalSaveData(compound);
        this.setBallID(compound.getInt("BallID"));
        if (compound.getBoolean("PersistenceRequired")) {
            this.setPersistenceRequired();
        }
        this.setTame(compound.getBoolean("Tamed"));
        this.setPokemonLevel(compound.getInt("PokemonLevel"));
        this.setCatchrate(compound.getInt("Catchrate"));
        this.setType1(compound.getInt("Type1"));
        this.setType2(compound.getInt("Type2"));
        this.setPokeHP(compound.getInt("PokeHP"));
        this.setPokeMaxHP(compound.getInt("PokeMaxHP"));
        this.setPokeAttack(compound.getInt("PokeAttack"));
        this.setPokeDefense(compound.getInt("PokeDefense"));
        this.setPokeSpecialAttack(compound.getInt("PokeSpecialAttack"));
        this.setPokeSpecialDefense(compound.getInt("PokeSpecialDefense"));
        this.setPokeSpeed(compound.getInt("PokeSpeed"));
        this.setPokeAccuracy(compound.getInt("PokeAccuracy"));
        this.setPokeEvasion(compound.getInt("PokeEvasion"));
        this.setPokeActionSpeed(compound.getInt("PokeActionSpeed"));
        this.setPokeActionSpeedTimer(compound.getInt("PokeActionSpeedTimer"));
        this.setDrawBattleGui(compound.getBoolean("DrawBattleGui"));
        if (compound.contains("OwnerUUID")) {
            String uuidString = compound.getString("OwnerUUID");
            if (!uuidString.isEmpty()) {
                this.setOwnerUUID(UUID.fromString(uuidString));
            }
        }
        ListTag moveList = compound.getList("Moves", 10);
        this.moveSlots.clear();
        this.movePP.clear();
        for (int i = 0; i < moveList.size(); i++) {
            CompoundTag moveTag = moveList.getCompound(i);
            String moveName = moveTag.getString("Move");
            int pp = moveTag.getInt("PP");
            PokemonMove move = PokemonMoveInit.getMove(moveName);
            if (move != null) {
                this.moveSlots.add(move);
                this.movePP.put(move, pp);
            } else {
                this.moveSlots.add(PokemonMoveInit.EMPTY.get());
                this.movePP.put(PokemonMoveInit.EMPTY.get(), 0);
            }
        }
    }

    public void tame(Player player) {
        super.tame(player);
        this.setPersistenceRequired();
    }

    @Override
    public void setPersistenceRequired() {
        super.setPersistenceRequired();
    }

    public int getPokemonLevel() {
        return getEntityData().get(LEVEL);
    }

    public void setPokemonLevel(int level) {
        getEntityData().set(LEVEL, level);
    }

    public int getCatchrate() {
        return getEntityData().get(CATCHRATE);
    }

    public void setCatchrate(int catchrate) {
        getEntityData().set(CATCHRATE, catchrate);
    }

    public int getType1() {
        return getEntityData().get(TYPE_1);
    }

    public void setType1(int type1) {
        getEntityData().set(TYPE_1, type1);
    }

    public int getType2() {
        return getEntityData().get(TYPE_2);
    }

    public void setType2(int type2) {
        getEntityData().set(TYPE_2, type2);
    }

    public int getActionSpeed(int entityAttributeValue) {
        if (getPokeSpeed() <= 15) {
            return 14;
        } else if (getPokeSpeed() <= 31) {
            return 13;
        } else if (getPokeSpeed() <= 55) {
            return 12;
        } else if (getPokeSpeed() <= 88) {
            return 11;
        } else if (getPokeSpeed() <= 129) {
            return 10;
        } else if (getPokeSpeed() <= 181) {
            return 9;
        } else if (getPokeSpeed() <= 242) {
            return 8;
        } else if (getPokeSpeed() <= 316) {
            return 7;
        } else if (getPokeSpeed() <= 401) {
            return 6;
        } else {
            return 5;
        }
    }
}
