package com.ladestitute.ancientlegends.entities.mobs.pokemon.base;

import com.ladestitute.ancientlegends.entities.mobs.pokemon.PokemonEntity;
import net.minecraft.world.entity.EntitySelector;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.goal.AvoidEntityGoal;
import net.minecraft.world.entity.ai.goal.FollowOwnerGoal;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;

@SuppressWarnings("EntityConstructor")
public class SkittishPokemonEntity extends PokemonEntity {
    private SkittishPokemonEntity.AvoidPlayerGoal<Player> avoidPlayersGoal;

    public SkittishPokemonEntity(EntityType<? extends PokemonEntity> p_28114_, Level p_28115_) {
        super(p_28114_, p_28115_);
    }

    @Override
    public String getSpeciesName() {
        return null;
    }

    protected void registerGoals() {
        this.goalSelector.addGoal(6, new FollowOwnerGoal(this, 1.0D, 10.0F, 5.0F, false));
        this.goalSelector.addGoal(12, new LookAtPlayerGoal(this, Player.class, 10.0F));
    }

    protected void reassessTameGoals() {
        if (this.avoidPlayersGoal == null) {
            this.avoidPlayersGoal = new SkittishPokemonEntity.AvoidPlayerGoal<>(this, Player.class, 16.0F, 0.8D, 1.33D);
        }

        this.goalSelector.removeGoal(this.avoidPlayersGoal);
        if (!this.isTame()) {
            this.goalSelector.addGoal(4, this.avoidPlayersGoal);
        }

    }

    static class AvoidPlayerGoal<T extends LivingEntity> extends AvoidEntityGoal<T> {
        private final SkittishPokemonEntity pokemon;

        public AvoidPlayerGoal(SkittishPokemonEntity p_i50440_1_, Class<T> p_i50440_2_, float p_i50440_3_, double p_i50440_4_, double p_i50440_6_) {
            super(p_i50440_1_, p_i50440_2_, p_i50440_3_, p_i50440_4_, p_i50440_6_, EntitySelector.NO_CREATIVE_OR_SPECTATOR::test);
            this.pokemon = p_i50440_1_;
        }

        public boolean canUse() {
            return !this.pokemon.isTame() && super.canUse();
        }

        public boolean canContinueToUse() {
            return !this.pokemon.isTame() && super.canContinueToUse();
        }
    }


}
