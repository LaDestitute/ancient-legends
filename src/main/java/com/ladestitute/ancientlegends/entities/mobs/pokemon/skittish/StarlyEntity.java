package com.ladestitute.ancientlegends.entities.mobs.pokemon.skittish;

import com.ladestitute.ancientlegends.entities.mobs.helper.FlyingTypeMovementController;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.base.SkittishPokemonEntity;
import net.minecraft.core.BlockPos;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.control.JumpControl;
import net.minecraft.world.entity.ai.goal.*;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;

public class StarlyEntity extends SkittishPokemonEntity {
    public StarlyEntity(EntityType<? extends SkittishPokemonEntity> type, Level worldIn) {
        super(type, worldIn);
        this.jumpControl = new JumpControl(this);
        this.moveControl = new FlyingTypeMovementController(this, 10, false);
        this.setTame(false);
    }

    protected void registerGoals() {
        //Be careful with modifying the numbers in the goals
        //You may cause unintended erratic mob behavior if you don't know what you're doing
        this.goalSelector.addGoal(0, new FloatGoal(this));
        this.goalSelector.addGoal(2, new FollowOwnerGoal(this, 1.0D, 5.0F, 1.0F, true));
        this.goalSelector.addGoal(5, new RandomStrollGoal(this, 1.0D));
        this.goalSelector.addGoal(6, new LookAtPlayerGoal(this, Player.class, 6.0F));
        this.goalSelector.addGoal(7, new RandomLookAroundGoal(this));
    }

    public static AttributeSupplier.Builder customAttributes() {
        return Mob.createMobAttributes().add(Attributes.MAX_HEALTH, 17.0D)
                .add(Attributes.FLYING_SPEED, 0.65D)
                .add(Attributes.MOVEMENT_SPEED, 1.6D);
    }

    @Override
    public String getSpeciesName() {
        return "starly";
    }

    @Override
    public void tick() {
        if(!this.onGround())
        {
            this.fallDistance=0F;
        }
        super.tick();
    }

    public void setTame(boolean p_30443_) {
        super.setTame(p_30443_);
        if (p_30443_) {
            this.getAttribute(Attributes.MAX_HEALTH).setBaseValue(17.0D);
            this.setHealth(17.0F);
        }
    }

    protected SoundEvent getHurtSound(DamageSource p_29437_) {
        return SoundEvents.PARROT_HURT;
    }

    protected SoundEvent getDeathSound() {
        return SoundEvents.PARROT_DEATH;
    }

    protected void playStepSound(BlockPos p_29419_, BlockState p_29420_) {
        this.playSound(SoundEvents.PARROT_STEP, 0.15F, 1.0F);
    }

}
