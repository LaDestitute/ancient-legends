package com.ladestitute.ancientlegends.keybinds;

import net.minecraft.client.KeyMapping;
import net.minecraft.client.Minecraft;
import com.mojang.blaze3d.platform.InputConstants;
import org.apache.commons.lang3.ArrayUtils;
import org.lwjgl.glfw.GLFW;

public class KeyBindings {
    public static final String CATEGORY = "key.categories.battle";
    public static final KeyMapping MOVE_1 = new KeyMapping("key.battle.move_1", InputConstants.Type.KEYSYM, GLFW.GLFW_KEY_1, CATEGORY);
    public static final KeyMapping MOVE_2 = new KeyMapping("key.battle.move_2", InputConstants.Type.KEYSYM, GLFW.GLFW_KEY_2, CATEGORY);
    public static final KeyMapping MOVE_3 = new KeyMapping("key.battle.move_3", InputConstants.Type.KEYSYM, GLFW.GLFW_KEY_3, CATEGORY);
    public static final KeyMapping MOVE_4 = new KeyMapping("key.battle.move_4", InputConstants.Type.KEYSYM, GLFW.GLFW_KEY_4, CATEGORY);

    public static void register() {
        net.minecraft.client.Minecraft.getInstance().options.keyMappings = ArrayUtils.addAll(
                Minecraft.getInstance().options.keyMappings,
                MOVE_1, MOVE_2, MOVE_3, MOVE_4
        );
    }
}
