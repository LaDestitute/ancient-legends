package com.ladestitute.ancientlegends.blocks;

import com.ladestitute.ancientlegends.AncientLegendsMain;
import com.ladestitute.ancientlegends.registry.BlockInit;
import com.mojang.serialization.MapCodec;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.HorizontalDirectionalBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.phys.shapes.BooleanOp;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

import javax.annotation.Nullable;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

public class EmptyApricornTreeBlock extends HorizontalDirectionalBlock {

    public static final MapCodec<ApricornTreeBlock> CODEC = simpleCodec(ApricornTreeBlock::new);

    @Override
    protected MapCodec<? extends HorizontalDirectionalBlock> codec() {
        return CODEC;
    }

    public EmptyApricornTreeBlock(Properties properties) {
        super(properties.randomTicks());
        runCalculation(SHAPE.orElse(Shapes.block()));
    }

    protected void runCalculation(VoxelShape shape) {
        for (Direction direction : Direction.values())
            SHAPES.put(direction, AncientLegendsMain.calculateShapes(direction, shape));
    }

    private static final Map<Direction, VoxelShape> SHAPES = new EnumMap<>(Direction.class);

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
        super.createBlockStateDefinition(builder);
        builder.add(FACING);
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter level, BlockPos pos, CollisionContext context) {
        return SHAPES.get(state.getValue(FACING));
    }

    @Override
    public BlockState getStateForPlacement(BlockPlaceContext context) {
        return defaultBlockState().setValue(FACING, context.getHorizontalDirection().getOpposite());
    }

    private static final Optional<VoxelShape> SHAPE = Stream
            .of(Block.box(7, 0, 7, 10, 8, 10),
                    Block.box(5, 8, 5, 12, 14, 12),
                    Block.box(6, 14, 6, 11, 15, 11
                    ))
            .reduce((v1, v2) -> Shapes.join(v1, v2, BooleanOp.OR));

    @Override
    public void appendHoverText(ItemStack p_49816_, @Nullable BlockGetter p_49817_, List<Component> p_49818_, TooltipFlag p_49819_) {
        p_49818_.add(Component.literal("An empty apricorn tree, you may have to wait to get more"));
        super.appendHoverText(p_49816_, p_49817_, p_49818_, p_49819_);
    }

    @Override
    public void randomTick(BlockState state, ServerLevel world, BlockPos pos, RandomSource p_60554_) {
        world.setBlock(pos, BlockInit.APRICORN_TREE.get().defaultBlockState().setValue(FACING, state.getValue(FACING)), 2);
        super.randomTick(state, world, pos, p_60554_);
    }
}
