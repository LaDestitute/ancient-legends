package com.ladestitute.ancientlegends.blocks;

import com.ladestitute.ancientlegends.AncientLegendsMain;
import com.ladestitute.ancientlegends.registry.BlockInit;
import com.mojang.serialization.MapCodec;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.HorizontalDirectionalBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.phys.shapes.BooleanOp;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

import javax.annotation.Nullable;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

public class DepletedTumblestoneOreBlock extends HorizontalDirectionalBlock {

    public static final MapCodec<ApricornTreeBlock> CODEC = simpleCodec(ApricornTreeBlock::new);

    @Override
    protected MapCodec<? extends HorizontalDirectionalBlock> codec() {
        return CODEC;
    }

    public DepletedTumblestoneOreBlock(Properties properties) {
        super(properties.randomTicks());
        runCalculation(SHAPE.orElse(Shapes.block()));
    }

    protected void runCalculation(VoxelShape shape) {
        for (Direction direction : Direction.values())
            SHAPES.put(direction, AncientLegendsMain.calculateShapes(direction, shape));
    }

    private static final Map<Direction, VoxelShape> SHAPES = new EnumMap<>(Direction.class);

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
        super.createBlockStateDefinition(builder);
        builder.add(FACING);
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter level, BlockPos pos, CollisionContext context) {
        return SHAPES.get(state.getValue(FACING));
    }

    @Override
    public BlockState getStateForPlacement(BlockPlaceContext context) {
        return defaultBlockState().setValue(FACING, context.getHorizontalDirection().getOpposite());
    }

    private static final Optional<VoxelShape> SHAPE = Stream
            .of(Block.box(2, 0, 8, 3, 1, 9),
                    Block.box(2, 0, 9, 3, 1, 10),
                    Block.box(2, 1, 9, 3, 2, 10),
                    Block.box(5, 0, 12, 8, 2, 13),
                    Block.box(8, 0, 12, 11, 2, 13),
                    Block.box(3, 0, 10, 4, 2, 12),
                    Block.box(2, 1, 8, 3, 2, 9),
                    Block.box(4, 0, 13, 8, 1, 14),
                    Block.box(8, 0, 13, 12, 1, 14),
                    Block.box(3, 0, 12, 5, 1, 13),
                    Block.box(4, 0, 10, 8, 2, 12),
                    Block.box(5, 3, 5, 6, 4, 6),
                    Block.box(10, 3, 10, 11, 4, 11),
                    Block.box(5, 3, 10, 6, 4, 11),
                    Block.box(10, 3, 5, 11, 4, 6),
                    Block.box(9, 2, 9, 10, 3, 10),
                    Block.box(6, 2, 5, 10, 4, 6),
                    Block.box(4, 0, 2, 8, 1, 3),
                    Block.box(3, 0, 4, 5, 2, 6),
                    Block.box(8, 0, 12, 12, 1, 13),
                    Block.box(8, 0, 3, 13, 1, 4),
                    Block.box(8, 0, 3, 11, 2, 4),
                    Block.box(8, 0, 10, 13, 2, 12),
                    Block.box(5, 0, 3, 8, 2, 4),
                    Block.box(4, 0, 4, 11, 2, 10),
                    Block.box(5, 2, 6, 6, 4, 10),
                    Block.box(10, 2, 6, 11, 4, 10),
                    Block.box(8, 0, 2, 12, 1, 3),
                    Block.box(10, 2, 5, 11, 3, 6),
                    Block.box(6, 2, 10, 10, 4, 11),
                    Block.box(6, 2, 7, 10, 4, 10),
                    Block.box(3, 0, 3, 5, 1, 4),
                    Block.box(8, 0, 4, 13, 2, 6),
                    Block.box(9, 2, 5, 10, 3, 6),
                    Block.box(5, 2, 10, 6, 3, 11),
                    Block.box(12, 0, 12, 13, 1, 13),
                    Block.box(7, 0, 3, 12, 1, 4),
                    Block.box(10, 0, 6, 14, 2, 10),
                    Block.box(2, 0, 6, 6, 2, 10),
                    Block.box(9, 3, 6, 10, 4, 7),
                    Block.box(6, 2, 4, 10, 3, 5),
                    Block.box(10, 2, 10, 11, 3, 11),
                    Block.box(4, 0, 6, 5, 3, 10),
                    Block.box(6, 2, 6, 10, 4, 7),
                    Block.box(5, 2, 5, 6, 3, 6),
                    Block.box(12, 0, 3, 13, 1, 4),
                    Block.box(11, 0, 6, 12, 3, 10),
                    Block.box(6, 2, 11, 10, 3, 12))
            .reduce((v1, v2) -> Shapes.join(v1, v2, BooleanOp.OR));

    @Override
    public void appendHoverText(ItemStack p_49816_, @Nullable BlockGetter p_49817_, List<Component> p_49818_, TooltipFlag p_49819_) {
        p_49818_.add(Component.literal("A depleted deposit of tumblestone, you may have to wait to get more"));
        super.appendHoverText(p_49816_, p_49817_, p_49818_, p_49819_);
    }

    @Override
    public void randomTick(BlockState state, ServerLevel world, BlockPos pos, RandomSource p_60554_) {
        world.setBlock(pos, BlockInit.TUMBLESTONE_ORE.get().defaultBlockState().setValue(FACING, state.getValue(FACING)), 2);
        super.randomTick(state, world, pos, p_60554_);
    }
}

