package com.ladestitute.ancientlegends.blocks;

import com.ladestitute.ancientlegends.AncientLegendsMain;
import com.ladestitute.ancientlegends.registry.BlockInit;
import com.ladestitute.ancientlegends.registry.ItemInit;
import com.mojang.serialization.MapCodec;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.HorizontalDirectionalBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.BooleanOp;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.neoforged.neoforge.items.ItemHandlerHelper;

import javax.annotation.Nullable;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

public class ApricornTreeBlock extends HorizontalDirectionalBlock {

    public static final MapCodec<ApricornTreeBlock> CODEC = simpleCodec(ApricornTreeBlock::new);

    @Override
    protected MapCodec<? extends HorizontalDirectionalBlock> codec() {
        return CODEC;
    }

    public ApricornTreeBlock(Properties properties) {
        super(properties);
        runCalculation(SHAPE.orElse(Shapes.block()));
    }

    protected void runCalculation(VoxelShape shape) {
        for (Direction direction : Direction.values())
            SHAPES.put(direction, AncientLegendsMain.calculateShapes(direction, shape));
    }

    private static final Map<Direction, VoxelShape> SHAPES = new EnumMap<>(Direction.class);

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
        super.createBlockStateDefinition(builder);
        builder.add(FACING);
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter level, BlockPos pos, CollisionContext context) {
        return SHAPES.get(state.getValue(FACING));
    }

    @Override
    public BlockState getStateForPlacement(BlockPlaceContext context) {
        return defaultBlockState().setValue(FACING, context.getHorizontalDirection().getOpposite());
    }

    private static final Optional<VoxelShape> SHAPE = Stream
            .of(Block.box(7, 0, 7, 10, 8, 10),
                    Block.box(5, 8, 5, 12, 14, 12),
                    Block.box(6, 14, 6, 11, 15, 11
                    ))
            .reduce((v1, v2) -> Shapes.join(v1, v2, BooleanOp.OR));

    @Override
    public void appendHoverText(ItemStack p_49816_, @Nullable BlockGetter p_49817_, List<Component> p_49818_, TooltipFlag p_49819_) {
        p_49818_.add(Component.literal("A tree full of apricorns."));
        super.appendHoverText(p_49816_, p_49817_, p_49818_, p_49819_);
    }

    protected boolean mayPlaceOn(BlockState p_51042_, BlockGetter p_51043_, BlockPos p_51044_) {
        return p_51042_.is(Blocks.SAND) || p_51042_.is(Blocks.RED_SAND) ||
                p_51042_.is(Blocks.DIRT) || p_51042_.is(Blocks.GRASS_BLOCK) ||
                p_51042_.is(Blocks.STONE) || p_51042_.is(Blocks.TERRACOTTA);
    }

    @Override
    public boolean canSurvive(BlockState p_51028_, LevelReader p_51029_, BlockPos p_51030_) {
        BlockPos blockpos = p_51030_.below();
        return this.mayPlaceOn(p_51029_.getBlockState(blockpos), p_51029_, blockpos);
    }

    @Override
    public InteractionResult use(BlockState state, Level world, BlockPos pos, Player player, InteractionHand p_60507_, BlockHitResult p_60508_) {
        ItemHandlerHelper.giveItemToPlayer(player, ItemInit.APRICORN.get().getDefaultInstance());
        ItemHandlerHelper.giveItemToPlayer(player, ItemInit.APRICORN.get().getDefaultInstance());
        ItemHandlerHelper.giveItemToPlayer(player, ItemInit.APRICORN.get().getDefaultInstance());
        ItemHandlerHelper.giveItemToPlayer(player, ItemInit.APRICORN.get().getDefaultInstance());
        world.setBlock(pos, BlockInit.EMPTY_APRICORN_TREE.get().defaultBlockState().setValue(FACING, state.getValue(FACING)), 2);
        return super.use(state, world, pos, player, p_60507_, p_60508_);
    }
}

