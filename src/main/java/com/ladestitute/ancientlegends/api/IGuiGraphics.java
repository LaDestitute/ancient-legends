package com.ladestitute.ancientlegends.api;

import com.mojang.blaze3d.vertex.PoseStack;

public interface IGuiGraphics {
    void setPose(PoseStack pose);
}

