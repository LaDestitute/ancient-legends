package com.ladestitute.ancientlegends.api;

import com.mojang.blaze3d.vertex.PoseStack;

public interface IGuiGraphicsSetter {
    void setPose(PoseStack pose);
}
