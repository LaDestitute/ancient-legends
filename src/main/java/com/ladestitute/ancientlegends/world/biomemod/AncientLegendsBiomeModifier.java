package com.ladestitute.ancientlegends.world.biomemod;

import com.ladestitute.ancientlegends.registry.BiomeModifiersInit;
import com.ladestitute.ancientlegends.registry.EntityTypeInit;
import com.mojang.serialization.Codec;
import net.minecraft.core.Holder;
import net.minecraft.tags.BiomeTags;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.biome.MobSpawnSettings;
import net.neoforged.neoforge.common.Tags;
import net.neoforged.neoforge.common.world.BiomeModifier;
import net.neoforged.neoforge.common.world.ModifiableBiomeInfo;

public class AncientLegendsBiomeModifier implements BiomeModifier {
    public static final AncientLegendsBiomeModifier INSTANCE = new AncientLegendsBiomeModifier();

    @Override
    public void modify(Holder<Biome> biome, Phase phase, ModifiableBiomeInfo.BiomeInfo.Builder builder) {
        if (phase == Phase.ADD) {
            if (biome.is(BiomeTags.IS_OVERWORLD) && !biome.is(Biomes.DEEP_DARK) && !biome.is(Tags.Biomes.IS_VOID)) {

                if (biome.is(Biomes.MANGROVE_SWAMP) || biome.is(Biomes.SWAMP) || biome.is(Biomes.SUNFLOWER_PLAINS) ||
                        biome.is(Biomes.PLAINS) || biome.is(Biomes.MEADOW) ||
                        biome.is(Biomes.FOREST)|| biome.is(Biomes.FLOWER_FOREST) || biome.is(Biomes.BIRCH_FOREST) ||
                        biome.is(Biomes.OLD_GROWTH_BIRCH_FOREST)||biome.is(Biomes.RIVER)) {
                        builder.getMobSpawnSettings().getSpawner(MobCategory.CREATURE).add(
                                new MobSpawnSettings.SpawnerData(EntityTypeInit.BIDOOF.get(), 75, 1, 3)
                        );
                }
                if (biome.is(Biomes.MANGROVE_SWAMP) || biome.is(Biomes.SWAMP) || biome.is(Biomes.SUNFLOWER_PLAINS) || biome.is(Biomes.PLAINS) || biome.is(Biomes.MEADOW) ||
                        biome.is(Biomes.WINDSWEPT_HILLS)|| biome.is(Biomes.WINDSWEPT_GRAVELLY_HILLS) ||
                        biome.is(Biomes.BEACH)||biome.is(Biomes.STONY_SHORE)) {
                    builder.getMobSpawnSettings().getSpawner(MobCategory.CREATURE).add(
                            new MobSpawnSettings.SpawnerData(EntityTypeInit.STARLY.get(), 75, 1, 2)
                    );
                }
            }
        }
    }

    @Override
    public Codec<? extends BiomeModifier> codec() {
        return BiomeModifiersInit.POKEMON_ENTITY_MODIFIER_TYPE.get();
    }

}

