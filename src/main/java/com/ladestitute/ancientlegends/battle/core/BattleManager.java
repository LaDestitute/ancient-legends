package com.ladestitute.ancientlegends.battle.core;

import com.ladestitute.ancientlegends.battle.constructors.PokemonMove;
import com.ladestitute.ancientlegends.battle.registry.PokemonMoveInit;
import com.ladestitute.ancientlegends.battle.util.TypeMatchLookup;
import com.ladestitute.ancientlegends.battle.util.WildPokemonAI;
import com.ladestitute.ancientlegends.client.ticker.BattleMusicTicker;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.PokemonEntity;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;

import java.util.*;
import java.util.stream.Collectors;

public class BattleManager {
    //Fields
    public static BattleManager instance;
    private final Level level;
    public final List<PokemonEntity> playerPokemonList;
    public final List<PokemonEntity> enemyPokemonList;
    private final Map<PokemonEntity, Integer> actionTimers;
    private Timer battleTimer;
    private Timer teleportTimer;
    public boolean isPlayerTurn = false;
    public boolean battleOver = false;
    private final Map<PokemonEntity, double[]> initialPositions;
    public int wild_maxhp;
    public int wild_hp;
    public int wild_atk;
    public int wild_spatk;
    public int wild_speed;
    public int user_maxhp;
    public int user_hp;
    public int user_def;
    public int user_spdef;
    public int user_speed;
    private boolean backstrike;
    private int currentTurnNumber;

    private Map<PokemonEntity, RolloutState> rolloutStateMap;
    private static class RolloutState {
        private int turnCounter;
        private int basePower;

        public RolloutState() {
            this.turnCounter = 0;
            this.basePower = 40;
        }

        public void incrementTurn() {
            this.turnCounter++;
            this.basePower *= 1.5;
        }

        public void reset() {
            this.turnCounter = 0;
            this.basePower = 40;
        }

        public int getTurnCounter() {
            return turnCounter;
        }

        public int getBasePower() {
            return basePower;
        }
    }

    //Core
    public BattleManager(Level level, List<PokemonEntity> playerPokemonList, List<PokemonEntity> enemyPokemonList, Boolean backstrike) {
        instance = this;
        this.level = level;
        this.playerPokemonList = playerPokemonList;
        this.enemyPokemonList = enemyPokemonList;
        this.actionTimers = new HashMap<>();
        this.initialPositions = new HashMap<>();
        this.backstrike = backstrike;
        this.rolloutStateMap = new HashMap<>();
        this.currentTurnNumber = 1;

        initializeTimers(playerPokemonList);
        initializeTimers(enemyPokemonList);
        setInitialPositions(playerPokemonList);
        setInitialPositions(enemyPokemonList);

        BattleMusicTicker.startBattleMusic(level);
    }

    private void incrementTurnNumber() {
        currentTurnNumber++;
    }

    public int getCurrentTurn() {
        return currentTurnNumber;
    }

    private void initializeTimers(List<PokemonEntity> pokemonList) {
        for (PokemonEntity p : pokemonList) {
            actionTimers.put(p, getInitialActionSpeed(p.getPokeSpeed()));
        }
    }

    private void setInitialPositions(List<PokemonEntity> pokemonList) {
        for (PokemonEntity p : pokemonList) {
            initialPositions.put(p, new double[]{p.getX(), p.getY(), p.getZ()});
        }
    }

    public void startBattle() {
        battleTimer = new Timer();
        battleTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                PokemonEntity playerPokemon = getPlayerPokemon();
                if (playerPokemon == null) return;
                List<PokemonMove> moves = playerPokemon.getMoveSlots();
                System.out.println("MOVE 1 PP IS: " + moves.get(0).getPP());
                processTurn();
                checkBattleOver();
                if (battleOver) {
                    battleTimer.cancel();
                    teleportTimer.cancel();
                    if(getEnemyPokemon() != null) {
                        getEnemyPokemon().discard();
                    }
                    if(getPlayerPokemon() != null)
                    {
                        getPlayerPokemon().discard();
                    }
                    BattleMusicTicker.stopBattleMusic(level);
                    endBattle();
                }
            }
        }, 0, 455); // Run the task every 1000 milliseconds (20 ticks or 1 seconds)

        teleportTimer = new Timer();
        teleportTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                teleportParticipants();
            }
        }, 0, 455); // Run the task every 500 milliseconds (10 ticks)
    }

    private void checkBattleOver() {
        PokemonEntity wildPokemon = getEnemyPokemon();
        if (wildPokemon != null && wildPokemon.getPokeHP() <= 0) {
            battleOver = true;
            endBattle();
        }
    }

    private void processTurn() {
        //Set int values as a fallback
        if(getEnemyPokemon() != null) {
            wild_maxhp = getEnemyPokemon().getPokeMaxHP();
            wild_hp = getEnemyPokemon().getPokeHP();
            wild_atk = getEnemyPokemon().getPokeAttack();
            wild_spatk = getEnemyPokemon().getPokeSpecialAttack();
            wild_speed = getEnemyPokemon().getPokeSpeed();
        }
        if(getPlayerPokemon() != null) {
            user_maxhp = getPlayerPokemon().getPokeMaxHP();
            user_hp = getPlayerPokemon().getPokeHP();
            user_def = getPlayerPokemon().getPokeDefense();
            user_spdef = getPlayerPokemon().getPokeSpecialDefense();
            user_speed = getPlayerPokemon().getPokeSpeed();
        }
        if (!battleOver) {
            //Give player first turn if they start with a backstrike
            if(backstrike)
            {
                Player player = (Player) getPlayerPokemon().getOwner();
                actionTimers.put(getPlayerPokemon(), 0);
                actionTimers.put(getEnemyPokemon(), 1);
                if(player != null)
                {
                    player.sendSystemMessage(Component.literal("You caught " + getEnemyPokemon().toString() + " unawares!"));
                }
            }
            for (PokemonEntity p : actionTimers.keySet()) {
                int timer = actionTimers.get(p);
                timer--;
                if (timer <= 0) {
                    if (p.isTame()) {
                        isPlayerTurn = true;
                        handlePlayerTurn(p);
                    } else {
                        isPlayerTurn = false;
                        handleEnemyTurn(p);
                    }
                    actionTimers.put(p, getInitialActionSpeed(p.getPokeSpeed()));
                } else {
                    actionTimers.put(p, timer);
                }
            }
        }
    }

    public PokemonEntity getPlayerPokemon() {
        return playerPokemonList.isEmpty() ? null : playerPokemonList.get(0);
    }

    public PokemonEntity getEnemyPokemon() {
        return enemyPokemonList.isEmpty() ? null : enemyPokemonList.get(0);
    }

    private void teleportParticipants() {
        for (PokemonEntity p : playerPokemonList) {
            double[] initialPos = initialPositions.get(p);
            teleportEntity(p, initialPos[0], initialPos[1], initialPos[2]);
        }
        for (PokemonEntity p : enemyPokemonList) {
            double[] initialPos = initialPositions.get(p);
            teleportEntity(p, initialPos[0], initialPos[1], initialPos[2]);
        }
    }

    private void teleportEntity(PokemonEntity pokemon, double x, double y, double z) {
        // Reset motion to prevent the Pokémon from moving on its own
        pokemon.setDeltaMovement(0, 0, 0);
        pokemon.setNoAi(true); // Disable AI temporarily
        if (isSafePosition(pokemon.level(), x, y, z)) {
            pokemon.teleportTo(x, y, z);
        }
        pokemon.setNoAi(false); // Re-enable AI
    }

    private boolean isSafePosition(Level level, double x, double y, double z) {
        // Check if the block at the target position is air or non-collidable
        BlockPos pos = new BlockPos((int) x, (int) y, (int) z);
        return level.getBlockState(pos).canBeReplaced() &&
                level.getBlockState(pos.above()).canBeReplaced();
    }

    public void endBattle() {
        // Handle end of battle logic
    }

    public static void initiateBattle(Level level, Player player, PokemonEntity userPokemon, PokemonEntity wildPokemon, boolean backstrike) {
        List<PokemonEntity> playerPokemon = new ArrayList<>();
        List<PokemonEntity> enemyPokemon = new ArrayList<>();
        playerPokemon.add(userPokemon);
        enemyPokemon.add(wildPokemon);
        userPokemon.setPos(player.blockPosition().getX() + 1.5, player.blockPosition().getY(), player.blockPosition().getZ());
        wildPokemon.setPos(player.blockPosition().getX() - 1.5, player.blockPosition().getY(), player.blockPosition().getZ());

        BattleManager battleManager = new BattleManager(level, playerPokemon, enemyPokemon, backstrike);
        battleManager.startBattle();
    }

    //Turns
    int misschance;
    private void handlePlayerTurn(PokemonEntity playerPokemon) {
        Player player = (Player) playerPokemon.getOwner();
        if (player != null) {
            player.playSound(SoundEvents.BEACON_ACTIVATE, 1.5F, 1F);
            player.sendSystemMessage(Component.literal("Player's turn: " + playerPokemon.getName().getString()));
        }
    }

    public int getPlayerActionTimer() {
        PokemonEntity playerPokemon = getPlayerPokemon();
        if (playerPokemon != null && actionTimers.containsKey(playerPokemon)) {
            return actionTimers.get(playerPokemon);
        }
        return -1; // Return -1 if there is no player Pokemon or the timer is not found
    }

    public void handlePlayerMoveInput(Player player, int moveSlot) {
        if(!isPlayerTurn)
        {
            return;
        }
        PokemonEntity playerPokemon = getPlayerPokemon();
        if (playerPokemon == null) return;

        List<PokemonMove> moves = playerPokemon.getMoveSlots();
        if (moveSlot < 0 || moveSlot >= moves.size()) return;

        PokemonMove selectedMove = moves.get(moveSlot);
        misschance = random.nextInt(selectedMove.getAccuracy()+1);
        if (selectedMove == null || selectedMove.getName().equalsIgnoreCase("empty") || selectedMove.getPP() <= 0) {
            player.sendSystemMessage(Component.literal("Invalid move selection."));
            return;
        }

        List<PokemonMove> validMoves = getValidMoves(playerPokemon);
        boolean allMovesHaveNoPP = validMoves.stream().allMatch(move -> move.getPP() == 0);
        if (allMovesHaveNoPP) {
            executePlayerMove(getPlayerPokemon(), getEnemyPokemon(), PokemonMoveInit.STRUGGLE.get()); // No valid moves available
            if (player != null) {
                player.sendSystemMessage(Component.literal(playerPokemon.getName().getString() + " used " + selectedMove.getName() + "!"));
            }
        } else if (selectedMove.getName().equalsIgnoreCase("rollout")) {
            startRollout(playerPokemon);
            executeRolloutMove(playerPokemon, getEnemyPokemon());
            if (player != null) {
                player.sendSystemMessage(Component.literal(playerPokemon.getName().getString() + " used " + selectedMove.getName() + "!"));
            }
        }
        else if(selectedMove.getAccuracy() == 100|selectedMove == PokemonMoveInit.AERIAL_ACE.get()) {
            executePlayerMove(getPlayerPokemon(), getEnemyPokemon(), selectedMove);
            if (player != null) {
                player.sendSystemMessage(Component.literal(playerPokemon.getName().getString() + " used " + selectedMove.getName() + "!"));
            }
        }
        else if(misschance <= selectedMove.getAccuracy())
        {
            executePlayerMove(getPlayerPokemon(), getEnemyPokemon(), selectedMove);
            if (player != null) {
                player.sendSystemMessage(Component.literal(playerPokemon.getName().getString() + " used " + selectedMove.getName() + "!"));
            }
        }
        else if(misschance > selectedMove.getAccuracy())
        {
            if(player != null)
            {
                player.sendSystemMessage(Component.literal(getEnemyPokemon().toString() + " avoided the attack!"));
            }
        }
        // End player's turn and switch to enemy turn
        incrementTurnNumber();
    }

    PokemonMove bestMove; // AI Decision

    private void handleEnemyTurn(PokemonEntity enemyPokemon) {
        Player player = getPlayerPokemon().getOwner() instanceof Player ? (Player) getPlayerPokemon().getOwner() : null;
        bestMove = WildPokemonAI.chooseBestMove(enemyPokemon, getPlayerPokemon()); // AI Decision
        if (isRolloutActive(enemyPokemon)) {
            bestMove = PokemonMoveInit.ROLLOUT.get();
            executeRolloutMove(enemyPokemon, getPlayerPokemon());
        } else if (player != null) {
            player.sendSystemMessage(Component.literal("The wild " + enemyPokemon.getName().getString() + " used " + bestMove.getName() + "!"));
        }
        if(bestMove.getAccuracy() == 100|bestMove == PokemonMoveInit.AERIAL_ACE.get()) {
            executeEnemyMove(enemyPokemon, getPlayerPokemon(), bestMove);
        }
        misschance = random.nextInt(bestMove.getAccuracy()+1);
        if(misschance <= bestMove.getAccuracy())
        {
            executeEnemyMove(enemyPokemon, getPlayerPokemon(), bestMove);
        }
        else if(misschance > bestMove.getAccuracy())
        {
            if(player != null)
            {
                player.sendSystemMessage(Component.literal(getPlayerPokemon().toString() + " avoided the attack!"));
            }
        }
        actionTimers.put(enemyPokemon, getInitialActionSpeed(enemyPokemon.getPokeSpeed()));
        incrementTurnNumber();
    }

    private void executePlayerMove(PokemonEntity attacker, PokemonEntity defender, PokemonMove move) {
        PokemonMove attackerMove = attacker.getMoveSlots().stream()
                .filter(m -> m.getName().equals(move.getName()))
                .findFirst()
                .orElse(null);

        if (attackerMove != null && attackerMove.getPP() > 0) {
            attackerMove.setPP(move.getPP() - 1);  // Reduce PP for the player's move
            attacker.playSound(SoundEvents.PLAYER_ATTACK_CRIT, 1.0F, 1.0F);
            damage = doDamage(attacker, defender, move);
            defender.setPokeHP(defender.getPokeHP() - damage); // Reduce defender's HP
            Player player = (Player) attacker.getOwner();

            // Handle type effectiveness messages
            double effectiveness = calculateTypeMatchup(move, defender);
            if (effectiveness == 2.0) {
                player.sendSystemMessage(Component.literal("It's super effective!"));
            } else if (effectiveness == 0.5) {
                player.sendSystemMessage(Component.literal("It's not very effective..."));
            } else if (effectiveness == 0.0) {
                player.sendSystemMessage(Component.literal("It seems to have no effect on " + defender.getName().getString() + "..."));
            }

            doAdditionalEffects(attacker, defender, move);
            applyActionSpeedModifiers(attacker, defender, move);
            actionTimers.put(attacker, getInitialActionSpeed(attacker.getPokeSpeed()));
        }
    }

    private void executeEnemyMove(PokemonEntity attacker, PokemonEntity defender, PokemonMove move) {
        PokemonMove attackerMove = attacker.getMoveSlots().stream()
                .filter(m -> m.getName().equals(move.getName()))
                .findFirst()
                .orElse(null);

        if (attackerMove != null && attackerMove.getPP() > 0) {
            attackerMove.setPP(move.getPP() - 1);  // Reduce PP for the enemy's move
            attacker.playSound(SoundEvents.PLAYER_ATTACK_CRIT, 1.0F, 1.0F);
            damage = doDamage(attacker, defender, move);
            defender.setPokeHP(defender.getPokeHP() - damage); // Reduce defender's HP
            Player player = (Player) defender.getOwner();

            // Handle type effectiveness messages
            double effectiveness = calculateTypeMatchup(move, defender);
            if (effectiveness == 2.0) {
                player.sendSystemMessage(Component.literal("It's super effective!"));
            } else if (effectiveness == 0.5) {
                player.sendSystemMessage(Component.literal("It's not very effective..."));
            } else if (effectiveness == 0.0) {
                player.sendSystemMessage(Component.literal("It seems to have no effect on " + defender.getName().getString() + "..."));
            }

            doAdditionalEffects(attacker, defender, move);
            applyActionSpeedModifiers(attacker, defender, move);
            actionTimers.put(attacker, getInitialActionSpeed(attacker.getPokeSpeed()));
        }
    }

    private void applyActionSpeedModifiers(PokemonEntity attacker, PokemonEntity defender, PokemonMove move) {
        int attackerSpeed;
        int defenderSpeed;
        if(attacker.equals(getEnemyPokemon())) {
            attackerSpeed = getInitialActionSpeed(wild_speed);
            defenderSpeed = getInitialActionSpeed(user_speed);
            attackerSpeed = attackerSpeed-move.getNormalSelfActionSpeedMod();
            defenderSpeed = defenderSpeed-move.getNormalTargetActionSpeedMod();
            actionTimers.put(attacker, Math.max(attackerSpeed, 5)); // Ensure timer doesn't go below 5
            actionTimers.put(defender, Math.max(defenderSpeed, 5)); // Ensure timer doesn't go below 5
        }
        else if(!attacker.equals(getEnemyPokemon())) {
            attackerSpeed = getInitialActionSpeed(user_speed);
            defenderSpeed = getInitialActionSpeed(wild_speed);
            attackerSpeed = attackerSpeed-move.getNormalSelfActionSpeedMod();
            defenderSpeed = defenderSpeed-move.getNormalTargetActionSpeedMod();
            actionTimers.put(attacker, Math.max(attackerSpeed, 5)); // Ensure timer doesn't go below 5
            actionTimers.put(defender, Math.max(defenderSpeed, 5)); // Ensure timer doesn't go below 5
        }

        //Todo: strong and agile modifiers
    }

    //Stats
    public int getInitialActionSpeed(int speed) {
        if (speed <= 15) {
            return 14;
        } else if (speed <= 31) {
            return 13;
        } else if (speed <= 55) {
            return 12;
        } else if (speed <= 88) {
            return 11;
        } else if (speed <= 129) {
            return 10;
        } else if (speed <= 181) {
            return 9;
        } else if (speed <= 242) {
            return 8;
        } else if (speed <= 316) {
            return 7;
        } else if (speed <= 401) {
            return 6;
        } else {
            return 5;
        }
    }

    private int getSafeStat(int stat) {
        return stat == 0 ? 1 : stat; // Prevent division by zero by returning 1 if stat is 0
    }

    public int getWildPokemonAttackStat() {
        return getSafeStat(wild_atk);
    }

    public int getWildPokemonSpAttackStat() {
        return getSafeStat(wild_spatk);
    }

    public int getWildPokemonMaxHPStat() {
        return getSafeStat(wild_maxhp);
    }

    public int getWildPokemonHPStat() {
        return getSafeStat(wild_hp);
    }

    public int getUserPokemonDefStat() {
        return getSafeStat(user_def);
    }

    public int getUserPokemonSpDefStat() {
        return getSafeStat(user_spdef);
    }

    public int getUserPokemonMaxHPStat() {
        return getSafeStat(user_maxhp);
    }

    public int getUserPokemonHPStat() {
        return getSafeStat(user_hp);
    }

    //Damage calculations
    public int damage;
    private static final Random random = new Random();

    public int doDamage(PokemonEntity attacker, PokemonEntity defender, PokemonMove move)
    {
        if(move.getCategory().equals("Special"))
        {
            damage = (((2 * attacker.getPokemonLevel() / 5 + 2) * move.getPower() * attacker.getPokeSpecialAttack() / defender.getPokeSpecialDefense()) / 50) + 2;
            damage = damage * (85 + random.nextInt(16)) / 100;
            if (isStab(move, attacker)) {
                damage = Math.round((int) (damage * 1.5));
            }
            double typeEffectiveness = calculateTypeMatchup(move, defender);
            damage = (int) (damage * typeEffectiveness);
        }
        else damage = (((2 * attacker.getPokemonLevel() / 5 + 2) * move.getPower() * attacker.getPokeAttack() / defender.getPokeDefense()) / 50) + 2;
        damage = damage * (85 + random.nextInt(16)) / 100;
        if (isStab(move, attacker)) {
            damage = Math.round((int) (damage * 1.5));
        }
        double typeEffectiveness = calculateTypeMatchup(move, defender);
        damage = (int) (damage * typeEffectiveness);
        return damage;
    }

    //Util
    public static boolean isStab(PokemonMove move, PokemonEntity userPokemon) {
        String moveType = TypeMatchLookup.typeToString(move.getType());
        String userType1 = TypeMatchLookup.typeToString(userPokemon.getType1());
        String userType2 = TypeMatchLookup.typeToString(userPokemon.getType2());

        return moveType.equals(userType1) || moveType.equals(userType2);
    }

    public static double calculateTypeMatchup(PokemonMove move, PokemonEntity userPokemon) {
        String moveType = TypeMatchLookup.typeToString(move.getType());
        String userType1 = TypeMatchLookup.typeToString(userPokemon.getType1());
        String userType2 = TypeMatchLookup.typeToString(userPokemon.getType2());

        double effectiveness = TypeMatchLookup.getEffectiveness(moveType, userType1, userType2);

        return effectiveness;
    }

    private static List<PokemonMove> getValidMoves(PokemonEntity pokemon) {
        return pokemon.getMoveSlots().stream()
                .filter(move -> move != null && !move.getName().equalsIgnoreCase("empty"))
                .collect(Collectors.toList());
    }

    public void doAdditionalEffects(PokemonEntity attacker, PokemonEntity defender, PokemonMove move)
    {
        if(attacker.equals(getEnemyPokemon()) && move == PokemonMoveInit.STRUGGLE.get())
        {
            attacker.setPokeHP(getWildPokemonHPStat()-Math.round((getWildPokemonMaxHPStat()/4)));
        }
        if(attacker.equals(getPlayerPokemon()) && move == PokemonMoveInit.STRUGGLE.get())
        {
            attacker.setPokeHP(getUserPokemonHPStat()-Math.round((getUserPokemonMaxHPStat()/4)));
        }
    }

    //Specialized moves
    private void startRollout(PokemonEntity pokemon) {
        if (!rolloutStateMap.containsKey(pokemon)) {
            rolloutStateMap.put(pokemon, new RolloutState());
        }
    }

    public boolean isRolloutActive(PokemonEntity pokemon) {
        return rolloutStateMap.containsKey(pokemon) && rolloutStateMap.get(pokemon).getTurnCounter() < 5;
    }

    private void executeRolloutMove(PokemonEntity attacker, PokemonEntity defender) {
        RolloutState rolloutState = rolloutStateMap.get(attacker);
        if (rolloutState != null) {
            rolloutState.incrementTurn();
            int rolloutPower = rolloutState.getBasePower();
            PokemonMove rolloutMove = PokemonMoveInit.ROLLOUT.get().cloneWithNewPower(rolloutPower);
            executePlayerMove(attacker, defender, rolloutMove);

            // Reset rollout if it misses
            misschance = random.nextInt(91); // 90% accuracy
            if (misschance > 90) {
                rolloutState.reset();
            }
        }
    }

    //Rendering
    public List<PokemonEntity> getTurnOrder(List<PokemonEntity> playerPokemon, List<PokemonEntity> enemyPokemon) {
        List<PokemonEntity> turnOrder = new ArrayList<>();
        int maxSize = Math.max(playerPokemonList.size(), enemyPokemonList.size());
        for (int i = 0; i < maxSize; i++) {
            if (i < playerPokemonList.size()) {
                turnOrder.add(playerPokemonList.get(i));
            }
            if (i < enemyPokemonList.size()) {
                turnOrder.add(enemyPokemonList.get(i));
            }
            if (i < playerPokemonList.size()) {
                turnOrder.add(playerPokemonList.get(i));
            }
            if (i < enemyPokemonList.size()) {
                turnOrder.add(enemyPokemonList.get(i));
            }
            if (i < playerPokemonList.size()) {
                turnOrder.add(playerPokemonList.get(i));
            }
            if (i < enemyPokemonList.size()) {
                turnOrder.add(enemyPokemonList.get(i));
            }
        }
        return turnOrder;
    }

}
