package com.ladestitute.ancientlegends.battle.constructors;

import java.util.Map;

public class PokemonLevelLearnList {
    private String pokemonName;
    private Map<Integer, PokemonMoveLevel> learnList;

    public PokemonLevelLearnList(String pokemonName, Map<Integer, PokemonMoveLevel> learnList) {
        this.pokemonName = pokemonName;
        this.learnList = learnList;
    }

    // Getters
    public String getPokemonName() {
        return pokemonName;
    }

    public Map<Integer, PokemonMoveLevel> getLearnList() {
        return learnList;
    }

    // Setters
    public void setPokemonName(String pokemonName) {
        this.pokemonName = pokemonName;
    }

    public void setLearnList(Map<Integer, PokemonMoveLevel> learnList) {
        this.learnList = learnList;
    }
}
