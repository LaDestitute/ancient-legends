package com.ladestitute.ancientlegends.battle.constructors;

public class PokemonMove {
    private String name; //Move's name
    private int type; //One of the 18 types
    private String category; // Physical, Special, Status
    private int power; //Base power
    private int strongPower; //Strong style base power
    private int agilePower; //Agile style base power
    private int pp; //PP
    private int maxPP; //PP
    private int accuracy; //Move accuracy, moves with "——%" on Bulbapedia are assigned as 0 or 100 depending on use-cases
    private int strongAccuracy; //Strong style move accuracy, moves with "——%" on Bulbapedia are assigned as 0 or 100 depending on use-cases
    private int agileAccuracy; //Agile style move accuracy, moves with "——%" on Bulbapedia are assigned as 0 or 100 depending on use-cases
    private int normalSelfActionSpeedMod; //self-action speed modifier for normal moves, anywhere from -5 to 8
    private int strongSelfActionSpeedMod; //self-action speed modifier for strong style moves, anywhere from -5 to 8
    private int agileSelfActionSpeedMod; //self-action speed modifier for agile moves, anywhere from -5 to 8
    private int normalTargetActionSpeedMod; //target-action speed modifier for normal moves, anywhere from -5 to 8
    private int strongTargetActionSpeedMod; //target-action speed modifier for strong style moves, anywhere from -5 to 8
    private int agileTargetActionSpeedMod; //target-action speed modifier for agile style moves, anywhere from -5 to 8
    private boolean usableOutsideBattle; //Is it usable outside of battle
    private int range; // one of three int-flags, 0 for single-target, 1 for self-only and 2 for random
    // swords dance, soft-boiled, iron defense, nasty plot, shelter, teleport, splash, bulk up, double hit,
    // lunar blessing, recover, acid armor, calm mind, power shift, take heart,
    // focus energy, rest, roost and victory dance are int-flag 1
    // struggle is int-flag 2, all other moves are int-flag 0

    public PokemonMove(String name, int type, String category, int power, int strongPower, int agilePower, int pp, int maxPP, int accuracy, int strongAccuracy, int agileAccuracy, int normalSelfActionSpeedMod, int strongSelfActionSpeedMod, int agileSelfActionSpeedMod, int normalTargetActionSpeedMod, int strongTargetActionSpeedMod, int agileTargetActionSpeedMod, boolean usableOutsideBattle, int range) {
        this.name = name;
        this.type = type;
        this.category = category;
        this.power = power;
        this.strongPower = strongPower;
        this.agilePower = agilePower;
        this.pp = pp;
        this.maxPP = maxPP;
        this.accuracy = accuracy;
        this.strongAccuracy = strongAccuracy;
        this.agileAccuracy = agileAccuracy;
        this.normalSelfActionSpeedMod = normalSelfActionSpeedMod;
        this.strongSelfActionSpeedMod = strongSelfActionSpeedMod;
        this.agileSelfActionSpeedMod = agileSelfActionSpeedMod;
        this.normalTargetActionSpeedMod = normalTargetActionSpeedMod;
        this.strongTargetActionSpeedMod = strongTargetActionSpeedMod;
        this.agileTargetActionSpeedMod = agileTargetActionSpeedMod;
        this.usableOutsideBattle = usableOutsideBattle;
        this.range = range;
    }

    // Getters
    public String getName() {
        return name;
    }

    public int getType() {
        return type;
    }

    public String getCategory() {
        return category;
    }

    public int getPower() {
        return power;
    }

    public int getStrongPower() {
        return strongPower;
    }

    public int getAgilePower() {
        return agilePower;
    }

    public int getPP() {
        return pp;
    }

    public int getMaxPP() {
        return maxPP;
    }

    public int getAccuracy() {
        return accuracy;
    }

    public int getStrongAccuracy() {
        return strongAccuracy;
    }

    public int getAgileAccuracy() {
        return agileAccuracy;
    }

    public int getNormalSelfActionSpeedMod() {
        return normalSelfActionSpeedMod;
    }

    public int getStrongSelfActionSpeedMod() {
        return strongSelfActionSpeedMod;
    }

    public int getAgileSelfActionSpeedMod() {
        return agileSelfActionSpeedMod;
    }

    public int getNormalTargetActionSpeedMod() {
        return normalTargetActionSpeedMod;
    }

    public int getStrongTargetActionSpeedMod() {
        return strongTargetActionSpeedMod;
    }

    public int getAgileTargetActionSpeedMod() {
        return agileTargetActionSpeedMod;
    }

    public boolean isUsableOutsideBattle() {
        return usableOutsideBattle;
    }

    public int getRange() {
        return range;
    }

    // Setters
    public void setName(String name) {
        this.name = name;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public void setStrongPower(int strongPower) {
        this.strongPower = strongPower;
    }

    public void setAgilePower(int agilePower) {
        this.agilePower = agilePower;
    }

    public void setPP(int pp) {
        this.pp = pp;
    }

    public void setMaxPP(int pp) {
        this.maxPP = pp;
    }

    public void setAccuracy(int accuracy) {
        this.accuracy = accuracy;
    }

    public void setStrongAccuracy(int strongAccuracy) {
        this.strongAccuracy = strongAccuracy;
    }

    public void setAgileAccuracy(int agileAccuracy) {
        this.agileAccuracy = agileAccuracy;
    }

    public void setNormalSelfActionSpeedMod(int normalSelfActionSpeedMod) {
        this.normalSelfActionSpeedMod = normalSelfActionSpeedMod;
    }

    public void setStrongSelfActionSpeedMod(int strongSelfActionSpeedMod) {
        this.strongSelfActionSpeedMod = strongSelfActionSpeedMod;
    }

    public void setAgileSelfActionSpeedMod(int agileSelfActionSpeedMod) {
        this.agileSelfActionSpeedMod = agileSelfActionSpeedMod;
    }

    public void setNormalTargetActionSpeedMod(int normalTargetActionSpeedMod) {
        this.normalTargetActionSpeedMod = normalTargetActionSpeedMod;
    }

    public void setStrongTargetActionSpeedMod(int strongTargetActionSpeedMod) {
        this.strongTargetActionSpeedMod = strongTargetActionSpeedMod;
    }

    public void setAgileTargetActionSpeedMod(int agileTargetActionSpeedMod) {
        this.agileTargetActionSpeedMod = agileTargetActionSpeedMod;
    }

    public void setUsableOutsideBattle(boolean usableOutsideBattle) {
        this.usableOutsideBattle = usableOutsideBattle;
    }

    public void setRange(int range) {
        this.range = range;
    }

    @Override
    public String toString() {
        return name + " [Power: " + power + ", Type: " + type + ", PP: " + pp + ", Accuracy: " + accuracy + "]";
    }

    public PokemonMove cloneWithNewPower(int newPower) {
        return new PokemonMove(this.name, this.type, this.category, newPower, this.strongPower, this.agilePower, this.pp, this.maxPP, this.accuracy, this.strongAccuracy, this.agileAccuracy, this.normalSelfActionSpeedMod, this.strongSelfActionSpeedMod, this.agileSelfActionSpeedMod, this.normalTargetActionSpeedMod, this.strongTargetActionSpeedMod, this.agileTargetActionSpeedMod, this.usableOutsideBattle, this.range);
    }
}