package com.ladestitute.ancientlegends.battle.constructors;

public class PokemonMoveLevel {
    private int learnLevel;
    private int masteryLevel;
    private PokemonMove move;

    public PokemonMoveLevel(int learnLevel, int masteryLevel, PokemonMove move) {
        this.learnLevel = learnLevel;
        this.masteryLevel = masteryLevel;
        this.move = move;
    }

    public int getLearnLevel() {
        return learnLevel;
    }

    public int getMasteryLevel() {
        return masteryLevel;
    }

    public PokemonMove getMove() {
        return move;
    }

    public void setLearnLevel(int set) {
        this.learnLevel = set;
    }

    public void setMasteryLevel(int set) {
        this.masteryLevel = set;
    }

    public void setMove(PokemonMove set) {
        this.move = set;
    }


}
