package com.ladestitute.ancientlegends.battle.util;

import com.ladestitute.ancientlegends.battle.constructors.PokemonMove;
import com.ladestitute.ancientlegends.battle.core.BattleManager;
import com.ladestitute.ancientlegends.battle.registry.PokemonMoveInit;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.PokemonEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class WildPokemonAI {

    private static final Random random = new Random();

    private static List<PokemonMove> getValidMoves(PokemonEntity pokemon) {
        return pokemon.getMoveSlots().stream()
                .filter(move -> move != null && !move.getName().equalsIgnoreCase("empty"))
                .collect(Collectors.toList());
    }

    public static PokemonMove chooseBestMove(PokemonEntity attacker, PokemonEntity defender) {
        BattleManager battleManager = BattleManager.instance;
        if (battleManager.isRolloutActive(attacker)) {
            return PokemonMoveInit.ROLLOUT.get();
        }

        List<PokemonMove> moveSlots = attacker.getMoveSlots();
        PokemonMove bestMove = null;
        int highestScore = Integer.MIN_VALUE;
        int damage;

        List<PokemonMove> validMoves = getValidMoves(attacker);

        // Check if all valid moves have 0 PP
        boolean allMovesHaveNoPP = validMoves.stream().allMatch(move -> move.getPP() == 0);
        if (allMovesHaveNoPP) {
            bestMove = PokemonMoveInit.STRUGGLE.get(); // No valid moves available
        }
        else for (PokemonMove move : moveSlots) {
            int score = 20;

            // Ignore moves that will fail
            if (willMoveFail(move, attacker, defender)||move.getPP() == 0) {
                score = -100;
            } else {
                // Self-destruct check
                if (isSelfDestruct(move) && defender.getPokeHP() > defender.getPokeMaxHP() / 2) {
                    score -= 5;
                }

                // Damage calculation and RNG roll
                if(move.getCategory().equals("Status"))
                {
                    damage = 0;
                }
                else if(move.getCategory().equals("Special"))
                {
                    damage = (((2 * attacker.getPokemonLevel() / 5 + 2) * move.getPower() * BattleManager.instance.getWildPokemonSpAttackStat() / BattleManager.instance.getUserPokemonSpDefStat()) / 50) + 2;
                }
                else damage = (((2 * attacker.getPokemonLevel() / 5 + 2) * move.getPower() * BattleManager.instance.getWildPokemonAttackStat() / BattleManager.instance.getUserPokemonDefStat()) / 50) + 2;
                if (damage >= defender.getPokeHP()) {
                    score += 4;
                }

                if (move.getPower() > 1 && damage > defender.getPokeHP()) {
                    score += 4;
                } else if (move.getPower() > 1 && move != getHighestDamageMove(moveSlots, attacker, defender)) {
                    score -= 1;
                }

                if (isSelfDestruct(move) && defender.getPokeHP() > defender.getPokeMaxHP() / 2) {
                    score -= 5;
                } else if (isSelfDestruct(move) && defender.getPokeHP() > defender.getPokeMaxHP() / 4 && random.nextInt(100) < 92) {
                    score += 5;
                }

                if(move.getNormalSelfActionSpeedMod() < 0||move.getNormalTargetActionSpeedMod() > 0)
                {
                    score += 4;
                }

                if (random.nextInt(100) < 69) {
                    if(BattleManager.calculateTypeMatchup(getHighestPowerMove(moveSlots), attacker) >= 4.0||
                            BattleManager.calculateTypeMatchup(move, attacker) >= 4.0 && move.getPower() == 1) {
                        score += 4;
                    }
                }

                if (BattleManager.calculateTypeMatchup(move, attacker) == 2.0 && BattleManager.calculateTypeMatchup(move, attacker) < 2.1) {
                    score += 4;
                }

                if (BattleManager.calculateTypeMatchup(move, attacker) == 4.0) {
                    score += 8;
                }

                if (BattleManager.calculateTypeMatchup(move, attacker) == 0.5) {
                    score -= 8;
                }

                if (BattleManager.isStab(move, attacker)) {
                    score += 4;
                }

                //Todo, add checks for poisoned, burned or frostbit for this
                if (BattleManager.instance.wild_speed > BattleManager.instance.user_speed) {
                    score += 1;
                }

                if (move.getPower() > 1 && move != getHighestPowerMove(moveSlots)) {
                    score -= 5;
                } else if (move.getPower() == getHighestPowerMove(moveSlots).getPower()) {
                    score += 5;
                }
            }

            if (score > highestScore) {
                highestScore = score;
                bestMove = move;
            } else if (score == highestScore && random.nextBoolean()) {
                bestMove = move;
            }
        }

        if (bestMove == null) {
            bestMove = moveSlots.get(0);
        }

        return bestMove;
    }

    private static boolean willMoveFail(PokemonMove move, PokemonEntity attacker, PokemonEntity defender) {
        if(BattleManager.calculateTypeMatchup(move, attacker) == 0.0)
        {
            return true;
        }
        else return false;
    }

    private static boolean isSelfDestruct(PokemonMove move) {
        // Implement logic to check if the move is self-destruct
        if(move.getName().equals("self_destruct"))
        {
            return true;
        }
        else return false;
    }

    private static PokemonMove getHighestDamageMove(List<PokemonMove> moveSlots, PokemonEntity attacker, PokemonEntity defender) {
        PokemonMove highestDamageMove = null;
        int highestDamage = 0;

        for (PokemonMove move : moveSlots) {
            if (move.getPower() > 1) {
                int damage = DamageCalculations.calculateDamage(attacker, defender, move);
                if (damage > highestDamage) {
                    highestDamage = damage;
                    highestDamageMove = move;
                }
            }
        }

        return highestDamageMove;
    }

    private static PokemonMove getHighestPowerMove(List<PokemonMove> moveSlots) {
        PokemonMove highestPowerMove = null;
        int highestPower = 0;

        for (PokemonMove move : moveSlots) {
            if (move.getPower() > highestPower) {
                highestPower = move.getPower();
                highestPowerMove = move;
            }
        }

        return highestPowerMove;
    }
}