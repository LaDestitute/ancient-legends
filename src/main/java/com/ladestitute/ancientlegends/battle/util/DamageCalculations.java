package com.ladestitute.ancientlegends.battle.util;

import com.ladestitute.ancientlegends.battle.constructors.PokemonMove;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.PokemonEntity;

import java.util.Objects;
import java.util.Random;

public class DamageCalculations {
    private static final Random random = new Random();

    public static int calculateDamage(PokemonEntity attacker, PokemonEntity defender, PokemonMove move) {
        int level = attacker.getPokemonLevel();
        int power = move.getPower();
        String category = move.getCategory();
        int attackStat = attacker.getPokeAttack();
        int defenseStat = defender.getPokeDefense();
        int sattackStat = attacker.getPokeSpecialAttack();
        int sdefenseStat = defender.getPokeSpecialDefense();
        int damage = 0;

        // Simplified calculation formula
        if(category.equals("Special"))
        {
            damage = (((2 * level / 5 + 2) * power * sattackStat / sdefenseStat) / 50) + 2;
            damage = damage * (85 + random.nextInt(16)) / 100;
            if (isStab(move, attacker)) {
                damage = Math.round((int) (damage * 1.5));
            }
            double typeEffectiveness = calculateTypeMatchup(move, defender);
            damage = (int) (damage * typeEffectiveness);
        }
        else damage = (((2 * level / 5 + 2) * power * attackStat / defenseStat) / 50) + 2;
        damage = damage * (85 + random.nextInt(16)) / 100;
        if (isStab(move, attacker)) {
            damage = Math.round((int) (damage * 1.5));
        }
        double typeEffectiveness = calculateTypeMatchup(move, defender);
        damage = (int) (damage * typeEffectiveness);
        return damage;
    }


    public static boolean isStab(PokemonMove move, PokemonEntity userPokemon) {
        String moveType = TypeMatchLookup.typeToString(move.getType());
        String userType1 = TypeMatchLookup.typeToString(userPokemon.getType1());
        String userType2 = TypeMatchLookup.typeToString(userPokemon.getType2());

        return moveType.equals(userType1) || moveType.equals(userType2);
    }

    private static double calculateTypeMatchup(PokemonMove move, PokemonEntity userPokemon) {
        String moveType = TypeMatchLookup.typeToString(move.getType());
        String userType1 = TypeMatchLookup.typeToString(userPokemon.getType1());
        String userType2 = TypeMatchLookup.typeToString(userPokemon.getType2());

        double effectiveness = TypeMatchLookup.getEffectiveness(moveType, userType1, userType2);

        return effectiveness;
    }
}
