package com.ladestitute.ancientlegends.battle.util;

import com.ladestitute.ancientlegends.battle.constructors.PokemonMove;
import com.ladestitute.ancientlegends.battle.registry.PokemonMoveInit;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.PokemonEntity;

public class MoveEffectHandler {

    public static void handleMoveEffects(PokemonMove move, PokemonEntity attacker, PokemonEntity defender) {
        if(move == PokemonMoveInit.STRUGGLE.get())
        {
            attacker.setPokeHP(attacker.getPokeHP() - Math.round(attacker.getPokeMaxHP()/4));
        }
    }
}
