package com.ladestitute.ancientlegends.battle.util;


import com.ladestitute.ancientlegends.entities.mobs.pokemon.PokemonEntity;

import java.util.ArrayList;
import java.util.List;

public class TurnOrderCalculator {
    public static List<PokemonEntity> calculateTurnOrder(PokemonEntity playerPokemon, PokemonEntity enemyPokemon) {
        List<PokemonEntity> turnOrder = new ArrayList<>();
        // Simple logic to alternate turns starting with the player
        boolean playerTurn = true;
        for (int i = 0; i < 6; i++) {
            if (playerTurn) {
                turnOrder.add(playerPokemon);
            } else {
                turnOrder.add(enemyPokemon);
            }
            playerTurn = !playerTurn;
        }
        return turnOrder;
    }
}
