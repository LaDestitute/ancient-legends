package com.ladestitute.ancientlegends.battle.registry;

import com.ladestitute.ancientlegends.AncientLegendsMain;
import com.ladestitute.ancientlegends.battle.api.MoveRegistryHelper;
import com.ladestitute.ancientlegends.battle.constructors.PokemonMove;
import net.minecraft.core.Registry;
import net.minecraft.resources.ResourceLocation;
import net.neoforged.neoforge.registries.DeferredHolder;
import net.neoforged.neoforge.registries.DeferredRegister;

import java.util.HashMap;
import java.util.Map;

    public class PokemonMoveInit {
        public static final DeferredRegister<PokemonMove> POKEMON_MOVES = DeferredRegister.create(new ResourceLocation(AncientLegendsMain.MOD_ID, "pokemon_moves"), AncientLegendsMain.MOD_ID);
        public static Registry<PokemonMove> POKEMON_MOVES_REGISTRY;

        static {
            System.out.println("Initializing Pokemon Moves...");
            POKEMON_MOVES_REGISTRY = POKEMON_MOVES.makeRegistry((builder) ->
                    builder
                            .maxId(Integer.MAX_VALUE - 1).onAdd((reg, id, key, val) ->
                                    AncientLegendsMain.LOGGER.info("Pokemon Move Added: " + key.location() + " ")
                            ).defaultKey(new ResourceLocation(AncientLegendsMain.MOD_ID, "null"))
            );
            System.out.println("Pokemon Moves initialized.");
        }

        public static final DeferredHolder<PokemonMove, PokemonMove> EMPTY = registerMove("empty", new PokemonMove("empty", 0, "Status", 0, 0, 0, 0, 0,
                0, 0, 0, 0, 5, 0,
                0, 0, 0,false, 0));
        public static final DeferredHolder<PokemonMove, PokemonMove> TACKLE = registerMove("tackle", new PokemonMove("tackle", 0, "Physical", 40, 50, 30, 30, 30,
                50, 100, 100, 0, 5, 0,
                0, 0, 0,false, 0));
        public static final DeferredHolder<PokemonMove, PokemonMove> STRUGGLE = registerMove("struggle", new PokemonMove("struggle", 0, "Physical", 50, 50, 50, 1, 1,
                100, 100, 100, 0, 0, 0,
                0, 0, 0,false, 2));
        public static final DeferredHolder<PokemonMove, PokemonMove> ROLLOUT = registerMove("rollout", new PokemonMove("rollout", 10, "Physical", 40, 50, 30, 20, 20,
                90, 100, 90, 0, 5, 0,
                0, 0, 0,false, 0));
        public static final DeferredHolder<PokemonMove, PokemonMove> QUICK_ATTACK = registerMove("quick_attack", new PokemonMove("quick_attack", 0, "Physical", 40, 50, 30, 20, 20, 100, 100, 100, -4, 2, -4, 0, 0, 3, false, 0));
        public static final DeferredHolder<PokemonMove, PokemonMove> BITE = registerMove("bite", new PokemonMove("bite", 15, "Physical", 60, 75, 45, 25, 25,
                100, 100, 100, 0, 5, 0,
                3, 3, 5,false, 0));
        public static final DeferredHolder<PokemonMove, PokemonMove> GUST = registerMove("gust", new PokemonMove("gust", 4, "Special", 40, 50, 30, 25, 25,
                100, 100, 100, 0, 5, 0,
                0, 0, 3,false, 0));
        public static final DeferredHolder<PokemonMove, PokemonMove> AERIAL_ACE = registerMove("aerial_ace", new PokemonMove("aerial_ace", 4, "Physical", 60, 75, 45, 20, 20,
                100, 100, 100, 0, 5, 0,
                0, 0, 3,false, 0));

        private static final Map<String, DeferredHolder<PokemonMove, PokemonMove>> moveMap = new HashMap<>();

        static {
            System.out.println("Populating move map...");
            moveMap.put("empty", EMPTY);
            moveMap.put("tackle", TACKLE);
            moveMap.put("struggle", STRUGGLE);
            moveMap.put("rollout", ROLLOUT);
            moveMap.put("quick_attack", QUICK_ATTACK);
            moveMap.put("bite", BITE);
            moveMap.put("gust", GUST);
            moveMap.put("aerial_ace", AERIAL_ACE);
            System.out.println("Move map populated.");
        }

        private static DeferredHolder<PokemonMove, PokemonMove> registerMove(String name, PokemonMove move) {
            System.out.println("Registering move: " + name);
            return POKEMON_MOVES.register(name, key -> move);
        }

        public static PokemonMove getMove(String moveName) {
            moveName = moveName.toLowerCase(); // Ensure case-insensitivity
            DeferredHolder<PokemonMove, PokemonMove> move = moveMap.get(moveName);
            if (move != null) {
                return move.get();
            }
            System.out.println("Move not found: " + moveName);
            return moveMap.get("empty").get(); // Return the "empty" move as a fallback
        }

        public static void initMoveAPI() {
            MoveRegistryHelper.initializeRegistry();
            AncientLegendsMain.LOGGER.info("Initializing Move API");
        }
}