package com.ladestitute.ancientlegends.battle.registry;

import com.ladestitute.ancientlegends.battle.constructors.PokemonMoveLevel;

import java.util.HashMap;
import java.util.Map;

public class PokemonLevelLearnListInit {
    private static final Map<String, Map<Integer, PokemonMoveLevel>> learnList = new HashMap<>();

    static {
        initializeDefaultLearnLists();
    }

    // Initialize default learn lists
    public static void initializeDefaultLearnLists() {
        Map<Integer, PokemonMoveLevel> bidoofLearnList = new HashMap<>();
        bidoofLearnList.put(1, new PokemonMoveLevel(1, 9, PokemonMoveInit.TACKLE.get()));
        bidoofLearnList.put(5, new PokemonMoveLevel(5, 10, PokemonMoveInit.QUICK_ATTACK.get()));
        bidoofLearnList.put(10, new PokemonMoveLevel(10, 19, PokemonMoveInit.ROLLOUT.get()));
        learnList.put("bidoof", bidoofLearnList);

        Map<Integer, PokemonMoveLevel> starlyLearnList = new HashMap<>();
        starlyLearnList.put(1, new PokemonMoveLevel(1, 9, PokemonMoveInit.GUST.get()));
        starlyLearnList.put(5, new PokemonMoveLevel(5, 14, PokemonMoveInit.QUICK_ATTACK.get()));
        starlyLearnList.put(9, new PokemonMoveLevel(9, 18, PokemonMoveInit.AERIAL_ACE.get()));
        learnList.put("starly", starlyLearnList);
    }

    // Get learn list for a species
    public static Map<Integer, PokemonMoveLevel> getLearnList(String species) {
        return learnList.get(species);
    }

    // Set learn list for a species
    public static void setLearnList(String species, Map<Integer, PokemonMoveLevel> newLearnList) {
        learnList.put(species, newLearnList);
    }

    // Get all learn lists
    public static Map<String, Map<Integer, PokemonMoveLevel>> getAllLearnLists() {
        return learnList;
    }
    }
