package com.ladestitute.ancientlegends.battle.gui.entity_display;

import com.ladestitute.ancientlegends.entities.mobs.pokemon.PokemonEntity;
import net.minecraft.util.Mth;

public class BarState {

    public final PokemonEntity entity;

    public float health;
    public float previousHealth;
    public float previousHealthDisplay;
    public float previousHealthDelay;
    public int lastDmg;
    public int lastDmgCumulative;
    public float lastHealth;
    public float lastDmgDelay;
    private float animationSpeed = 0;

    private static final float HEALTH_INDICATOR_DELAY = 10;

    public BarState(PokemonEntity entity) {
        this.entity = entity;
    }

    public void tick() {
        if(!(entity instanceof PokemonEntity))
        {
            return;
        }
        health = Math.min(entity.getPokeHP(), entity.getPokeMaxHP());
        incrementTimers();

        if (lastHealth < 0.1) {
            reset();

        } else if (lastHealth != health) {
            handleHealthChange();

        } else if (lastDmgDelay == 0.0F) {
            reset();
        }

        updateAnimations();
    }

    private void reset() {
        lastHealth = health;
        lastDmg = 0;
        lastDmgCumulative = 0;
    }

    private void incrementTimers() {
        if (this.lastDmgDelay > 0) {
            this.lastDmgDelay--;
        }
        if (this.previousHealthDelay > 0) {
            this.previousHealthDelay--;
        }
    }

    private void handleHealthChange() {
        lastDmg = Mth.ceil(lastHealth) - Mth.ceil(health);
        lastDmgCumulative += lastDmg;

        lastDmgDelay = HEALTH_INDICATOR_DELAY * 2;
        lastHealth = health;
    }

    private void updateAnimations() {
        if (previousHealthDelay > 0) {
            float diff = previousHealthDisplay - health;
            if (diff > 0) {
                animationSpeed = diff / 10f;
            }
        } else if (previousHealthDelay < 1 && previousHealthDisplay > health) {
            previousHealthDisplay -= animationSpeed;
        } else {
            previousHealthDisplay = health;
            previousHealth = health;
            previousHealthDelay = HEALTH_INDICATOR_DELAY;
        }
    }

}
