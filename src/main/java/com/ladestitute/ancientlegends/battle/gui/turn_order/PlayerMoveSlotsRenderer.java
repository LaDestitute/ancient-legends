package com.ladestitute.ancientlegends.battle.gui.turn_order;

import com.ladestitute.ancientlegends.AncientLegendsMain;
import com.ladestitute.ancientlegends.battle.constructors.PokemonMove;
import com.ladestitute.ancientlegends.battle.core.BattleManager;
import com.ladestitute.ancientlegends.battle.util.TypeMatchLookup;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.PokemonEntity;
import com.ladestitute.ancientlegends.network.MoveSelectionPacket;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.client.resources.language.I18n;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.Style;
import net.minecraft.resources.ResourceLocation;
import net.neoforged.neoforge.client.gui.overlay.ExtendedGui;
import net.neoforged.neoforge.client.gui.overlay.IGuiOverlay;
import net.neoforged.neoforge.network.PacketDistributor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PlayerMoveSlotsRenderer implements IGuiOverlay {
    private static final PlayerMoveSlotsRenderer hud = new PlayerMoveSlotsRenderer();
    private static final ResourceLocation MOVE_BAR = new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/move_bar.png");
    private static final ResourceLocation MOVE_BAR_SELECTED = new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/move_bar_selected.png");
    private static final ResourceLocation PP_OVERLAY = new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/move_pp_icon.png");
    private static final ResourceLocation ARROW_ICON = new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/move_selection_arrow.png");
    private static final ResourceLocation WHITE_OVERLAY_ICON = new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/selected_move_type_outline.png");
    private static final ResourceLocation NORMAL_ICON = new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/types/normal_type_icon.png");

    private static final Map<String, ResourceLocation> TYPE_ICONS = new HashMap<>();
    private static final Map<Double, ResourceLocation> EFFECTIVENESS_ICONS = new HashMap<>();

    // Single pair of X and Y offsets for all assets
    private static final int X_OFFSET = -210;
    private static final int Y_OFFSET = 25;

    static {
        // Initialize type icons
        TYPE_ICONS.put("normal", new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/types/normal_type_icon.png"));
        TYPE_ICONS.put("fire", new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/types/fire_type_icon.png"));
        TYPE_ICONS.put("flying", new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/types/flying_type_icon.png"));
        TYPE_ICONS.put("grass", new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/types/grass_type_icon.png"));
        TYPE_ICONS.put("fighting", new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/types/fighting_type_icon.png"));
        TYPE_ICONS.put("water", new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/types/water_type_icon.png"));
        TYPE_ICONS.put("poison", new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/types/poison_type_icon.png"));
        TYPE_ICONS.put("electric", new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/types/electric_type_icon.png"));
        TYPE_ICONS.put("ground", new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/types/ground_type_icon.png"));
        TYPE_ICONS.put("psychic", new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/types/psychic_type_icon.png"));
        TYPE_ICONS.put("bug", new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/types/bug_type_icon.png"));
        TYPE_ICONS.put("dragon", new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/types/dragon_type_icon.png"));
        TYPE_ICONS.put("rock", new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/types/rock_type_icon.png"));
        TYPE_ICONS.put("ice", new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/types/ice_type_icon.png"));
        TYPE_ICONS.put("ghost", new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/types/ghost_type_icon.png"));
        TYPE_ICONS.put("dark", new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/types/dark_type_icon.png"));
        TYPE_ICONS.put("steel", new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/types/steel_type_icon.png"));
        TYPE_ICONS.put("fairy", new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/types/fairy_type_icon.png"));
        // Add more type icons here...

        // Initialize effectiveness icons
        EFFECTIVENESS_ICONS.put(0.0, new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/no_effectiveness_dot.png"));
        EFFECTIVENESS_ICONS.put(0.5, new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/resisted_effectiveness_dot.png"));
        EFFECTIVENESS_ICONS.put(1.0, new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/normal_effectiveness_dot.png"));
        EFFECTIVENESS_ICONS.put(2.0, new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/super_effectiveness_dot.png"));
        EFFECTIVENESS_ICONS.put(4.0, new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/super_effectiveness_dot.png"));
    }

    private final Minecraft minecraft;
    private static int selectedMoveIndex = -1;
    public static boolean overlayActive = true;

    public PlayerMoveSlotsRenderer() {
        this.minecraft = Minecraft.getInstance();
    }

    public static boolean isOverlayActive()
    {
        return overlayActive;
    }

    public static PlayerMoveSlotsRenderer getInstance() {
        return hud;
    }

    public static void selectMove(int index) {
        selectedMoveIndex = index;
        // Send packet to execute the move if it's a valid selection
        if (selectedMoveIndex != -1 && BattleManager.instance != null) {
            MoveSelectionPacket message = new MoveSelectionPacket(index);
            PacketDistributor.SERVER.noArg().send(message);
        }
    }

    public static boolean isMoveSlotHovered(int mouseX, int mouseY) {
        // Logic to check if the mouse is hovering over a move slot
        int x = Minecraft.getInstance().getWindow().getGuiScaledWidth() - 200;
        int y = Minecraft.getInstance().getWindow().getGuiScaledHeight() - (BattleManager.instance.getPlayerPokemon().getMoveSlots().size() * 40) - 10;

        for (int i = 0; i < BattleManager.instance.getPlayerPokemon().getMoveSlots().size(); i++) {
            if (mouseX >= x && mouseX <= x + 192 && mouseY >= y && mouseY <= y + 32) {
                return true;
            }
            y += 40;
        }
        return false;
    }

    public static int getHoveredMoveSlot(int mouseX, int mouseY) {
        // Logic to get the index of the hovered move slot
        int x = Minecraft.getInstance().getWindow().getGuiScaledWidth() - 200;
        int y = Minecraft.getInstance().getWindow().getGuiScaledHeight() - (BattleManager.instance.getPlayerPokemon().getMoveSlots().size() * 40) - 10;

        for (int i = 0; i < BattleManager.instance.getPlayerPokemon().getMoveSlots().size(); i++) {
            if (mouseX >= x && mouseX <= x + 192 && mouseY >= y && mouseY <= y + 32) {
                return i;
            }
            y += 40;
        }
        return -1;
    }

    public static void toggleOverlay() {
        overlayActive = !overlayActive;
        if (overlayActive) {
            Minecraft.getInstance().mouseHandler.grabMouse();
        } else {
            Minecraft.getInstance().mouseHandler.releaseMouse();
        }
    }

    @Override
    public void render(ExtendedGui gui, GuiGraphics graphics, float partialTick, int screenWidth, int screenHeight) {
        if (!overlayActive || BattleManager.instance == null || BattleManager.instance.battleOver) {
            return;
        }

        Minecraft mc = Minecraft.getInstance();
        PokemonEntity playerPokemon = BattleManager.instance.playerPokemonList.get(0);
        if (playerPokemon == null) return;

        List<PokemonMove> moves = playerPokemon.getMoveSlots();
        int x = screenWidth - 200 + X_OFFSET; // Bottom right corner X position
        int y = screenHeight - (moves.size() * 40) - 10 + Y_OFFSET; // Bottom right corner Y position

        for (int i = 0; i < moves.size(); i++) {
            PokemonMove move = moves.get(i);
            if (move == null || move.getName().equalsIgnoreCase("empty")) continue;

            boolean isSelected = (i == selectedMoveIndex);

            ResourceLocation moveBarTexture = isSelected ? MOVE_BAR_SELECTED : MOVE_BAR;
            renderMoveBar(graphics, x, y, moveBarTexture, move, isSelected);

            y += 40; // Move to the next slot position
        }
    }

    private void renderMoveBar(GuiGraphics graphics, int x, int y, ResourceLocation moveBarTexture, PokemonMove move, boolean isSelected) {
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderTexture(0, moveBarTexture);
        RenderSystem.enableBlend();

        graphics.blit(moveBarTexture, x, y, 0, 0, 246, 18, 246, 18);

        // Render PP overlay
        RenderSystem.setShaderTexture(0, PP_OVERLAY);
        graphics.blit(PP_OVERLAY, x + 120, y + 2, 0, 0, 50, 14, 50, 14);
        Component ppComponent = Component.literal(move.getPP() + " / " + move.getMaxPP()).withStyle(Style.EMPTY.withFont(new ResourceLocation("ancientlegends", "eurotypo")));
        graphics.drawString(minecraft.font, ppComponent, x + 127, y + 7, 0xFFFFFF);

        // Render move type icon
        ResourceLocation typeIcon = TYPE_ICONS.get(TypeMatchLookup.typeToString(move.getType()));
        if (typeIcon != null) {
            RenderSystem.enableBlend();
            RenderSystem.defaultBlendFunc();
            RenderSystem.setShaderTexture(0, typeIcon);
            graphics.blit(typeIcon, x + -10, y + -5, 0, 0, 24, 24, 24, 24);
        }

        // Render effectiveness icon
        double effectiveness = BattleManager.calculateTypeMatchup(move, BattleManager.instance.getEnemyPokemon());
        ResourceLocation effectivenessIcon = EFFECTIVENESS_ICONS.get(effectiveness);
        if (effectivenessIcon != null) {
            RenderSystem.setShaderTexture(0, effectivenessIcon);
            RenderSystem.enableBlend();
            RenderSystem.defaultBlendFunc();
            graphics.blit(effectivenessIcon, x + 219, y + 5, 0, 0, 8, 8, 8, 8);
        }

        // Render move name
        String localizedName = I18n.get("move.ancientlegends." + move.getName());

        Component nameComponent = Component.literal(localizedName).withStyle(Style.EMPTY.withFont(new ResourceLocation("ancientlegends", "cstberlin")));
        graphics.drawString(minecraft.font, nameComponent, x + 20, y + 6, 0xFFFFFF);

        // Render arrow icon and white overlay icon if selected
        if (isSelected) {
            RenderSystem.setShaderTexture(0, ARROW_ICON);
            graphics.blit(ARROW_ICON, x - 20, y + -5, 0, 0, 12, 21, 12, 21);

            RenderSystem.setShaderTexture(0, WHITE_OVERLAY_ICON);
            graphics.blit(WHITE_OVERLAY_ICON, x + -10, y + -5, 0, 0, 26, 26, 26, 26);
        }

        RenderSystem.disableBlend();
    }
}
