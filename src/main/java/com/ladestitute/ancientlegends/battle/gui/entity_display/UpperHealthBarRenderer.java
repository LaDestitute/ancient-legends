package com.ladestitute.ancientlegends.battle.gui.entity_display;

import com.ladestitute.ancientlegends.AncientLegendsMain;
import com.ladestitute.ancientlegends.battle.gui.entity_display.BarState;
import com.ladestitute.ancientlegends.battle.gui.entity_display.BarStates;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.PokemonEntity;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.*;
import com.mojang.math.Axis;
import net.minecraft.client.Camera;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.phys.Vec3;
import org.joml.Matrix4f;
import org.lwjgl.opengl.GL11;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public class UpperHealthBarRenderer {

    private static final ResourceLocation GUI_BARS_TEXTURES = new ResourceLocation(AncientLegendsMain.MOD_ID + ":textures/gui/battle/bars.png");
    private static final ResourceLocation ARROW_TEXTURE = new ResourceLocation(AncientLegendsMain.MOD_ID + ":textures/gui/battle/arrow.png");
    private static final ResourceLocation GENDER_MALE_TEXTURE = new ResourceLocation(AncientLegendsMain.MOD_ID + ":textures/gui/battle/gender_male.png");
    private static final ResourceLocation GENDER_FEMALE_TEXTURE = new ResourceLocation(AncientLegendsMain.MOD_ID + ":textures/gui/battle/gender_female.png");
    private static final ResourceLocation LEVEL_PLATE_TEXTURE = new ResourceLocation(AncientLegendsMain.MOD_ID + ":textures/gui/battle/level_plate.png");

    private static final int DARK_GRAY = 0x808080;
    private static final float FULL_SIZE = 40;

    private static final List<LivingEntity> renderedEntities = new ArrayList<>();

    public static void prepareRenderInWorld(PokemonEntity entity) {
        Minecraft client = Minecraft.getInstance();
        if (!(entity instanceof PokemonEntity)) {
            return;
        }

        if (client.getCameraEntity() == null) return;

        if (entity.distanceTo(client.getCameraEntity()) > 60f) {
            return;
        }

        BarStates.getState(entity);

        renderedEntities.add(entity);
    }

    public static void renderInWorld(float partialTick, GuiGraphics graphics, Camera camera) {
        Minecraft client = Minecraft.getInstance();

        if (camera == null) {
            camera = client.getEntityRenderDispatcher().camera;
        }

        if (camera == null) {
            renderedEntities.clear();
            return;
        }

        if (renderedEntities.isEmpty()) {
            return;
        }

        RenderSystem.enableDepthTest();
        RenderSystem.enableBlend();
        RenderSystem.blendFuncSeparate(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA, GL11.GL_ONE, GL11.GL_ZERO);

        for (LivingEntity entity : renderedEntities) {
            if(!(entity instanceof PokemonEntity))
            {
                return;
            }
            float scaleToGui = 0.025f;
            boolean sneaking = entity.isCrouching();
            float height = entity.getBbHeight() + 0.3F - (sneaking ? 0.25F : 0.0F);

            double x = Mth.lerp(partialTick, entity.xo, entity.getX());
            double y = Mth.lerp(partialTick, entity.yo, entity.getY());
            double z = Mth.lerp(partialTick, entity.zo, entity.getZ());

            Vec3 camPos = camera.getPosition();
            double camX = camPos.x();
            double camY = camPos.y();
            double camZ = camPos.z();

            final PoseStack matrix = graphics.pose();

            matrix.pushPose();
            matrix.translate(x - camX, (y + height) - camY, z - camZ);
            matrix.mulPose(Axis.YP.rotationDegrees(-camera.getYRot()));
            matrix.mulPose(Axis.XP.rotationDegrees(camera.getXRot()));
            matrix.scale(-scaleToGui, -scaleToGui, scaleToGui);

            render(graphics, (PokemonEntity) entity, 0, 0, FULL_SIZE, true);

            matrix.popPose();
        }

        RenderSystem.disableBlend();

        renderedEntities.clear();
    }

    public static void render(GuiGraphics graphics, PokemonEntity entity, double x, double y, float width, boolean inWorld) {
        BarState state = BarStates.getState(entity);

        float percent = Math.min(1, Math.min(state.health, entity.getPokeMaxHP()) / entity.getPokeMaxHP());
        float percent2 = Math.min(state.previousHealthDisplay, entity.getPokeMaxHP()) / entity.getPokeMaxHP();
        int zOffset = 0;
        final PoseStack matrix = graphics.pose();

        Matrix4f m4f = matrix.last().pose();

        RenderSystem.setShader(GameRenderer::getPositionColorShader);
        RenderSystem.setShaderTexture(0, GUI_BARS_TEXTURES);

        drawBar(m4f, x, y, width, 1, DARK_GRAY, zOffset++, inWorld);
        drawBar(m4f, x, y, width, percent2, 0x413A36, zOffset++, inWorld);
        drawBar(m4f, x, y, width, percent, getColorForHealth(percent), zOffset, inWorld);

        RenderSystem.disableBlend();
        RenderSystem.defaultBlendFunc();
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
    }

    private static void drawBar(Matrix4f matrix4f, double x, double y, float width, float percent, int color, int zOffset, boolean inWorld) {
        float c = 0.00390625F;
        int u = 0;
        int v = 6 * 5 * 2 + 5;
        int uw = Mth.ceil(92 * percent);
        int vh = 5;

        double size = percent * width;
        double h = inWorld ? 4 : 6;

        float r = (color >> 16 & 255) / 255.0F;
        float g = (color >> 8 & 255) / 255.0F;
        float b = (color & 255) / 255.0F;

        RenderSystem.setShaderColor(r, g, b, 1);
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderTexture(0, GUI_BARS_TEXTURES);
        RenderSystem.enableBlend();

        float half = width / 2;

        float zOffsetAmount = inWorld ? -0.1F : 0.1F;

        Tesselator tessellator = Tesselator.getInstance();
        BufferBuilder buffer = tessellator.getBuilder();
        buffer.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_TEX);

        buffer.vertex(matrix4f, (float) (-half + x), (float) y, zOffset * zOffsetAmount)
                .uv(u * c, v * c).endVertex();
        buffer.vertex(matrix4f, (float) (-half + x), (float) (h + y), zOffset * zOffsetAmount)
                .uv(u * c, (v + vh) * c).endVertex();
        buffer.vertex(matrix4f, (float) (-half + size + x), (float) (h + y), zOffset * zOffsetAmount)
                .uv((u + uw) * c, (v + vh) * c).endVertex();
        buffer.vertex(matrix4f, (float) (-half + size + x), (float) y, zOffset * zOffsetAmount)
                .uv(((u + uw) * c), v * c).endVertex();
        tessellator.end();

        RenderSystem.disableBlend();
        RenderSystem.defaultBlendFunc();
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
    }

    private static void drawTexture(GuiGraphics graphics, ResourceLocation texture, double x, double y, double width, double height, int u, int v, int uw, int vh) {
        graphics.blit(texture, (int) x, (int) y, u, v, (int) width, (int) height, uw, vh);
    }

    private static void drawString(GuiGraphics graphics, Component text, int x, int y, int color) {
        graphics.drawString(Minecraft.getInstance().font, text, x, y, color);
    }

    private static int getColorForHealth(float healthPercentage) {
        if (healthPercentage > 0.5f) {
            return 0x36D08C; // Green
        } else if (healthPercentage > 0.2f) {
            return 0xECE75B; // Yellow
        } else {
            return 0xFE5558; // Red
        }
    }
}
