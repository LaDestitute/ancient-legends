package com.ladestitute.ancientlegends.battle.gui.entity_display;

import com.ladestitute.ancientlegends.battle.gui.GuiHelper;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.PokemonEntity;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.Style;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.LivingEntity;

public class BarDisplay {

    private final Minecraft mc;

    public BarDisplay(Minecraft mc) {
        this.mc = mc;
    }

    public void draw(GuiGraphics graphics, LivingEntity entity) {
        PokemonEntity pokentity = (PokemonEntity) entity;
       if (!(entity instanceof PokemonEntity)) {
            return;
        }
        int xOffset = 0;

        graphics.setColor(1.0F, 1.0F, 1.0F, 1.0F);
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.enableBlend();

        UpperHealthBarRenderer.render(graphics, (PokemonEntity) entity, 63, 14, 130, true);
        int healthMax = Mth.ceil(pokentity.getPokeMaxHP());
        int healthCur = Math.min(Mth.ceil(pokentity.getPokeHP()), healthMax);
        String healthText = healthCur + "/" + healthMax;

        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        xOffset += 50;

        Component healthComponent = Component.literal(healthText).withStyle(Style.EMPTY.withFont(new ResourceLocation("ancientlegends", "substance_two")));
        GuiHelper.drawText(graphics, healthComponent, xOffset, 2, 0xFFFFFF, true);
    }
}
