package com.ladestitute.ancientlegends.battle.gui.turn_order;

import com.ladestitute.ancientlegends.AncientLegendsMain;
import com.ladestitute.ancientlegends.battle.core.BattleManager;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.PokemonEntity;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.client.renderer.entity.EntityRenderDispatcher;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.Style;
import net.minecraft.resources.ResourceLocation;
import net.neoforged.neoforge.client.gui.overlay.ExtendedGui;
import net.neoforged.neoforge.client.gui.overlay.IGuiOverlay;

import java.util.List;

public class TurnOrderRenderer implements IGuiOverlay {
    private static final TurnOrderRenderer hud = new TurnOrderRenderer();
    private static final ResourceLocation USER_BACKGROUND_TEXTURE = new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/turn_order_user.png");
    private static final ResourceLocation ENEMY_BACKGROUND_TEXTURE = new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/turn_order_enemy.png");
    private static final ResourceLocation OVERSIZED_USER_BACKGROUND_TEXTURE = new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/turn_order_user_oversized.png");
    private static final ResourceLocation OVERSIZED_ENEMY_BACKGROUND_TEXTURE = new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/turn_order_enemy_oversized.png");
    private static final ResourceLocation ACTION_ORDER_ICON = new ResourceLocation(AncientLegendsMain.MOD_ID, "textures/gui/battle/action_order_icon.png");
    private final Minecraft minecraft;
    private final int width;
    private final int height;
    private static boolean showTurnOrder = true;

    public static void toggleTurnOrderDisplay() {
        showTurnOrder = !showTurnOrder;
    }

    public static TurnOrderRenderer getInstance() {
        return hud;
    }

    public TurnOrderRenderer() {
        this.width = Minecraft.getInstance().getWindow().getScreenWidth();
        this.height = Minecraft.getInstance().getWindow().getScreenHeight();
        this.minecraft = Minecraft.getInstance();
    }

    private static boolean isPlayerPokemon(PokemonEntity pokemon) {
        return BattleManager.instance.playerPokemonList.contains(pokemon);
    }

    @Override
    public void render(ExtendedGui gui, GuiGraphics graphics, float partialTick, int screenWidth, int screenHeight) {
        if (!showTurnOrder || BattleManager.instance == null || BattleManager.instance.battleOver) {
            return;
        }

        Minecraft mc = Minecraft.getInstance();
        List<PokemonEntity> turnOrder = BattleManager.instance.getTurnOrder(BattleManager.instance.playerPokemonList, BattleManager.instance.enemyPokemonList);
        int x = 291; // X position of the turn order display
        int y = 50; // Y position of the turn order display
        int width = 75;
        int height = 15;
        int oversizedHeight = 23;
        int oversizedWidth = 113;
        int yOffset = -15; // Initial gap between the action order icon and the first turn bar

        // Render Action Order text and icon
        renderActionOrder(graphics, x, y);

        yOffset += 25; // Adjust for the Action Order header height and initial gap

        // Calculate the current turn index (1-6)
        int currentTurnIndex = (BattleManager.instance.getCurrentTurn() - 1) % 6;

        // Render the current turn bar
        for (int i = 0; i < 6; i++) {
            if (i >= turnOrder.size()) {
                break;
            }
            PokemonEntity pokemon = turnOrder.get(i);
            boolean isPlayerPokemon = isPlayerPokemon(pokemon);
            boolean isCurrentTurn = (i == currentTurnIndex);

            ResourceLocation backgroundTexture = isPlayerPokemon ? USER_BACKGROUND_TEXTURE : ENEMY_BACKGROUND_TEXTURE;
            ResourceLocation oversizedBackgroundTexture = isPlayerPokemon ? OVERSIZED_USER_BACKGROUND_TEXTURE : OVERSIZED_ENEMY_BACKGROUND_TEXTURE;

            // Draw background
            RenderSystem.setShader(GameRenderer::getPositionTexShader);
            RenderSystem.setShaderTexture(0, backgroundTexture);
            RenderSystem.enableBlend();

            if (isCurrentTurn) {
                RenderSystem.setShaderTexture(0, oversizedBackgroundTexture);
                graphics.blit(oversizedBackgroundTexture, x + 45, y + yOffset + -3, oversizedWidth, oversizedHeight, 0, 0, oversizedWidth, oversizedHeight, oversizedWidth, oversizedHeight);
            } else {
                graphics.blit(backgroundTexture, x + 60, y + yOffset, width, height, 0, 0, width, height, width, height);
            }

            RenderSystem.disableBlend();

            // Draw Pokémon name
            Component nameComponent = Component.literal(pokemon.getName().getString())
                    .withStyle(Style.EMPTY.withFont(new ResourceLocation("ancientlegends", "cinio")));
            graphics.drawString(mc.font, nameComponent, x + 70, y + yOffset + 5, 0xFFFFFF, false);

            // Draw Pokémon sprite
            drawPokemonSprite(graphics, pokemon, x + width + 30, y + yOffset + 5);

            yOffset += height; // No additional gap between bars
        }
    }

    private static void drawPokemonSprite(GuiGraphics graphics, PokemonEntity pokemon, int x, int y) {
        PoseStack poseStack = graphics.pose();
        poseStack.pushPose();
        poseStack.translate(x, y, 50);
        poseStack.scale(0.25f, 0.25f, 0.25f);

        EntityRenderDispatcher dispatcher = Minecraft.getInstance().getEntityRenderDispatcher();
        dispatcher.render(pokemon, 0, 0, 0, 0, 1.0f, poseStack, graphics.bufferSource(), 15728880);

        poseStack.popPose();
    }

    private static void renderActionOrder(GuiGraphics graphics, double x, double y) {
        // Render the icon
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderTexture(0, ACTION_ORDER_ICON);
        RenderSystem.enableBlend();
        graphics.blit(ACTION_ORDER_ICON, (int) x - 5, (int) y - 20, 0, 0, 147, 20, 147, 20);
        RenderSystem.disableBlend();
    }
}
