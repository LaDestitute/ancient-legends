package com.ladestitute.ancientlegends.battle.gui.entity_display;

import com.ladestitute.ancientlegends.battle.core.BattleManager;
import com.ladestitute.ancientlegends.battle.gui.turn_order.BattleGuiRenderer;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.PokemonEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.LivingEntity;
import net.neoforged.neoforge.client.gui.overlay.ExtendedGui;

import java.util.List;

public class Hud extends Screen {
    private final EntityDisplay entityDisplay = new EntityDisplay();
    private LivingEntity entity;
    private final BarDisplay barDisplay;
    private int age;
    private List<PokemonEntity> enemyPokemons;
    private List<PokemonEntity> playerPokemons;

    public enum AnchorPoint {
        TOP_LEFT, TOP_CENTER, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_CENTER, BOTTOM_RIGHT
    }

    public Hud() {
        super(Component.literal(""));
        this.minecraft = Minecraft.getInstance();
        barDisplay = new BarDisplay(Minecraft.getInstance());
    }

    public void draw(ExtendedGui gui, GuiGraphics poseStack, float partialTick, int width, int height) {
        if (minecraft == null || minecraft.getDebugOverlay().showDebugScreen()) return;
        float x = determineX();
        float y = determineY();
        draw(poseStack, x, y, 1);
    }

    private float determineX() {
        float x = 4f;
        AnchorPoint anchor = AnchorPoint.TOP_CENTER;
        float wScreen = minecraft.getWindow().getGuiScaledHeight();

        return switch (anchor) {
            case BOTTOM_CENTER, TOP_CENTER -> (wScreen / 2) + x;
            case BOTTOM_RIGHT, TOP_RIGHT -> (wScreen) + x;
            default -> x;
        };
    }

    private float determineY() {
        float y = 4f;
        AnchorPoint anchor = AnchorPoint.TOP_CENTER;
        if (minecraft == null) return y;
        float hScreen = minecraft.getWindow().getGuiScaledHeight();

        return switch (anchor) {
            case BOTTOM_CENTER, BOTTOM_LEFT, BOTTOM_RIGHT -> y + hScreen;
            default -> y;
        };
    }

    public void tick() {
        age++;
    }

    public void setEntity(LivingEntity entity) {
        if (entity != null) {
            age = 0;
        }

        if (entity == null && age > 20) {
            setEntityWork(null);
        }

        if (entity != null && entity != this.entity) {
            setEntityWork(entity);
        }
    }

    private void setEntityWork(LivingEntity entity) {
        this.entity = entity;
        entityDisplay.setEntity(entity);
    }

    public LivingEntity getEntity() {
        return entity;
    }

    private void draw(GuiGraphics graphics
            , float x, float y, float scale) {
        if (entity == null) {
            return;
        }

        final PoseStack matrix = graphics.pose();

        matrix.pushPose();
        matrix.scale(scale, scale, scale);
        matrix.translate(x - 10, y - 10, 0);

        matrix.translate(10, 10, 0);

        entityDisplay.draw(graphics.pose(), scale);

        matrix.translate(44, 0, 0);
        if (entity instanceof PokemonEntity) {
            barDisplay.draw(graphics, entity);
        }

        // Render the turn order UI
        if (entity instanceof PokemonEntity) {
            if(BattleManager.instance != null) {
                BattleGuiRenderer.renderBattleGui(graphics, playerPokemons, enemyPokemons);
            }
        }

        matrix.popPose();
    }
}

