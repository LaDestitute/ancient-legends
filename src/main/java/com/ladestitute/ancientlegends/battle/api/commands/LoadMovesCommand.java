package com.ladestitute.ancientlegends.battle.api.commands;

import com.ladestitute.ancientlegends.battle.api.MoveRegistryHelper;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;
import net.minecraft.network.chat.Component;

public class LoadMovesCommand {
    public static void register(CommandDispatcher<CommandSourceStack> dispatcher) {
        dispatcher.register(Commands.literal("loadmoves")
                .executes(LoadMovesCommand::execute));
    }

    private static int execute(CommandContext<CommandSourceStack> context) {
        MoveRegistryHelper moveregistryhelper = new MoveRegistryHelper();
        moveregistryhelper.loadMovesFromJson();
        context.getSource().sendSuccess(() -> Component.literal("Moves loaded from JSON"), true);
        return 1;
    }
}