package com.ladestitute.ancientlegends.battle.api;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ladestitute.ancientlegends.AncientLegendsMain;
import com.ladestitute.ancientlegends.battle.constructors.PokemonMove;
import com.ladestitute.ancientlegends.battle.constructors.PokemonMoveLevel;
import com.ladestitute.ancientlegends.battle.registry.PokemonLevelLearnListInit;
import com.ladestitute.ancientlegends.battle.registry.PokemonMoveInit;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class LearnListRegistryHelper {
    private static final Gson gson = new Gson();
    private static final String DIRECTORY_PATH = "src/main/resources/data/ancientlegends/learn_lists/";

    // Load learn lists from JSON files
    public static void loadLearnLists() {
        try {
            Files.list(Paths.get(DIRECTORY_PATH))
                    .filter(Files::isRegularFile)
                    .forEach(file -> {
                        String fileName = file.getFileName().toString();
                        String species = fileName.substring(0, fileName.indexOf('.'));
                        try (FileReader reader = new FileReader(file.toFile())) {
                            Type type = new TypeToken<Map<Integer, PokemonMoveLevel>>() {}.getType();
                            Map<Integer, PokemonMoveLevel> learnList = gson.fromJson(reader, type);
                            setLearnList(species, learnList);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Save learn lists to JSON files
    public static void saveLearnLists() {
        PokemonLevelLearnListInit.getAllLearnLists().forEach((species, learnList) -> {
            try (FileWriter writer = new FileWriter(new File(DIRECTORY_PATH + species + ".json"))) {
                gson.toJson(learnList, writer);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    // Set learn list for a species
    public static void setLearnList(String species, Map<Integer, PokemonMoveLevel> learnList) {
        PokemonLevelLearnListInit.setLearnList(species, learnList);
    }

    // Get learn list for a species
    public static Map<Integer, PokemonMoveLevel> getLearnList(String species) {
        return PokemonLevelLearnListInit.getLearnList(species);
    }

    // Get all learn lists
    public static Map<String, Map<Integer, PokemonMoveLevel>> getAllLearnLists() {
        return PokemonLevelLearnListInit.getAllLearnLists();
    }

    // Initialize default learn lists
    public static void initializeRegistry() {
        PokemonLevelLearnListInit.initializeDefaultLearnLists();
    }

    // Set level for a move
    public static void setLevel(String species, int oldLevel, int newLevel) {
        if (newLevel == 0) {
            AncientLegendsMain.LOGGER.warn("Warning: Setting a level to 0.");
            return;
        }

        Map<Integer, PokemonMoveLevel> learnList = getLearnList(species);
        if (learnList != null) {
            PokemonMoveLevel move = learnList.remove(oldLevel);
            if (move != null) {
                learnList.put(newLevel, move);
            } else {
                AncientLegendsMain.LOGGER.warn("Warning: No move found at level " + oldLevel + " for species " + species + ".");
            }
        } else {
            AncientLegendsMain.LOGGER.warn("Warning: Learn list for species " + species + " does not exist.");
        }
    }

    // Set mastery level for a move
    public static void setMasteryLevel(String species, int level, int newMasteryLevel) {
        if (newMasteryLevel == 0) {
            AncientLegendsMain.LOGGER.warn("Warning: Setting a mastery level to 0.");
            return;
        }

        Map<Integer, PokemonMoveLevel> learnList = getLearnList(species);
        if (learnList != null) {
            PokemonMoveLevel move = learnList.get(level);
            if (move != null) {
                move.setMasteryLevel(newMasteryLevel);
            } else {
                AncientLegendsMain.LOGGER.warn("Warning: No move found at level " + level + " for species " + species + ".");
            }
        } else {
            AncientLegendsMain.LOGGER.warn("Warning: Learn list for species " + species + " does not exist.");
        }
    }

    // Set move for a level
    public static void setMove(String species, int level, PokemonMoveLevel newMove) {
        Map<Integer, PokemonMoveLevel> learnList = getLearnList(species);
        if (learnList != null) {
            learnList.put(level, newMove);
        } else {
            AncientLegendsMain.LOGGER.warn("Warning: Learn list for species " + species + " does not exist.");
        }
    }

    // Get move for a level
    public static PokemonMoveLevel getMoveForLevel(String species, int level) {
        Map<Integer, PokemonMoveLevel> learnList = getLearnList(species);
        return (learnList != null) ? learnList.get(level) : null;
    }

    // Remove a move from the learn list
    public static void removeMove(String species, int level) {
        Map<Integer, PokemonMoveLevel> learnList = getLearnList(species);
        if (learnList != null) {
            learnList.remove(level);
        } else {
            AncientLegendsMain.LOGGER.warn("Warning: Learn list for species " + species + " does not exist.");
        }
    }

    // Add a new move to the learn list
    public static void addNewMove(String species, int level, PokemonMoveLevel newMove) {
        Map<Integer, PokemonMoveLevel> learnList = getLearnList(species);
        if (learnList == null) {
            learnList = new HashMap<>();
            setLearnList(species, learnList);
        }
        learnList.put(level, newMove);
    }

    public static void addNewMove(String species, int level, String moveName) {
        // Convert moveName to lower case to ensure case-insensitive matching
        PokemonMove move = PokemonMoveInit.getMove(moveName.toLowerCase());
        PokemonMoveLevel newMove = new PokemonMoveLevel(level, 0, move); // Adjust parameters as necessary
        addNewMove(species, level, newMove);
    }

    private static PokemonMoveLevel getMoveByName(String moveName) {
        // Implement the logic to retrieve the move by its name.
        // This is just a placeholder for the actual implementation.
        return new PokemonMoveLevel(0, 0, null); // Replace with actual logic
    }

    // Add a new learn list for a new Pokémon species
    public static void addNewLearnList(String species, Map<Integer, PokemonMoveLevel> newLearnList) {
        setLearnList(species, newLearnList);
    }
}
