package com.ladestitute.ancientlegends.battle.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.ladestitute.ancientlegends.AncientLegendsMain;
import com.ladestitute.ancientlegends.battle.constructors.PokemonMove;
import com.ladestitute.ancientlegends.battle.registry.PokemonLevelLearnListInit;
import com.ladestitute.ancientlegends.battle.registry.PokemonMoveInit;
import net.minecraft.core.Registry;
import net.neoforged.neoforge.registries.DeferredHolder;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

    public class MoveRegistryHelper {
        private final String JSON_DIR_PATH = "src/main/resources/data/ancientlegends/moves/";
        private final Gson GSON = new GsonBuilder()
                .setPrettyPrinting()
                .create();
        private static Map<String, PokemonMove> moveRegistry = new HashMap<>();

        public static void addMove(PokemonMove move) {
            moveRegistry.put(move.getName(), move);
        }

        public void modifyMove(String moveName, PokemonMove updatedMove) {
            if (moveRegistry.containsKey(moveName)) {
                moveRegistry.put(moveName, updatedMove);
            } else {
                AncientLegendsMain.LOGGER.error("Move not found in the registry: " + moveName);
            }
        }

        public PokemonMove getMove(String moveName) {
            return moveRegistry.get(moveName);
        }

        public Collection<PokemonMove> getAllMoves() {
            return moveRegistry.values();
        }

        private String getTypeFileName(int typeInt) {
            switch (typeInt) {
                case 0: return "normal_moves.json";
                case 1: return "fire_moves.json";
                case 2: return "fighting_moves.json";
                case 3: return "water_moves.json";
                case 4: return "flying_moves.json";
                case 5: return "grass_moves.json";
                case 6: return "poison_moves.json";
                case 7: return "electric_moves.json";
                case 8: return "ground_moves.json";
                case 9: return "psychic_moves.json";
                case 10: return "rock_moves.json";
                case 11: return "ice_moves.json";
                case 12: return "bug_moves.json";
                case 13: return "dragon_moves.json";
                case 14: return "ghost_moves.json";
                case 15: return "dark_moves.json";
                case 16: return "steel_moves.json";
                case 17: return "fairy_moves.json";
                default: return "unknown_moves.json";
            }
        }

        public void saveMovesToJson() {
            try {
                Map<Integer, Map<String, PokemonMove>> movesByType = new HashMap<>();
                for (PokemonMove move : moveRegistry.values()) {
                    int typeInt = move.getType(); // Assumes there's a method getTypeInt() that returns the type as int
                    movesByType.computeIfAbsent(typeInt, k -> new HashMap<>()).put(move.getName(), move);
                }

                for (Map.Entry<Integer, Map<String, PokemonMove>> entry : movesByType.entrySet()) {
                    File file = new File(JSON_DIR_PATH + getTypeFileName(entry.getKey()));
                    file.getParentFile().mkdirs(); // Ensure the directories exist
                    try (FileWriter writer = new FileWriter(file)) {
                        GSON.toJson(entry.getValue(), writer);
                        AncientLegendsMain.LOGGER.info("Saved " + entry.getValue().size() + " moves to " + file.getName());
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void loadMovesFromJson() {
            moveRegistry.clear();
            for (int typeInt = 0; typeInt <= 17; typeInt++) {
                File jsonFile = new File(JSON_DIR_PATH + getTypeFileName(typeInt));
                if (jsonFile.exists()) {
                    try (FileReader reader = new FileReader(jsonFile)) {
                        Type moveRegistryType = new TypeToken<Map<String, PokemonMove>>(){}.getType();
                        Map<String, PokemonMove> moves = GSON.fromJson(reader, moveRegistryType);
                        moveRegistry.putAll(moves);
                        AncientLegendsMain.LOGGER.info("Loaded " + moves.size() + " moves from " + jsonFile.getName());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        public static void initializeRegistry() {
            MoveRegistryHelper moveregistryhelper = new MoveRegistryHelper();
            moveregistryhelper.loadMovesFromJson();
            if (moveRegistry.isEmpty()) {
                addDefaultMoves();
            }
        }

        private static void addDefaultMoves() {
            for (DeferredHolder<PokemonMove, ? extends PokemonMove> holder : PokemonMoveInit.POKEMON_MOVES.getEntries()) {
                PokemonMove move = holder.get();
                addMove(move);
            }
        }

        public static void modifyDefaultMove(String moveName, PokemonMove updatedMove) {
            if (moveRegistry.containsKey(moveName)) {
                // Log the modification
                AncientLegendsMain.LOGGER.info("Modifying default move: " + moveName);
                moveRegistry.put(moveName, updatedMove);
                MoveRegistryHelper moveregistryhelper = new MoveRegistryHelper();
                moveregistryhelper.saveMovesToJson();
            } else {
                AncientLegendsMain.LOGGER.error("Default move not found in the registry: " + moveName);
            }
        }

    }
