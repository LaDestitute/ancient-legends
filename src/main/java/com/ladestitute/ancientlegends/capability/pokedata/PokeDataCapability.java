package com.ladestitute.ancientlegends.capability.pokedata;

import net.minecraft.nbt.CompoundTag;
import net.neoforged.neoforge.common.util.INBTSerializable;

public class PokeDataCapability implements INBTSerializable<CompoundTag> {

    private int pokemon_level;
    private int gender;

    public void AddPokemonLevel(int amount) {
        this.pokemon_level += amount;
    }

    public void SetPokemonLevel(int amount) {
        this.pokemon_level = amount;
    }

    public int GetPokemonLevel() {
        return pokemon_level;
    }

    public void SetPokemonGender(int amount) {
        this.gender = amount;
    }

    public int GetPokemonGender() {
        return gender;
    }

    public CompoundTag serializeNBT() {
        CompoundTag nbt = new CompoundTag();
        nbt.putInt("PokemonLevel", this.pokemon_level);
        nbt.putInt("PokemonGender", this.gender);
        return nbt;
    }

    public void deserializeNBT(CompoundTag nbt) {
        this.pokemon_level = nbt.getInt("PokemonLevel");
        this.gender = nbt.getInt("PokemonGender");
    }

}

