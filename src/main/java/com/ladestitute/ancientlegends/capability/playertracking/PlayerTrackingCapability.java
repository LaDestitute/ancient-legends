package com.ladestitute.ancientlegends.capability.playertracking;

import com.ladestitute.ancientlegends.network.PlayerDataPacket;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.LivingEntity;
import net.neoforged.neoforge.common.util.INBTSerializable;
import net.neoforged.neoforge.network.PacketDistributor;

public class PlayerTrackingCapability implements INBTSerializable<CompoundTag> {
    private boolean hasneworldkit;

    public void setHasNewWorldKit(LivingEntity entity, boolean kit) {
        this.hasneworldkit = kit;
        if (!entity.level().isClientSide()) {
            PlayerDataPacket message = new PlayerDataPacket(entity, kit);
            PacketDistributor.TRACKING_ENTITY_AND_SELF.with(entity).send(message);
        }
    }

    public boolean getHasNewWorldKit() {
        return this.hasneworldkit;
    }

    public CompoundTag serializeNBT() {
        CompoundTag nbt = new CompoundTag();
        nbt.putBoolean("HasKit", this.hasneworldkit);
        return nbt;
    }

    public void deserializeNBT(CompoundTag nbt) {
        this.hasneworldkit = nbt.getBoolean("HasKit");
    }
}
