package com.ladestitute.ancientlegends.items.pokeballs;

import com.ladestitute.ancientlegends.entities.pokeballs.PokeBallEntity;
import com.ladestitute.ancientlegends.entities.pokeballs.PokeBallItemEntity;
import net.minecraft.client.resources.language.I18n;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.network.chat.Component;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;

import java.util.List;

public class PokeBallItem extends Item
{
    private static final String CRAFTED_TOOLTIP = "A mysterious ball that can be thrown at wild Pokémon in order to catch them. These balls can be crafted by hand if you gather the necessary materials.";

    public PokeBallItem(Item.Properties properties) {
        super(properties.stacksTo(64));
    }

    pic
    @Override
    public boolean onDroppedByPlayer(ItemStack item, Player player) {
        // Create a new custom ItemEntity for the dropped item
        PokeBallItemEntity pokeballEntity = new PokeBallItemEntity(
                player.level(),
                player.getX(),
                player.getY() + 0.5, // Slightly above the player to simulate throwing
                player.getZ(),
                item.copy() // Create a copy of the item stack to drop
        );

        // Set motion of the item entity (optional, for throwing effect)
        Vec3 motion = new Vec3(
                player.level().random.nextGaussian() * 0.05D,
                player.level().random.nextGaussian() * 0.05D + 0.2D,
                player.level().random.nextGaussian() * 0.05D
        );
        pokeballEntity.setDeltaMovement(motion);

        // Add the custom item entity to the world
        player.level().addFreshEntity(pokeballEntity);

        // Reduce the item stack by 1 since it's dropped
        item.shrink(1);

        // Return false to prevent the normal drop behavior
        return false;
    }

    @Override
    public InteractionResultHolder<ItemStack> use(Level level, Player player, InteractionHand hand) {
        player.getCooldowns().addCooldown(this, 10);
        if (!player.level().isClientSide) {
            // Create a new instance of our entity
            PokeBallEntity thrownPokeBall = new PokeBallEntity(player, player.level());
            // Set the entity to the item we are holding
            ItemStack pokeBallStack = player.getItemInHand(hand);
            thrownPokeBall.setItem(pokeBallStack);
            // Set shoot properties
            thrownPokeBall.shootFromRotation(player, player.getXRot(), player.getYRot(), 0.0F, 1.5F, 1F);
            // Add entity to the world
            player.level().addFreshEntity(thrownPokeBall);
            // Shrink item stack
            if (!player.isCreative()) {
                pokeBallStack.shrink(1);
            }
        }
        return super.use(level, player, hand);
    }

    @Override
    public void appendHoverText(ItemStack stack, Level level, List<Component> tooltip, TooltipFlag flag) {
        super.appendHoverText(stack, level, tooltip, flag);
        if (stack.hasTag() && stack.getTag().contains("CapturedPokemon")) {
            CompoundTag capturedPokemon = stack.getTag().getCompound("CapturedPokemon");
            String name = I18n.get(capturedPokemon.getString("Name"));
            String gender = capturedPokemon.getString("Gender");
            int pokelevel = capturedPokemon.getInt("Level");
            int pokehp = capturedPokemon.getInt("HP");
            int pokemaxhp = capturedPokemon.getInt("Max-HP");
            tooltip.add(Component.literal("Name: " + name));
            tooltip.add(Component.literal("Gender: " + gender));
            tooltip.add(Component.literal("Level: " + pokelevel));
            tooltip.add(Component.literal("HP: " + pokehp + "/" + pokemaxhp));
            ListTag movesList = capturedPokemon.getList("MoveSlots", 10);
            StringBuilder movesTooltip = new StringBuilder();
            for (int i = 0; i < movesList.size(); i++) {
                CompoundTag moveTag = movesList.getCompound(i);
                String moveName = I18n.get(moveTag.getString("MoveName"));
                int currentPP = moveTag.getInt("CurrentPP");
                int maxPP = moveTag.getInt("MaxPP");
                if (!moveName.isEmpty() && maxPP > 0) {
                    if (movesTooltip.length() > 0) {
                        movesTooltip.append("/");
                    }
                    movesTooltip.append(moveName).append(" (PP: ").append(currentPP).append("/").append(maxPP).append(")");
                }
            }
            if (movesTooltip.length() > 0) {
                tooltip.add(Component.literal(movesTooltip.toString()));
            }
        } else {
            tooltip.add(Component.translatable(CRAFTED_TOOLTIP));
        }
    }

    public static boolean hasCraftedTooltip(ItemStack stack) {
        List<Component> tooltip = stack.getTooltipLines(null, TooltipFlag.Default.NORMAL);
        for (Component component : tooltip) {
            if (component.getString().contains(CRAFTED_TOOLTIP)) {
                return true;
            }
        }
        return false;
    }

    }




