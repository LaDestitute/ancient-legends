package com.ladestitute.ancientlegends.items.utilities;

import com.ladestitute.ancientlegends.container.PartyMenu;
import com.ladestitute.ancientlegends.inventory.partybag.PartyBagData;
import com.ladestitute.ancientlegends.inventory.partybag.PartyBagManager;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.SimpleMenuProvider;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.UUID;

public class PartyBagItem extends Item {
    public PartyBagItem(Item.Properties properties) {
        super(new Item.Properties().stacksTo(1));

    }

    public static PartyBagData getData(ItemStack stack) {
        if (!(stack.getItem() instanceof PartyBagItem))
            return null;
        UUID uuid;
        CompoundTag tag = stack.getOrCreateTag();
        if (!tag.contains("UUID")) {
            uuid = UUID.randomUUID();
            tag.putUUID("UUID", uuid);
        } else
            uuid = tag.getUUID("UUID");
        return PartyBagManager.get().getOrCreatePartyBag(uuid, ((PartyBagItem) stack.getItem()));
    }

    @Override
    public boolean isEnchantable(@Nonnull ItemStack stack) {
        return false;
    }

    @Override
    @Nonnull
    public InteractionResultHolder<ItemStack> use(Level worldIn, Player playerIn, @Nonnull InteractionHand handIn) {
        ItemStack storage = playerIn.getItemInHand(handIn);
        if (!worldIn.isClientSide && playerIn instanceof ServerPlayer && storage.getItem() instanceof PartyBagItem) {
            PartyBagData data = PartyBagItem.getData(storage);
            UUID uuid = data.getUuid();
            data.updateAccessRecords(playerIn.getName().getString(), System.currentTimeMillis());
            playerIn.openMenu(new SimpleMenuProvider( (windowId, playerInventory, playerEntity)
                            -> new PartyMenu(windowId, playerInventory, uuid, data.getHandler()), storage.getHoverName()),
                    (buffer -> buffer.writeUUID(uuid)));
        }
        return InteractionResultHolder.success(playerIn.getItemInHand(handIn));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("A small bag used to store Poké Balls containing Pokémon conveniently."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

}

