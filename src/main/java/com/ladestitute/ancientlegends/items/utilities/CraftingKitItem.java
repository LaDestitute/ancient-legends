package com.ladestitute.ancientlegends.items.utilities;

import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.SimpleMenuProvider;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ContainerLevelAccess;
import net.minecraft.world.inventory.CraftingMenu;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class CraftingKitItem extends Item {
    private static final Component TITLE = Component.literal("Crafting");

    public CraftingKitItem(Item.Properties properties) {
        super(properties.stacksTo(1));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("A kit containing everything you might need for crafting, even when there is no crafting table around. However, you can only use the materials you have in your inventory and satchel."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public InteractionResultHolder<ItemStack> use(Level world, Player player, InteractionHand hand) {
        if (!world.isClientSide()) {
            player.openMenu(this.getContainer(world, player.blockPosition()));
        }

        return super.use(world, player, hand);
    }

    private MenuProvider getContainer(Level world, BlockPos pos) {
        return new SimpleMenuProvider((windowId, playerInventory, playerEntity) -> {
            return new CraftingMenu(windowId, playerInventory, ContainerLevelAccess.create(world, pos)) {
                @Override
                public boolean stillValid(Player player) {
                    return true;
                }
            };
        }, TITLE);
    }

}

