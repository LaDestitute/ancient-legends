package com.ladestitute.ancientlegends.items.utilities;

import com.ladestitute.ancientlegends.capability.pokedata.PokeDataCapability;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.PokemonEntity;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.passive.BidoofEntity;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.skittish.StarlyEntity;
import com.ladestitute.ancientlegends.registry.AttachmentInit;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;

public class PokeScannerItem extends Item {

    public PokeScannerItem(Item.Properties properties) {
        super(properties.stacksTo(64));
    }

    @Override
    public InteractionResult interactLivingEntity(ItemStack p_41398_, Player player, LivingEntity pokemon, InteractionHand p_41401_) {
        if(!player.level().isClientSide && pokemon instanceof PokemonEntity)
        {
            PokeDataCapability data = pokemon.getData(AttachmentInit.POKE_DATA);
            if(pokemon instanceof BidoofEntity) {
                player.sendSystemMessage(Component.literal("Pokemon name: Bidoof"));
                player.sendSystemMessage(Component.literal("Pokemon species: Plump Mouse"));
                player.sendSystemMessage(Component.literal("Pokemon type 1: Normal"));
                player.sendSystemMessage(Component.literal("Pokemon type 2: Normal"));
            }
            if(pokemon instanceof StarlyEntity)
            {
                player.sendSystemMessage(Component.literal("Pokemon name: Starly"));
                player.sendSystemMessage(Component.literal("Pokemon species: Starling"));
                player.sendSystemMessage(Component.literal("Pokemon type 1: Normal"));
                player.sendSystemMessage(Component.literal("Pokemon type 2: Flying"));
            }
                    player.sendSystemMessage(Component.literal("Pokemon level: " + data.GetPokemonLevel()));
                if(data.GetPokemonGender() == 0)
                {
                    player.sendSystemMessage(Component.literal("Pokemon gender: ♂"));
                }
            if(data.GetPokemonGender() == 1)
            {
                player.sendSystemMessage(Component.literal("Pokemon gender: ♀"));
            }
            player.sendSystemMessage(Component.literal("Pokemon catchrate: " + ((PokemonEntity) pokemon).getCatchrate()));
            player.sendSystemMessage(Component.literal("Pokemon ATK: " + ((PokemonEntity) pokemon).getPokeAttack()));
            player.sendSystemMessage(Component.literal("Pokemon DEF: " + ((PokemonEntity) pokemon).getPokeDefense()));
            System.out.println("data: " + data.GetPokemonLevel() + " / " + data.GetPokemonGender());
        }

        return super.interactLivingEntity(p_41398_, player, pokemon, p_41401_);
    }
}
