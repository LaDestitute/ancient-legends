package com.ladestitute.ancientlegends.items.materials;

import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class TumblestoneItem extends Item {

    public TumblestoneItem(Item.Properties properties) {
        super(properties.stacksTo(64));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("When combined with an Apricorn, this mysterious, tumbled stone can draw out the properties required to forge a Poke Ball."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }



}

