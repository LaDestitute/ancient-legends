package com.ladestitute.ancientlegends.items.materials;

import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class ApricornItem extends Item {

    public ApricornItem(Item.Properties properties) {
        super(properties.stacksTo(64));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("A fruit whose shell can be used to form the capsules of all manners of Poke Balls when crafting them from scratch. Its proper name is actually ''Brown Apricorn.''"));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }



}
