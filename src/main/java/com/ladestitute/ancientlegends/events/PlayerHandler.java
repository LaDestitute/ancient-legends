package com.ladestitute.ancientlegends.events;

import com.ladestitute.ancientlegends.capability.playertracking.PlayerTrackingCapability;
import com.ladestitute.ancientlegends.registry.AttachmentInit;
import com.ladestitute.ancientlegends.registry.ItemInit;
import net.minecraft.world.entity.monster.Zombie;
import net.minecraft.world.entity.player.Player;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.common.util.TriState;
import net.neoforged.neoforge.event.entity.player.EntityItemPickupEvent;
import net.neoforged.neoforge.event.entity.player.PlayerEvent;
import net.neoforged.neoforge.items.ItemHandlerHelper;

public class PlayerHandler {


    @SubscribeEvent
    public void onClone(PlayerEvent.Clone event) {
        Player oldPlayer = event.getOriginal();
        Player newPlayer = event.getEntity();
        if (!event.isWasDeath()) {
            PlayerTrackingCapability data1 = oldPlayer.getData(AttachmentInit.PLAYER_DATA);
            PlayerTrackingCapability data2 = newPlayer.getData(AttachmentInit.PLAYER_DATA);
            data2.deserializeNBT(data1.serializeNBT());
        }
    }

    @SubscribeEvent
    public void onlogin(PlayerEvent.PlayerLoggedInEvent event)
    {
        PlayerTrackingCapability data = event.getEntity().getData(AttachmentInit.PLAYER_DATA);
        if (!event.getEntity().level().isClientSide() && event.getEntity() != null && !data.getHasNewWorldKit()) {
            ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.PARTY_BAG.get().getDefaultInstance());
            ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.SATCHEL.get().getDefaultInstance());
            ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.CRAFTING_KIT.get().getDefaultInstance());
            ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.POKE_SCANNER.get().getDefaultInstance());
            ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.POKE_BALL.get().getDefaultInstance());
            ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.POKE_BALL.get().getDefaultInstance());
            ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.POKE_BALL.get().getDefaultInstance());
            ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.POKE_BALL.get().getDefaultInstance());
            ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.POKE_BALL.get().getDefaultInstance());
            data.setHasNewWorldKit(event.getEntity(), true);
        }
    }
}
