package com.ladestitute.ancientlegends.events;

import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.animal.Fox;
import net.minecraft.world.entity.monster.*;
import net.minecraft.world.item.ItemStack;
import net.neoforged.neoforge.event.entity.living.LivingEvent;

public class MainEventHandler {
    public void onLivingUpdate(LivingEvent.LivingTickEvent event) {
        LivingEntity entity = event.getEntity();

        if (entity instanceof Zombie||entity instanceof Fox||entity instanceof Husk ||
                entity instanceof Drowned||entity instanceof ZombieVillager||
                entity instanceof Skeleton||entity instanceof WitherSkeleton||
                entity instanceof Stray) {
            ItemStack heldItem = entity.getMainHandItem();

            if (!heldItem.isEmpty() && heldItem.hasTag() && heldItem.getTag().contains("CapturedPokemon")) {
                // Drop the item with the NBT data intact
                entity.spawnAtLocation(heldItem, 0.5F);

                // Clear the item from the zombie's hand
                entity.setItemInHand(entity.getUsedItemHand(), ItemStack.EMPTY);
            }
        }
    }
}
