package com.ladestitute.ancientlegends.events;

import com.ladestitute.ancientlegends.battle.core.BattleManager;
import com.ladestitute.ancientlegends.battle.gui.turn_order.PlayerMoveSlotsRenderer;
import com.ladestitute.ancientlegends.client.AncientLegendsClient;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.PokemonEntity;
import com.ladestitute.ancientlegends.registry.ItemInit;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.client.event.InputEvent;
import net.neoforged.neoforge.event.TickEvent;
import net.neoforged.neoforge.event.entity.player.PlayerInteractEvent;

public class BattleEvents {
    private static BattleManager battleManager;

    public static void setBattleManager(BattleManager manager) {
        battleManager = manager;
    }

    @SubscribeEvent
    public void onPokeballUse(PlayerInteractEvent.RightClickItem event) {
        Player player = event.getEntity();
        ItemStack itemStack = event.getItemStack();
        if (battleManager != null) {
            if (itemStack.getItem() == ItemInit.POKE_BALL.get() && (!itemStack.hasTag() || !itemStack.getTag().getBoolean("Captured"))) {
                event.setCanceled(true);
                player.sendSystemMessage(Component.literal("You cannot use this if its not your turn!"));
            }
        }
    }

    @SubscribeEvent
    public void onPokeballUse(PlayerInteractEvent.EntityInteract event) {
        Player player = event.getEntity();
        ItemStack itemStack = event.getItemStack();
        if (battleManager != null) {
            if (itemStack.getItem() == ItemInit.POKE_BALL.get() && (!itemStack.hasTag() || !itemStack.getTag().getBoolean("Captured"))) {
                event.setCanceled(true);
                player.sendSystemMessage(Component.literal("You cannot use this if its not your turn!"));
            }
        }
    }

    //Test event for denying item use on wild pokemon turns
    @SubscribeEvent
    public void onEntityInteract(PlayerInteractEvent.EntityInteract event) {
        Player player = event.getEntity();
        if (battleManager != null) {
            if (event.getItemStack().getItem() == Items.APPLE && event.getTarget() instanceof PokemonEntity) {
                PokemonEntity targetPokemon = (PokemonEntity) event.getTarget();
                if (targetPokemon.isTame() && targetPokemon.getOwnerUUID().equals(player.getUUID())) {
                    event.setCanceled(true);
                    player.sendSystemMessage(Component.literal("You cannot use this if its not your turn!"));
                }
            }
        }
    }

//    @SubscribeEvent
//    public void tick(TickEvent.PlayerTickEvent event) {
//        if (battleManager != null && battleManager.isPlayerTurn()) {
//            if (AncientLegendsClient.KEY_MOVE_1.consumeClick() ) {
//                BattleManager.instance.handlePlayerMove(0);
//            } else if (AncientLegendsClient.KEY_MOVE_2.consumeClick()) {
//                BattleManager.instance.handlePlayerMove(1);
//            } else if (AncientLegendsClient.KEY_MOVE_3.consumeClick()) {
//                BattleManager.instance.handlePlayerMove(2);
//            } else if (AncientLegendsClient.KEY_MOVE_4.consumeClick()) {
//                BattleManager.instance.handlePlayerMove(3);
//            }
//        }
//    }
}
