package com.ladestitute.ancientlegends.events;

import com.ladestitute.ancientlegends.capability.playertracking.PlayerTrackingCapability;
import com.ladestitute.ancientlegends.capability.pokedata.PokeDataCapability;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.PokemonEntity;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.passive.BidoofEntity;
import com.ladestitute.ancientlegends.entities.mobs.pokemon.skittish.StarlyEntity;
import com.ladestitute.ancientlegends.network.PlayerDataPacket;
import com.ladestitute.ancientlegends.network.SyncPokeDataPacket;
import com.ladestitute.ancientlegends.registry.AttachmentInit;
import com.ladestitute.ancientlegends.util.enums.PokemonTypes;
import net.minecraft.nbt.CompoundTag;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.event.entity.EntityJoinLevelEvent;
import net.neoforged.neoforge.network.PacketDistributor;

import java.util.Random;

public class AddPokeDataHandler {
    @SubscribeEvent
    public void addPokeData(EntityJoinLevelEvent event) {
        if(!(event.getEntity() instanceof PokemonEntity))
        {
            return;
        }
        PokemonEntity pokemon = (PokemonEntity) event.getEntity();
        PokeDataCapability data = pokemon.getData(AttachmentInit.POKE_DATA);
            data.SetPokemonLevel(event.getEntity().level().getRandom().nextInt(7) + 1);
        Random rand = new Random();
        if (event.getEntity() instanceof BidoofEntity) {
            ((BidoofEntity) event.getEntity()).setCatchrate(255);
            ((BidoofEntity) event.getEntity()).setType1(PokemonTypes.NORMAL.ordinal());
            ((BidoofEntity) event.getEntity()).setType2(PokemonTypes.NORMAL.ordinal());
            int genderoll = rand.nextInt(101);
            if (genderoll <= 49)
            {
                data.SetPokemonGender(0);
            }
            if (genderoll > 49)
            {
                data.SetPokemonGender(1);
            }
            ((BidoofEntity) event.getEntity()).setPokeHP(59);
            ((BidoofEntity) event.getEntity()).setPokeMaxHP(59);
            ((BidoofEntity) event.getEntity()).setPokeAttack(45);
            ((BidoofEntity) event.getEntity()).setPokeDefense(40);
            ((BidoofEntity) event.getEntity()).setPokeSpecialAttack(35);
            ((BidoofEntity) event.getEntity()).setPokeSpecialDefense(40);
            ((BidoofEntity) event.getEntity()).setPokeSpeed(31);
            ((BidoofEntity) event.getEntity()).setPokeAccuracy(0);
            ((BidoofEntity) event.getEntity()).setPokeEvasion(0);
            ((BidoofEntity) event.getEntity()).learnMovesBasedOnLevel();
        }
        //SEPARATOR
        if (event.getEntity() instanceof StarlyEntity) {
            ((StarlyEntity) event.getEntity()).setCatchrate(255);
            ((StarlyEntity) event.getEntity()).setType1(PokemonTypes.NORMAL.ordinal());
            ((StarlyEntity) event.getEntity()).setType2(PokemonTypes.FLYING.ordinal());
            int genderoll = rand.nextInt(101);
            if (genderoll <= 49)
            {
                data.SetPokemonGender(0);
            }
            if (genderoll > 49)
            {
                data.SetPokemonGender(1);
            }
            ((StarlyEntity) event.getEntity()).setPokeHP(40);
            ((StarlyEntity) event.getEntity()).setPokeMaxHP(40);
            ((StarlyEntity) event.getEntity()).setPokeAttack(55);
            ((StarlyEntity) event.getEntity()).setPokeDefense(30);
            ((StarlyEntity) event.getEntity()).setPokeSpecialAttack(30);
            ((StarlyEntity) event.getEntity()).setPokeSpecialDefense(30);
            ((StarlyEntity) event.getEntity()).setPokeSpeed(60);
            ((StarlyEntity) event.getEntity()).setPokeAccuracy(0);
            ((StarlyEntity) event.getEntity()).setPokeEvasion(0);
            ((StarlyEntity) event.getEntity()).learnMovesBasedOnLevel();
        }
        //SEPARATOR
        //SEPARATOR
        syncStatsToTrackingPlayers(pokemon);
    }

    private static void syncStatsToTrackingPlayers(PokemonEntity pokemon) {
        if(!pokemon.level().isClientSide()) {
            PokeDataCapability data = pokemon.getData(AttachmentInit.POKE_DATA);
            SyncPokeDataPacket message = new SyncPokeDataPacket(pokemon, data.GetPokemonLevel(), data.GetPokemonGender());
            PacketDistributor.TRACKING_ENTITY.with(pokemon).send(message);
        }
    }

}

