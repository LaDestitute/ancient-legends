An experimental-dev/alpha Minecraft NeoForge mod based on Pokémon Legends: Arceus.

Current Dev Branch: battle-dev (development of the action-speed turn based battle system)

Current Features:
* Groundwork Pokémon stats: HP/Max-HP, Attack, Defense, Special Attack, Special Defense, Speed, Accuracy, Evasion and action speed
* Misc Pokémon stats: level, gender, catchrate, type1, type2, ball-ID (for caught Pokemon)
* Functioning Poké Balls with sneak and backstrike catchrate modifiers       
* Apricorn Trees and Tumblestone Rocks, used to craft Poké Balls
* Satchel, six extra inventory spaces that can store Poké Balls and materials (will increase to 10 soon!)
* Party Bag, store six Poke Balls as long as they have a caught Pokémon inside
* Crafting Kit, a portable crafting bench